---
layout: page
title: Zeltlager
permalink: /zeltlager/
section: aktionen
cover_image: zeltlager.jpg
---
<!-- TODO: Mehr übers Zeltlager berichten -->

<figure class="full-width">
  <span style="font-size: 2rem">Zeltlagertermin 2022: 17. bis 27. August 2022</span>
</figure>

<figure class="full-width">
  <a href="https://files.smj-fulda.org/zeltlager/zeltlager-flyer-2022.pdf">
    <img src="/aktionen/zeltlager/2022/thumb.jpg" />
  </a>
</figure>

<a class="btn" href="https://files.smj-fulda.org/zeltlager/zeltlager-flyer-2022.pdf">
  Zeltlager-Flyer
</a>

<strong>Liebe Eltern, liebe Jungs,</strong><br />
Vom 17. bis 27. August findet in diesem Jahr unser Zeltlager für Jungs im Alter von 9 bis 14 Jahren statt.<br />
Das Zeltlager ist das Highlight unser Jahresarbeit und wird von unseren geschulten Gruppenleitern über einen langen Zeitraum geplant und vorbereitet.<br />
Diesmal wird es magisch! Unter dem Motto 'Im Zauberwald' geht es nach Heigenbrücken.<br />
Euch erwartet ein Abenteuer mit starker Gemeinschaft und Spaß für Zeltlagererfahrene wie Neulinge.<br />
Als Schönstatt-Mannesjugend Fulda liegen uns auch der Glaube und die Stärkung der Charaktere junger Menschen am Herzen. Wir freuen uns auf euch!<br />
Die SMJ Fulda mit ihren Lagerleitern Fabian Buhl & Lennard Wolf<br />
<strong>Kontakt:</strong> <a href="mailto:Fabian.Buhl@SMJ-Fulda.org">Fabian.Buhl@SMJ-Fulda.org</a> & <a href="Lennard.wolf.1@gmx.de">Lennard.wolf.1@gmx.de</a><br />

<h3>Nochmal Kurz:</h3>

<ul>
  <li><strong>Veranstalter:</strong> Schönstatt-Mannesjugend Fulda</li>
  <li><strong>Termin:</strong> Mittwoch 17. bis Samstag 27. August</li>
  <li><strong>Ort:</strong> Zeltplatz „Am Adamsberg“ in 63869 Heigenbrücken</li>
  <li><strong>Thema:</strong> Im Zauberwald</li>
  <li><strong>Teilnehmer:</strong>	Jungs von 9 bis 14 Jahren</li>
  <li><strong>Kosten:</strong> 215€ für ein Kind, 180€ für Geschwister</li>
</ul>


<a class="btn" href="{{ site.baseurl }}/artikel/2021-10-26-startschuss-zeltlager-2022/">Startschuss für das Zeltlager 2022</a>

# Zeltlager 2021

<figure class="full-width">
    <a href="https://files.smj-fulda.org/zeltlager/zeltlager-flyer-2021.pdf" title="Zeltlagerflyer 2021 (PDF)">
        <img src="{{ site.baseurl }}/aktionen/zeltlager/2021/thumb.jpg" />
    </a>
    <figcaption>
    <a class="btn" href="{{ site.baseurl }}/artikel/2021-08-25-bericht-vom-zeltlager-2021/">Bericht vom Zeltlager 2021</a>
    </figcaption>
</figure>

# Für Eltern

<figure>
    <img src="{{ site.baseurl }}/images/pages/zeltlager/kreuz-aufstellen.jpg" />
    <figcaption>Teamwork: Aufstellen des Lagerkreuzes</figcaption>
</figure>

Unser Zeltlager findet üblicherweise in den ersten beiden Wochen der hessischen Sommerferien an wechselnden Orten statt und dauert zehn Tage. Eingeladen sind Jungen von 8 bis 14 Jahren.

Auf dem Programm stehen unter anderem Lagerfeuer, Nachtwache, Gruppenspiele, Zwei-Tages-Tour sowie Gruppenstunden. Gottesdienste und Gebetszeiten sind ebenso selbstverständlich.

<figure>
    <img src="https://farm4.staticflickr.com/3896/14708499850_cab4d846a2_z_d.jpg" />
    <figcaption>Männerarbeit: Baumfällen</figcaption>
</figure>

> Im Zeltlager gibt es jeden Tag neue Herausforderungen. In starker Gemeinschaft und begleitet von Gruppenleitern kann jeder an neuen Erfahrungen wachsen und zu einer originellen Persönlichkeit reifen.

Jede Zeltgruppe besteht aus etwa sechs Jungen und wird von einem Gruppenleiter betreut. Unsere Betreuer haben eine Ausbildung nach Juleica-Standard absolviert.
Für das leibliche und seelische Wohl sorgen erfahrene Küchenfrauen und Lagerpriester. Alle Mitarbeiter sind ehrenamtlich tätig und erhalten keine Vergütung.

<figure>
    <img src="https://farm6.staticflickr.com/5584/14708620897_c8bdec7878_z_d.jpg" />
    <figcaption>Lagerkapelle</figcaption>
</figure>

Die Schönstatt-Mannesjugend ist eine katholische Jugendgemeinschaft und Teil der internationalen Schönstattbewegung.
Wir bieten nicht nur ein spannendes Ferienabenteuer, sondern unser Ziel ist es, christliche Werte zu vermitteln und die Entwicklung junger Menschen zu starken Charakteren zu fördern.

Wir wollen die Erfahrungen weitergeben, die uns selbst geprägt haben, in Gemeinschaft einen frohen und lebendigen Glauben leben.

# Lagerzeitungen

Über das Leben im Zeltlager berichtet jedes Jahr eine Lagerzeitung, hier gibt es die vergangenen Ausgaben zum Nachlesen:

<div class="cover-gallery">
{% assign jahre= '2021,2020,2016,2015,2014,2013,2012,2011,2010' | split:',' %}
{% for jahr in jahre %}
    <a class="zeitung-cover" href="https://files.smj-fulda.org/zeltlager/smj-fulda_lagerzeitung-{{ jahr }}.pdf" title="Lagerzeitung {{ jahr }}">
        <img src="https://files.smj-fulda.org/zeltlager/lagerzeitung-cover-{{ jahr }}.jpg">
    </a>
{% endfor %}
</div>

# Chronologie

<figure>
    <img src="https://farm4.staticflickr.com/3875/14892120471_bd93fae304_z_d.jpg" />
    <figcaption>Gemeinschaftszelt</figcaption>
</figure>
<figure>
    <img src="https://farm4.staticflickr.com/3853/14708457869_9e0d1e1d77_z_d.jpg" />
    <figcaption>Blick ins Zelt</figcaption>
</figure>
Die Schönstatt-Mannesjugend Fulda veranstaltet Zeltlager seit den 60-er Jahren.

<table class="table--list">
    <tr class="table__head">
        <th>Zeltlager</th>
        <th>Jahr</th>
        <th>Ort</th>
    </tr>
    <tr>
        <th>Im Zauberwald</th>
        <td>2022</td>
        <td>Heigenbrücken</td>
    </tr>
    <tr>
        <th>Im Lager der Griechen</th>
        <td>2021</td>
        <td>Dietershausen</td>
    </tr>
    <tr>
        <th>Die drei ???</th>
        <td>2020</td>
        <td>Dietershausen (Corona-Hauslager)</td>
    </tr>
    <tr>
        <th>In 8 Tagen um die Welt</th>
        <td>2019</td>
        <td>Hausen</td>
    </tr>
    <tr>
        <th>Volle Fahrt auf der Santa Maria</th>
        <td>2018</td>
        <td>Schönstatt</td>
    </tr>
    <tr>
        <th>Der Herr der Ringe</th>
        <td>2017</td>
        <td>Thalwenden</td>
    </tr>
    <tr>
        <th><!--a href="{{ site.baseurl }}/termine/2016/jul-19-zeltlager-2016"-->Asterix &amp; Obelix &ndash; Das verschwundene Rezept<!--/a--></th>
        <td>2016</td>
        <td>Oberweißenbrunn</td>
    </tr>
    <tr>
        <th><!--a href="{{ site.baseurl }}/artikel/1006-abenteuer-leben-%E2%80%93-eine-unerwartete-reise"-->Abenteuer Leben &ndash; Eine unerwartete Reise<!--/a--></th>
        <td>2015</td>
        <td>Schönstatt</td>
    </tr>
    <tr>
        <th><!--a href="{{ site.baseurl }}/termine/2014/jul-29-zeltlager-2014"-->Sherlock Holmes<!--/a--></th>
        <td>2014</td>
        <td>Schimborn</td>
    </tr>
    <tr>
        <th><!--a href="{{ site.baseurl }}/termine/2013/jul-9-zeltlager-2013"-->Wilder Westen<!--/a--></th>
        <td>2013</td>
        <td>Hillenberg</td>
    </tr>
    <tr>
        <th><!--a href="{{ site.baseurl }}/termine/2012/jul-3-zeltlager-2012"-->Die Siedler<!--/a--></th>
        <td>2012</td>
        <td>Schönstatt</td>
    </tr>
    <tr>
        <th><!--a href="{{ site.baseurl }}/zeltlager/2011"-->Asterix &amp; Obelix&nbsp;- Die Gallier sind los!<!--/a--></th>
        <td>2011</td>
        <td>Oberweißenbrunn</td>
    </tr>
    <tr>
        <th><!--a href="{{ site.baseurl }}/artikel/832-starker-wind-%E2%80%93-wir-setzen-segel"-->Starker Wind&nbsp;- Wir setzen Segel<!--/a--></th>
        <td>2010</td>
        <td>Hausen</td>
    </tr>
    <tr>
        <th><!--a href="{{ site.baseurl }}/artikel/178-zeltlager-2009"-->Highlander<!--/a--></th>
        <td>2009</td>
        <td>Schönstatt</td>
    </tr>
    <tr>
        <th>Tempelritter</th>
        <td>2008</td>
        <td>Glauberg</td>
    </tr>
    <tr>
        <th>Im Auftrag ihrer Majestät</th>
        <td>2007</td>
        <td>Obereschenbach</td>
    </tr>
    <tr>
        <th>Timeshift, die Jagd nach der Weltformel</th>
        <td>2006</td>
        <td>Schönstatt</td>
    </tr>
    <tr>
        <th>Volo incredibilis - maria fieri!</th>
        <td>2005</td>
        <td>Hausen</td>
    </tr>
    <tr>
        <th>Bonifatius</th>
        <td>2004</td>
        <td>Bastheim</td>
    </tr>
    <tr>
        <th>Pro gloria coronae</th>
        <td>2003</td>
        <td>Schönstatt</td>
    </tr>
    <tr>
        <th>Isola na costa</th>
        <td>2002</td>
        <td>Idstein</td>
    </tr>
    <tr>
        <th>Pentestyloi</th>
        <td>2001</td>
        <td>Weyersfeld</td>
    </tr>
    <tr>
        <th>Jeremia</th>
        <td>2000</td>
        <td>Hausen</td>
    </tr>
    <tr>
        <th>Spuren entdecken</th>
        <td>1999</td>
        <td>Schönstatt</td>
    </tr>
    <tr>
        <th>Die Siedler von Matar</th>
        <td>1998</td>
        <td>Schimborn</td>
    </tr>
    <tr>
        <th>Jesus von Nazareth &ndash; wag es, komm mit</th>
        <td>1997</td>
        <td>Hausen</td>
    </tr>
    <tr>
        <th>Expedition Leben</th>
        <td>1996</td>
        <td>Schönstatt</td>
    </tr>
    <tr>
        <th>Excalibur</th>
        <td>1995</td>
        <td>Bastheim</td>
    </tr>
    <tr>
        <th>Mose &ndash; Aufbruch ins gelobte Land</th>
        <td>1994</td>
        <td>Eckweisbach</td>
    </tr>
    <tr>
        <th>Unternehmen Arche</th>
        <td>1993</td>
        <td>Schönstatt</td>
    </tr>
    <tr>
        <th>Wage den Aufbruch &ndash; folge Franziskus</th>
        <td>1992</td>
        <td>Hausen</td>
    </tr>
    <tr>
        <th>&nbsp;</th>
        <td>1991</td>
        <td>Dingolshausen</td>
    </tr>
    <tr>
        <th>&nbsp;</th>
        <td>1990</td>
        <td>(Radtour)</td>
    </tr>
    <tr>
        <th>Mutter Theresa &ndash; Einsatz in Kalkutta</th>
        <td>1989</td>
        <td>Schönstatt</td>
    </tr>
    <tr>
        <th>&nbsp;</th>
        <td>1988</td>
        <td>Bastheim</td>
    </tr>
    <tr>
        <th>Ter Optima</th>
        <td>1987</td>
        <td>Sandberg</td>
    </tr>
    <tr>
        <th>Asterix Und Obelix</th>
        <td>1986</td>
        <td>Schönstatt</td>
    </tr>
    <tr>
        <th>Mit dir leben in der Neuen Stadt</th>
        <td>1984</td>
        <td>Schönstatt</td>
    </tr>
    <tr>
        <th>Aufbruch mit Bonifatius</th>
        <td>1983</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <th>Geht zur Quelle und baut eine Neue Stadt</th>
        <td>1982</td>
        <td>Mayen/Eifel</td>
    </tr>
    <tr>
        <th>Castra Aestiva Fuldensis MCMLXXXI</th>
        <td>1981</td>
        <td>Hausen</td>
    </tr>
    <tr>
        <th></th>
        <td>1970</td>
        <td>Schönstatt</td>
    </tr>
    <tr>
        <th></th>
        <td>1968</td>
        <td>Canisiushof/Eichstätt</td>
    </tr>
</table>
<figure>
    <img src="https://farm6.staticflickr.com/5558/14895146265_24ee4e7a05_z_d.jpg" />
    <figcaption>Zeltgruppe auf Zwei-Tagestour (2014)</figcaption>
</figure>

<figure class="full-width">
    <img src="https://farm6.staticflickr.com/5562/14872114546_4f391670f5_z_d.jpg" />
    <figcaption>Rübenziehen</figcaption>
</figure>
<figure>
    <img src="{{ site.baseurl }}/images/pages/zeltlager/fahnen.jpg" />
    <figcaption>Lagerfahnen</figcaption>
</figure>
<figure>
    <img src="{{ site.baseurl }}/images/pages/zeltlager/gruppenbild.jpg" />
    <figcaption>Gruppenbild (2008)</figcaption>
</figure>
<figure>
    <img src="{{ site.baseurl }}/images/pages/zeltlager/zelte.jpg" />
    <figcaption>Gruppenzelte (2008)</figcaption>
</figure>
<figure>
    <img src="" />
    <figcaption></figcaption>
</figure>
<!-- TODO:
Info zu aktuellem Zeltager?
Bilder
FunFacts?
Gute Ansicht der Zeltlager (Tabelle?)
 -->
