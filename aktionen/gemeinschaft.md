---
layout: page
cover_image:
title: Gemeinschaft in der SMJ
permalink: /gemeinschaft/
section: aktionen
cover_image: 
---
<!-- TODO: Terminen einfügen-->

Neben unseren Veranstaltungen für unsere Jungs im Alter von 9-13 gibt es auch Termine die grade die Gemeinschaft unter den Gruppenleitern stärken sollen.

## Stammtisch

Unser Stammtisch ist ein monatliches Treffen. Dieses Treffen findet ab dem Kreisalter statt und ist eine Aktion, die wir auch immer wieder zusammen mit der Mädchenjugend durchführen.

An unseren Stammtischen besuchen wir verschiedene Orte im Bistum Fulda und gehen zusammen essen, schauen einen Film, machen einen Spieleabend oder gehen Schlittschuhlaufen usw…

Beim Stammtisch steht die Gemeinschaft, welche ja auch eine Säule unserer Arbeit ist, im Vordergrund. Wir lernen uns untereinander besser kennen und unternehmen dabei auch gerne etwas mit der Mädchenjugend.


## Red Hearts

Die SMJ-Fulda hat auch eine eigene Fußballmannschaft, die Red Hearts (engl. Rote Herzen). Entstanden ist sie als Mannschaft der Gruppenleiter im traditionellen Zeltlager-Fußballmatch „Betreuer gegen Grüpplinge“. Seit einigen Jahren tritt die Mannschaft aber auch außerhalb des Zeltlagers auf, biespielsweise beim SMJ-Cup, dem Fußballturnier der SMJ-Deutschland, und diversen Schoppenturnieren. Größter Erfolg war der Gewinn des ersten SMJ-Cups 2011.

Für uns ein wichtiger Bestandteil und allgemeines Erkennungmerkmal ist unsere Hymne, die 16. Strophe des Bonifatiuslieds, die selbstverständlich vor jedem Spiel bzw. am Anfang eines Turniers lautstark gesungen wird.

Die Mannschaft setzt sich zusammen aus aktiven und ehemaligen Gruppenführern und Freunden der SMJ-Fulda. Trainer ist er jeweils amtierende Diözesanführer.

<figure>
    <img src="https://farm4.staticflickr.com/3926/14742608958_b71faeaf06_b_d.jpg">
    <figcaption>Red Hearts</figcaption>
</figure>

**Trainer:** Simon Wawra<br/>
**Kapitän:** Michael Amberg<br/>
**Präsident:** Alexander Dücker

<figure>
    <img src="https://farm8.static.flickr.com/7358/12956772413_363b1affac_z.jpg">
    <figcaption>Red Hearts in der Verteidigung beim SMJ-Cup 2014</figcaption>
</figure>
<figure>
    <img src="https://farm7.static.flickr.com/6010/5926247174_3731c1c923_z.jpg">
    <figcaption>Hymne der Red Hearts (Zeltlager 2011)</figcaption>
</figure>
<figure>
    <img src="https://farm7.static.flickr.com/6025/5925693721_cf0e212744_z.jpg">
    <figcaption>Teambesprechung (Zeltlager 2011)</figcaption>
</figure>
<figure>
    <img src="{{ site.baseurl }}/images/pages/smj-cup.jpg" />
    <figcaption>SMJ-Cup</figcaption>
</figure>

Bisherige Spiele (soweit bekannt):

* Zeltlager 1997 Hausen 10:1 Sieg
* Zeltlager 1998 Schimborn 4:7 Niederlage
* Zeltlager 1999 Schönstatt 7:8 Niederlage
* Zeltlager 2000 Hausen 3:10 Niederlage
* Zeltlager 2001 Weyersfeld: Niederlage i.E.
* Zeltlager 2002 Idstein: Sieg
* Zeltlager 2003 Schönstatt: Sieg
* Zeltlager 2004 Bastheim: Sieg
* Zeltlager 2005 Hausen : Sieg
* Zeltlager 2006 Schönstatt: 14:1 oder 7:3 Sieg
* Schoppenturnier Obereschenbach 2007
* Zeltlager 2007 Obereschenbach: 11:1 Sieg
* Zeltlager 2008 Glauberg: 9:2 Sieg
* Zeltlager 2009 Schönstatt: 27:4 Sieg
* Zeltlager 2010 Hausen: 17:0 Sieg
* SMJ-Cup 2011: 1. Platz von 3
* Zeltlager 2011 Oberweißenbrunn: 10:0 Sieg
* SMJ-Cup 2012: 3. Platz von 5 ([Bericht auf schoenstatt.de](http://www.schoenstatt.de/de/news/1581/160/Heiss-umkaempfte-Spiele-und-viele-tolle-Tore-5-Teams-beim-2-SMJ-Cup.htm))
* Zeltlager 2012 Schönstatt 9:1 Sieg
* SMJ-Cup 2013: 3. Platz von 7
  * SMJ Freiburg/Schwaben: 1:0
  * SMJ Mainz: 1:0
  * SMJ Trier 1: 0:1
  * SMJ Oberland (Halbfinale): 2:4 i.E. (0:0)
  * SMJ Köln (Spiel um Platz 3) 2:0
* Zeltlager 2013 Hillenberg 10:0 Sieg
* Schoppenturnier Pilgerzell 2014
* SMJ-Cup 2014: 4. Platz von 6 ([Bericht auf schoenstatt.de](http://www.schoenstatt.de/de/news/2403/112/SMJ-Cup-2014-Fussballturnier-der-Schoenstatt-Mannesjugend.htm))
  * SMJ Trier: 0:1
  * Volontäre 2014: 2:1
  * SMJ Oberland (Halbfinale): 0:4
  * SMJ Köln (Spiel um Platz 3) 1:2
* Zeltlager 2014 Schimborn 3:1 Sieg
* Zeltlager 2015 Schönstatt 12:7 Sieg
* Zeltlager 2016 Jossgrund 4:2 Sieg
* Zeltlager 2017 8:2 Sieg
* Zeltlager 2018 12:2 Sieg
* Zeltlager 2019 7:2 Sieg
* Zeltlager 2020 kein Spiel aufgrund von Corona
* Zeltlager 2021 2:1 Sieg

## Noahs Tafelrunde

Bei der Noah´s Tafelrunde, kurz NT, treffen sich alle Aktiven der SMJ-Fulda. Bei diesem Zusammentreffen werden alle Termine und Aktionen des aktuellen Jahres durchgesprochen und es werden die Aufgaben verteilt. 
Der wichtigste Termin mit dem größten Gewicht stellt unser Zeltlager dar. An der NT wird beschlossen welche Verantwortlichen hier mitfahren. 
Wir als SMJ sind uns unserer Verantwortung den Jungs gegenüber bewusst, sodass wir als Gruppenleiter nur junge Männer ab 16 mitnehmen, die eine entsprechende Gruppenleiterschulung besucht haben und über eine entsprechende Juleica-Karte verfügen. 
Neben dem Zeltlager werden auch die Gemeinschaftswochenenden und sonstige Termine an entsprechende Verantwortliche delegiert. 
Die NT beginnt und schließt in unserem Kapellchen in Dietershausen mit einem Gebet.


