---
layout: page
cover_image: 
title: Kreisarbeit
permalink: /kreise/
section: aktionen
cover_image: gruppenbild-kt2016.jpg
---

# Kreise
Wenn unsere Jungen aus dem Zeltlager-Alter herausgewachsen sind, können sie sich in einem sogenannten Kreis tiefer mit Schönstatt und dem katholischen Glauben befassen. In Gemeinschaft unter Gleichaltrigen wachsen sie zusammen und miteinander. Diese Zusammenkünfte finden vierteljährig im Josef-Engling-Haus im Schönstattzentrum Dietershausen statt. Sie werden von einem Kreisführer thematisch vorbereitet und von einem geitlichen Begleiter betreut.

In diesen Wochenendtagungen sollen Gemeinschaftsgeist, der katholische Glaube und schönstättische Ideale an die Teilnehmer herangetragen werden. Thematisiert werden relevante Fragen der persönlichen Entwicklung in den verschiedenen Altersttufen, Glaubensinhalte, allgemeines Schönstattwissen. In Diskussionen und persönlichen Gesprächen werden die Themen weiter bearbeitet und untereinander ausgetausch. Der einzelne Jugendliche soll, durch diese Treffen begleitet, zu einer freien, christlichen, aktiven und verantwortlichen Persönlichkeit heranwachsen und sich selbst erziehen im Bund mit Maria.

Neben den inhaltlichen Impulsen legen wir auch viel Wert auf gemeinschaftliche Aktionen, die das gemeinschaftliche Miteinander mit Freude beleben.

Mit der Zeit wachsen in einem Kreis tiefe Freundschaften und Verbundenheit unter den Mitgliedern (Jungmännern). Manche Kreise geben sich einen Namen, der ihr Ideal ausdrückt (z.B. Josefskreis) und damit ein Zeichen ihrer selbst ist.

Aus den momentan vier aktiven Kreisen findet sich eine Führungsgruppe, *Noahs Tafelrunde* genannt, zusammen. Mit Hilfe der erwachsenen Mitarbeiter sind diese für die Vorbereitung und Planung unserer Aktivitäten zuständig. Letzten Endes ist die Mitarbeit als Gruppenführer das Ziel der religiösen und pädagogischen Vorarbeit der Kreise. Nicht jedes Kreismitglied wird aber automatisch Gruppenführer.

<table class="table--list">
  <tr class="table__head">
    <th>Kreis</th>
    <th>Jahrgänge</th>
    <th>Kreisführer</th>
  </tr>
  <tr>
    <td>Kreis Thomas</td><td>1987–1988</td><td>Thomas Stey</td>
  </tr>
  <tr>
    <td>Die Pioniere</td><td>1988–1989</td><td>René Kohl</td>
  </tr>
  <tr>
    <td>Ritter Mariens</td><td>1990–1991</td><td>Benjamin Brähler</td>
  </tr>
  <tr>
    <td>Kreis Alexander</td><td>1991–1992</td><td>Alexander Dücker</td>
  </tr>
  <tr>
    <td>Kreis Matthias</td><td>1992–1993</td><td>Matthias Schott</td>
  </tr>
  <tr>
    <td>Kreis Ullrich</td><td>1994–1995</td><td>Ullrich Schott</td>
  </tr>
  <tr>
    <td>Kreis Tobias</td><td>1996–1997</td><td>Tobias Büdel</td>
  </tr>
  <tr>
    <td>Kreis Kilian</td><td>1998–1999</td><td>Kilian Machill</td>
  </tr>
  <tr>
    <td>Kreis Stefian</td><td>2000–2001</td><td>Steffen Büdel und Christian Schopp</td>
  </tr>
  <tr>
    <td>Kreis Jonas</td><td>2001–2002</td><td>Jonas Wolf</td>
  </tr>
</table>


## Kreis Ritter Mariens

<figure>
    <img src="https://farm1.staticflickr.com/601/23002392804_c870904fb2_z_d.jpg" />
    <figcaption>Ritter Mariens (2015)</figcaption>
</figure>
**Leiter:** Benjamin Brähler und Alexander Dücker

**Zu den Ritter Mariens gehören:**
Thomas Limbach, Christian Schopp, Kilian Machill, Philipp Müller, Niklas Abersfelder, Steffen Büdel, Christoph Schopp.

Wir sind ein Kreis in dem sich viele unterschiedliche Charaktere gefunden haben. Durch unsere Freundschaft ist in unserem Kreis ein enges Band der Verbundenheit entstanden. Wir versuchen, unser Kreisleben untereinander zu stärken. Deswegen haben wir den Ritter als Symbol unseres Kreises gewählt. Er drückt unsere Stärke im Glauben und unsere Treue zu Maria am Besten aus!

In den Jahren, in denen unser Kreis jetzt schon existiert haben wir viel voneinander und über unseren Glauben gelernt. Wir wollen auch in Zukunft aktiv bleiben und zu apostolischen Zeugen werden.

### Kreis Benjamin (2003–2008)

Unser Kreis entstand im Dezember 2003 vor unserem letzten Zeltlager. Damals waren wir noch über 10 Leute. Heute sind davon noch 8 übrig. Verbunden mir unserem Symbol, dem Ritter, sind wir die „Ritter Mariens“! In 2007 sind wir zusammen mit dem Kreis Alexander zu unserer ersten Kreisfahrt nach Schönstatt gefahren. Nachdem wir im Mai 2007 alle eine Gruppenleiterschulung absolvierten, ging es  auch schon los und 3 von uns sahen ihr erstes Zeltlager als Gruppenführer vor sich.

### Kreis Alexander (2005–2008)

2005 starteten wir unseren Kreis mit 14 Mitgliedern und unserem damaligen Kreisführer Tobias Kress. Im Januar des Jahres 2007 verließ Tobias uns, um ein Jahr in Australien zu verbringen. Sein Ersatz, Alex Dücker, übernimmt den Kreis bis heute. In 2007 sind wir zusammen mit dem Kreis Benjamin zu unserer ersten Kreisfahrt nach Schönstatt gefahren.

### Zusammenschluss (2008)

Seit 2008 arbeiten unsere Kreise zusammen, da beim Alexander-Kreis nur noch 3 aktive Mitglieder übrig geblieben waren. In 2008 waren 6 von uns als Gruppenleiter im Zeltlager dabei und der gesamte Kreis besuchte die Nacht des Heiligtums in Schönstatt. 2009 stellten wir 4 Gruppenleiter und 2 Joker beim Zeltlager in Schönstatt, ebenso einen ZBV.

Trotz der Tatsache, dass wir einer der älteren Kreise sind, ist es erfreulich, dass immer noch so viele von uns bei zahlreichen Aktionen, wie Zeltlager, GeWos, Regiofahrt, Fest des Glaubens, Nacht des Heiligtums usw. aktiv sind.


## Kreis Kilian

2013 gründete sich Kreis Kilian mit 8 Mitgliedern. Gemeinschaft, Unser Glaube und Ich-als-Gruppenleiter waren Themen auf verschiedenen Treffen. 2017 fusionierten wir dann mit Kreis Stefian da viele der Mitglieder weggebrochen waren.
Aktiv sind noch: Julian Mühr, Benedikt Ihrig, Nicolas Bockmühl, Jonathan Flohr.


## Kreis Stefian
<figure>
    <img src="https://farm2.staticflickr.com/1714/25341953573_f1f7ab430e_z_d.jpg" />
    <figcaption>Kreis Stefian (2016)</figcaption>
</figure>

<figure>
  <img src="https://farm1.staticflickr.com/478/19121402446_2fdea9b23f_z_d.jpg" />
  <figcaption>Kreisgottesdienst Kreis Stefian</figcaption>
</figure>

Wir gründeten unseren Kreis im September 2014.

Bei diesem Gründungswochenende waren wir insgesamt 14 Peronen. Mit dem Thema Gemeinschaft stiegen wir in unsere Kreisarbeit ein. Das Ziel von uns ist es eine starke Gemeinschaft zu werden und bei unseren regelmäßigen Treffen viel Spaß zu haben.
 
Da wir mit Christian und Steffen zwei Kreisleiter haben, entschieden wir, dass unser Kreis von nun an Kreis Stefian heißen soll.
In den letzten zwei Jahren haben wir viele tolle Dinge zusammen erlebt und über spannende Themen gesprochen. Wir haben uns die letzten Jahre sehr gut kennengelernt und es sind enge Freundschaften entstanden. Im Jahr 2016 waren schon zwei von uns als Gruppenleiter mit im Zeltlager. In diesem Jahr werden es mehr sein und wir gehen auch alle zusammen zur Gruppenleiterschulung nach Schönstatt. Im März diesen Jahres werden wir dann unser Mitarbeiterbündnis schließen. Wir freuen uns nun darauf, die SMJ aktiv mit zu gestalten.

# Senat

Im Senat, vormals auch *AHAk* (*Alte-Hasen-Arbeitskreis*) treffen die älteren Mitarbeiter zusammen, die keinem aktiven Kreis mehr angehören. Als lockere Gruppierung nehmen sie auch an Kreistagungen teil, unterstützen die Arbeit der jüngeren Gruppenleiter.

<figure>
  <img src="https://farm2.staticflickr.com/1482/25670115550_8fdd4887f8_z_d.jpg" />
  <figcaption>Senat beim Arbeitseinsatz (2016)</figcaption>
</figure>
