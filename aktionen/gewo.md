---
layout: page
cover_image: 
title: Gemeinschaftswochenenden
permalink: /gewo/
section: aktionen
cover_image: lagerfeuer.jpg
---

An den Gemeinschaftswochenenden bieten wir für Jungs von 8 bis 13 Jahren ein Freizeiterlebnis, das Spaß macht und Inhalte vermittelt. Jährlich finden drei solcher Wochenenden im Schönstattzentrum Dietershausen statt.
Im Vordergrund steht dabei das Erlebnis von Gemeinschaft, getragen vom christlichen Glauben. Mit den Gruppenleitern als Vorbild und Begleiter können die Jungen in Gemeinschaft Gleichaltriger ihre Grenzen und Fähigkeiten kennen lernen. Wichtig ist uns die Übermittlung christlicher Werte und die Erziehung zu freien Persönlichkeiten mit einer Festigkeit im Glauben. Als Methode dient dabei die Kentenich-Pädagogik.

Zusammen mit dem Zeltlager bilden die Gemeinschaftswochenenden unser Freizeitangebot für das Alter von 8 bis 13 Jahren. Die Gemeinschaftswochenenden und das Zeltlager stehen jedes Jahr unter einem Thema, das sich als roter Faden durchzieht. In diesem Rahmen bilden sie eine teilweise aufeinander aufbauende Einheit, aber jede einzelne Veranstaltung kann auch einzeln besucht werden.

Ältere Jungen ab 14 können in einen [Kreis]({{ site.baseurl }}/kreise) aufgenommen werden. Diese bestehen als feste Gruppe für meist ein oder zwei Jahrgänge. Ebenso gibt es für Ältere auch Einzelveranstaltungen wie das Regiozeltlager.

<a href="{{ site.baseurl }}/termine" class="btn">Aktuelle Terminübersicht</a>
<!-- TODO:  Noch mehr Content ? Termine?  -->

