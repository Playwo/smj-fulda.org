#! /usr/bin/env ruby

require 'jekyll-import'
require 'jekyll-import/importers/drupal_common'
require 'jekyll-import/importers/drupal6'
require 'sequel'
require 'flickraw'
require 'sanitize'
require 'pry'
require 'pastel'

class MigrateDrupalSMJArtikel
  # This module provides a base for the Drupal importers (at least for 6
  # and 7; since 8 will be a different beast). Version-specific importers
  # will need to implement the missing methods from the Importer class.
  #
  # The general idea is that this importer reads a MySQL database via Sequel
  # and creates a post file for each node it finds in the Drupal database.
  # include JekyllImport::Importers::DrupalCommon
  # extend JekyllImport::Importers::DrupalCommon::ClassMethods

  LIMIT = 250

  DEFAULTS = JekyllImport::Importers::DrupalCommon::ClassMethods::DEFAULTS

  attr_reader :options, :prefix

  def initialize(options)
    @options = DEFAULTS.merge(options)
    @prefix = @options.fetch('prefix')
  end

  def db
    @db ||= Sequel.mysql(options.fetch('dbname'),
                         user: options.fetch('user'),
                         password: options.fetch('password'),
                         host: options.fetch('host'),
                         encoding: 'utf8')
  end

  def process
    query = build_query

    conf = Jekyll.configuration({})
    src_dir = conf['source']

    dirs = {
      _posts: File.join(src_dir, '_posts').to_s,
      _drafts: File.join(src_dir, '_drafts').to_s,
      #    :_layouts => Jekyll.sanitized_path(src_dir, conf['layouts_dir'].to_s)
    }

    dirs.each do |_key, dir|
      FileUtils.mkdir_p dir
    end

    redirects = {}

    posts = db[query]
    posts.each_with_index do |post, i|
      @first_image_in_post = nil
      # Get required fields
      data, content = post_data(post)

      title = data['title'] = post[:title].strip.force_encoding('UTF-8')
      created_at = Time.at(post[:created])
      data['created_at'] = format_datetime(created_at)
      # data.delete 'created'
      data['excerpt'] = strip_html(strip_flickr_tags(post[:teaser])).gsub(/\s\s+/, ' ')

      # Get the relevant fields as a hash and delete empty fields
      data = data.delete_if { |_k, v| v.nil? || v == '' || v === /\A\s+\z/ }
      data = force_encoding(data)

      # Construct a Jekyll compatible file name
      is_published = post[:status] == 1
      node_id = post[:nid]
      dir = is_published ? dirs[:_posts] : dirs[:_drafts]
      slug = title.strip.downcase.gsub(/(&|&amp;)/, ' und ').gsub(/[\s\.\/\\]/, '-')
                  .gsub('ü', 'ue').gsub('ö', 'oe').gsub('ß', 'ss').gsub('ä', 'ae')
                  .gsub(/[^\w-]/, '').gsub(/[-_]{2,}/, '-').gsub(/^[-_]/, '').gsub(/[-_]$/, '')
      date = created_at.strftime('%Y-%m-%d')
      filename = "#{dir}/#{date}-#{slug}.md"
      permalink = "/artikel/#{date}-#{slug}/"

      # Write out the data and content to file
      File.open(filename, 'w') do |f|
        f.puts data.to_yaml(line_width: -1)
        f.puts '---'
        f.puts content
      end

      progress = "[#{i + 1}/#{posts.count}]"
      puts "#{pastel.green(progress)} created #{pastel.yellow(filename)}"

      # Make a file to redirect from the old Drupal URL
      redirects["node/#{post[:nid]}"] = permalink if is_published
      break if i >= LIMIT
    end

    puts "Migrated #{pastel.green(posts.count)} posts"

    create_redirects redirects
  end

  def create_redirects(node_redirects)
    redirects = {}
    node_redirects.each_pair { |node, permalink| redirects[permalink] = [node] }

    puts redirects.inspect

    db[aliases_query, node_redirects.keys].each do |r|
      puts r.inspect
      redirects[node_redirects[r[:source]]] << r[:alias]
    end

    unless File.exist? '.htacess'
      File.open('.htaccess', 'w') do |f|
        f.puts '# Redirects for URL scheme from old drupal installation'
        f.puts
        redirects.each_pair do |permalink, redirects|
          redirects.each do |redirect|
            f.puts "Redirect 301 #{redirect} #{permalink}"
          end
        end
        f.puts ''
      end
    end
  end

  def build_query
    query = <<EOS
            SELECT n.nid,
                   n.title,
                   nr.body,
                   nr.teaser,
                   n.created,
                   n.status,
                   n.type,
                   GROUP_CONCAT( td.name SEPARATOR '|' ) AS 'tags',
                   img.field_teaser_image_embed AS 'teaser_image',
                   tagline.field_tagline_value AS 'tagline',
                   alias.dst AS alias,
                   u.name AS username,
                   u.uid AS uid
            FROM node_revisions AS nr,
                 node AS n
                 LEFT OUTER JOIN term_node AS tn ON tn.nid = n.nid
                 LEFT OUTER JOIN term_data AS td ON tn.tid = td.tid
                 LEFT OUTER JOIN content_field_teaser_image AS img ON img.nid = n.nid
                 LEFT OUTER JOIN content_field_tagline AS tagline ON tagline.nid = n.nid
                 LEFT OUTER JOIN url_alias AS alias ON alias.src = CONCAT('node/', n.nid)
                 LEFT OUTER JOIN users AS u ON u.uid = n.uid
            WHERE type = 'story'
              AND n.vid = nr.vid
            GROUP BY n.nid
EOS
  end

  def aliases_query
    'SELECT src AS source, dst AS alias FROM url_alias WHERE src IN ?'
  end

  def post_data(sql_post_data)
    content = sql_post_data[:body].to_s.force_encoding('UTF-8')
                                  .gsub(%r{(</(?:p|h4|h3|h2|h1|div|ul)>) *}, "\\1\n")
                                  .gsub("\r\n", "\n").gsub(/\s+\n/, "\n")
                                  .gsub('<p>&nbsp;</p>', '')

    content = expand_flickr_tags content

    tags = (sql_post_data[:tags] || '').strip

    data = {
      'categories' => tags.split('|').compact.uniq.map { |c| c.force_encoding('UTF-8') },
      'migrated' => {
        'node_id' => sql_post_data[:nid],
        'migrated_at' => format_datetime,
        'alias' => sql_post_data[:alias].to_s,
        'user_id' => sql_post_data[:uid]
      },
      'tagline' => strip_html(sql_post_data[:tagline]),
      'image' => resolve_flickr_photo(sql_post_data[:teaser_image].to_s) || resolve_flickr_photo(@first_image_in_post)
    }

    puts pastel.bright_blue(data['tagline'].inspect) if data['tagline'] && (data['tagline'] != '')

    username = sql_post_data[:username].to_s
    data['author'] = username unless username == 'straight-shoota' # ignore default user

    [data, content]
  end

  def pastel
    @pastel ||= Pastel.new
  end

  def format_datetime(time = Time.now)
    time.strftime('%Y-%m-%d %H:%M')
  end

  def strip_html(html)
    Sanitize.fragment(
      html.to_s.force_encoding('UTF-8').gsub('<p>&nbsp;</p>', ''),
      Sanitize::Config.merge(
        Sanitize::Config::BASIC,
        elements: Sanitize::Config::BASIC[:elements] - ['p'],
        remove_contents: %w(h1 h2 h3 h4 h5 h6)
      )
    ).strip.gsub(/  +/, ' ')
  end

  def expand_flickr_tags(content)
    content = expand_flickr_photo(content)
    content = expand_flickr_photoset(content)
  end

  def expand_flickr_photo(content)
    content.gsub(%r{(?:<p>\s*)?\[photo id=(?:"|&quot;)?(\d+)[^\]]*\](?:\s*</p>)?}) do |_match|
      begin
        photo = flickr.photos.getInfo(photo_id: Regexp.last_match(1))
      rescue FlickRaw::FailedResponse => exc
        puts pastel.magenta(exc.to_s) + ' photo_id=' + pastel.cyan(Regexp.last_match(1))
        next
      end

      @first_image_in_post ||= photo

      url = FlickRaw.url_c(photo)
      date_taken = photo.dates.taken
      year_taken = date_taken[0..3]
      url_photopage = FlickRaw.url_photopage(photo)
      caption = flickr_photo_caption(photo)

      %(<figure data-href="#{url_photopage}">\n) +
        %(  <img src="#{url}" class="flickr-img flickr-img--medium" alt="#{caption}" />\n) +
        %{  <figcaption>#{caption} (<time datetime="#{date_taken}">#{year_taken}</time>)</figcaption>\n} +
        %(</figure>)
    end
  end

  def expand_flickr_photoset(content)
    content.gsub(%r{(?:<p>\s*)?\[(?:flickr-)?photoset[: ]id=(?:"|&quot;)?(\d+)[^\]]*\](?:\s*</p>)?}) do |_match|
      album = flickr.photosets.getInfo(photoset_id: Regexp.last_match(1))
      photos = flickr.photosets.getPhotos photoset_id: Regexp.last_match(1)

      puts "loaded album ##{album.id} #{album.title} with #{album.photos} photos"

      url = FlickRaw.url_photoset(album)
      title = album.title

      html = ''
      html << %(<figure class="flickr-photoset full-width" data-href="#{url}">\n)
      photos.photo.take(14).each do |photo|
        photo = flickr.photos.getInfo(photo_id: photo.id)

        @first_image_in_post ||= photo

        img_url = FlickRaw.url_q(photo)
        large_url = FlickRaw.url_b(photo)
        photo_url = FlickRaw.url_photopage(photo) + '/in/album-' + album.id
        alt = flickr_photo_caption photo
        # alt = "Bildergalerie" if !alt || alt.empty?
        html << %(  <a href="#{photo_url}"><img src="#{img_url}" alt="#{alt}" data-src-large="#{large_url}" /></a>\n)
      end
      html << %(  <a href="#{url}" class="flickr-link" title="Bildergalerie #{title} auf Flickr">Bildergalerie</a>\n)
      html << %(</figure>)
      html
    end
  rescue FlickRaw::FailedResponse => exc
    puts pastel.magenta(exc.to_s) + pastel.cyan(Regexp.last_match(1))
    content
  end

  def flickr_photo_caption(photo)
    caption = photo.title unless photo.title.start_with? 'DSC', 'IMG', 'CIMG'
    caption ||= photo.description if photo.respond_to? :description
    caption.strip if caption
  end

  def strip_flickr_tags(content)
    content.gsub(%r{(?:<p>\s*)?\[photo(?:set)? id=(?:"|&quot;)?(\d+)[^\]]*\](?:\s*</p>)?}, '')
  end

  def resolve_flickr_photo(idstring)
    photo_id = idstring.respond_to?(:id) ? idstring.id : idstring.to_s[/\d+/]

    idstring = nil if idstring == ''

    return idstring unless photo_id

    puts "Retrieving photo_id #{pastel.cyan(photo_id)} from Flickr."

    FlickRaw.url_b flickr.photos.getInfo(photo_id: photo_id)
  end

  def force_encoding(data)
    data.each_pair do |_k, v|
      case v
      when String
        v.force_encoding('UTF-8')
      when Hash
        force_encoding(v)
      else
        v
      end
    end
  end
end

DRUPAL_CONFIG = {
  'dbname' => 'smjfulda',
  'user' => 'smjfulda',
  'password' => ENV['SMJ_FULDA_DRUPAL_PASSWORD'],
  'host' => 'smj-fulda.org'
}.freeze

FlickRaw.shared_secret = ENV['FLICKRAW_SHARED_SECRET']
FlickRaw.api_key = ENV['FLICKRAW_API_KEY']

MigrateDrupalSMJArtikel.new(DRUPAL_CONFIG).process
