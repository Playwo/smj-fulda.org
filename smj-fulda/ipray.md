---
title: iPray – Lobpreistour der Schönstatt Mannesjugend Fulda
permalink: ipray/
---

<figure class="full-width">
  <blockquote>
    <p>Habt Vertrauen, ich bin es; fürchtet euch nicht!</p>
    <footer><cite>Mt 14,27</cite></footer>
  </blockquote>
</figure>

<figure>
  <img src="{{ site.baseurl }}/images/pages/ipray.jpg" />
</figure>
<figure>
  <img src="https://farm8.static.flickr.com/7241/7335293310_9910e314da_z.jpg" />
</figure>
<figure>
  <img src="https://farm8.static.flickr.com/7221/7335292918_627f0f3ed3_z.jpg" />
</figure>

Für viele Menschen ist die Kirche ein Ort der Sonntags nur missmutig aufgesucht oder gar nicht mehr besucht wird. Der Glaube spielt in ihrem Leben keine ernste Rolle mehr. Sie flüchten sich in Arbeit, Stress und kurzlebige Dinge des Alltags. Dabei ist Stille, Besinnung und ein fester Halt genauso wichtig.

Wir sind eine Gruppe innerhalb der Schönstatt Mannesjugend Fulda. 2012 riefen wir diese Initiative ins Leben. Seit dem veranstalten wir Lobpreise in Gemeinden und Pfarreien unseres Bistums und darüber hinaus Als Thema haben wir uns „Vertrauen“ ausgesucht. Mit verschiedenen Elementen ermöglichen wir Vertrauen zu Gott und zu den Menschen neu zu erleben.

Gestärkt durch unsere Arbeit und unseren Glauben, soll dies Beitrag sein für die junge Kirche und eine Antwort geben auf den Auftrag Christi: „Geht hinaus und verkündet allen das Evangelium“ (Mk 16,15)
