---
layout: page
permalink: 50jahre/
title: 50 Jahre SMJ-Fulda
cover_image: kapellchen50.jpg
---

## Feste muss man feiern, wie sie kommen...

<figure>
  <img src="https://farm5.static.flickr.com/4064/4225903967_6e216400a8_z.jpg" />
  <figcaption>Pater Josef Kentenich unterzeichnet die Gründungsurkunde der SMJ Fulda</figcaption>
</figure>
<figure>
  <img src="https://farm5.staticflickr.com/4196/34737783021_5b68203dc3_z.jpg"/>
  <figcaption>SMJ Fulda</figcaption>
</figure>

Die Schönstatt-Mannesjugend im Bistum Fulda wurde im Jahre 1967 anlässlich des Besuches von Pater Kentenich in Dietershausen gegründet. Dieser Moment ist in unserer Gründungsurkunde festgehalten, die seither von Diözesanführer zu Diözesanführer weitergereicht wird.

Was damals entstanden ist, ist heute ebenso aktuell und wir haben als SMJ [vieles erlebt](/geschichte). In diesen 50 Jahren sind zahlreiche Jungen aus dem ganzen Bistum in unsere Arbeit involviert gewesen, haben an Zeltlagern, Gemeinschaftswochenenden und -tagen teilgenommen, sind in Kreisen zu jungen Männern herangewachsen. Dieses Jubiläum wollen wir gerne gemeinsam feiern mit Aktiven und Ehemaligen sowie unseren Unterstützern und Freunden. Wir wollen Dank sagen für alles, was in dieser Zeit durch unsere Jugendarbeit geschehen ist.

Dazu laden wir alle, die sich mit uns verbunden fühlen, ein und bitten, diese Einladung gerne auch weiterzureichen, damit niemand vergessen wird.

Die Feierlichkeiten beginnen am<br/>
<time style="display: block; padding-left: 2.5rem;"><strong>26. August 2017 um 15:00 Uhr</strong></time>
im<br/>
<address style="display: block; padding-left: 2.5rem;"><strong>Schönstattzentrum Dietershausen</strong><br/>
Josef-Engling-Haus<br/>
Marienhöhe 1<br/>
36093 Künzell-Dietershausen</address>

<figure>
  <img src="https://farm5.staticflickr.com/4035/4250957688_aede2b2dd2_z.jpg" />
  <figcaption>Jubiläums-Feier</figcaption>
</figure>
<figure>
  <img src="https://farm5.staticflickr.com/4006/4285142600_c6ea3a94ee_z.jpg" />
  <figcaption>Liebesbündnis</figcaption>
</figure>

### Festprogramm

* 15:00 Heilige Messe
* 16:00 Begegnung und Rückblick
* 18:00 Festkommers
* 20:00 Lagerfeuerrunde
* 23:30 Nachtgebet im Heiligtum
* Ausklang

### Anmeldung
Im Teilnehmerbeitrag von 9 € ist das Abendessen bereits eingeschlossen. Gerne freuen wir uns auch über eine zusätzliche Spende zur Finanzierung dieser Feierlichkeit.

Zur besseren Planung bitten wir um eine kurze Anmeldung bis zum 18. August an [✉jubilaeum@smj-fulda.org](mailto:jubilaeum@smj-fulda.org).

<figure class="full-width">
  <img src="https://farm3.staticflickr.com/2768/4223110077_5d7b883a1d_o_d.jpg" />
  <figcaption>Du bist SMJ. Dann komm auch mit uns feiern!</figcaption>
</figure>
