* Fehlende Seiten
  * ✓ Kontakt
  * ✓ Impressum
  * ✓ Adventskalender
  * ✓ iPray

* ✓ Artikel und Termine übertragen
* ? URL-Weiterleitungen
* ? Flickr Integration
* Texte überarbeiten
* ✓ Google-Calendar Integration
* Facebook-Integration??
* Slack Integration
* Suche (Algolia?)
* Bilder-Galerien

* Design-Überarbeitung: Layout Homepage
