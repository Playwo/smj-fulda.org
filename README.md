Code repository für die Website https://smj-fulda.org/

Die statische Website wird mit [Jekyll](https://jekyllrb.com) gebaut.

# Deployment

Bei jeder Aktualisierung des `master` Branches wird die Website per Gitlab CI neu generiert und auf https://smj-fulda.org/ veröffentlicht.
Das Hosting liegt auf einem Account bei https://all-inkl.com

## files.smj-fulda.org

Einige Dateidownloads (Lagerzeitungen, Flyer) sind aus Platzgründen in einem anderen Repository ausgelagert: https://gitlab.com/smj-fulda/files.smj-fulda.org
Diese Dateien sind auf gleicher Art und Weise unter der Domain https://files.smj-fulda.org veröffentlicht.

# Usage

Die Website besteht weitgehend aus einzelnen HTML- oder Markdown-Dateien.
Zum Bearbeiten brauch es prinzipiell nur einen Text-Editor. Das geht auch per online-IDE auf gitlab.com.
Sobald eine Änderung auf den `master` Branch kommt, wird die Website automatisch aktualisiert.

Für eine lokale Vorschau von Änderungen kann die Website mit Jekyll gebaut werden ([Installationsanweisungen](http://jekyllrb.com/docs/installation/)).
Nach der Installation von Jekyll kann man mit `jekyll serve` einen lokalen Webserver starten.

