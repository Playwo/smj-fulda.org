---
categories: []
migrated:
  node_id: 786
  migrated_at: 2017-02-21 14:40
  alias: artikel/786-silvester-im-schatten-des-heiligtums
  user_id: 9
tagline: SMJ-Silvesterparty 2009 in Dietershausen
image:
  cover: https://farm5.staticflickr.com/4017/4250806903_d2a3a97e45_b.jpg
author: Sebastian Hartmann
title: Silvester im Schatten des Heiligtums
created_at: 2010-01-05 15:47
excerpt: 31.12.2009 ein Jahr neigt sich zu Ende. Die einen feiern dies in Nobel-Restaurants, die anderen bei gigantischen Feten wie in Berlin am Brandenburger Tor. Doch ein kleiner Kreis von Feierfreudigen trudelt gegen 20.15 Uhr gemütlich mit Schlafsack und Isomatte, Salaten und Bowle sowie Feuerwerkskörpern in Dietershausen ein. Am Empfang treiben Tobias Büdel und Matthias Schott den Unkostenbeitrag für das Mega-Event ein und erstatten das Rückgeld in Form von Checkkartenhüllen.
---
<p>31.12.2009 ein Jahr neigt sich zu Ende. Die einen feiern dies in Nobel-Restaurants, die anderen bei gigantischen Feten wie in Berlin am Brandenburger Tor. Doch ein kleiner Kreis von Feierfreudigen trudelt gegen 20.15 Uhr gemütlich mit Schlafsack und Isomatte, Salaten und Bowle sowie Feuerwerkskörpern in Dietershausen ein. Am Empfang treiben Tobias Büdel und Matthias Schott den Unkostenbeitrag für das Mega-Event ein und erstatten das Rückgeld in Form von Checkkartenhüllen.</p>
<h2>Buntes Programm</h2>
<p>Über 50 &bdquo;Mann&ldquo; und zwei Schwestern sitzen erwartungsvoll im Stuhlkreis &ndash; der ein oder andere hat sich schon mit Bowle oder Bierflasche versorgt. Kilian und Johannes begrüßen &bdquo;Alle, die&ldquo; da waren. Zwischendurch gab es was auf die Ohren von einer Jossgründer Heavy Xylophon-Formation und der Mädchenjugend.</p>
<figure data-href="https://www.flickr.com/photos/45962678@N06/4250806903">
  <img src="https://farm5.staticflickr.com/4017/4250806903_d2a3a97e45_c.jpg" class="flickr-img flickr-img--medium" alt="Adrian wartet auf sein Herzblatt" />
  <figcaption>Adrian wartet auf sein Herzblatt (<time datetime="2009-12-31 22:56:17">2009</time>)</figcaption>
</figure>
<p>Viel bequemer als in der ARD ging es im Anschluss zu, als Adrian Vogler sein &bdquo;Herzblatt&ldquo; suchte und sich letztlich für Milena aus der Mädchenjugend entschied, die sich als 50-jähriger Hörgeräteakustiker ausgab. Der Herzblatthubschrauber brachte die beiden direkt zum Buffet, das reichhaltig mit Brötchen, Frikadellen und Salaten gedeckt war. Während die SMJ genüsslich die Frikadellen vernichtete und die Mädchenjugend die Käsebrötchen aß, wurde der 80. Geburtstag von Miss Sophie gefeiert. Es war weit mehr als ein &bdquo;Dinner for one&ldquo;.</p>
<p>Film aus und ab gings auch schon in die Gott Vater Kirche wo viele Schwestern und einige Schönstattfamilien warteten. Kaplan Florian Böth zelebrierte die wohl wirklich letzte Messe im Jahr 2009. Viele Liedstrophen wurden weggelassen, damit wir den Jahreswechsel nicht verpassten. Einzig an Segen und Segenswünschen wurde nicht gespart. Nun konnte das neue Jahr kommen.</p>
<figure class="flickr-photoset full-width" data-href="https://www.flickr.com/photos/45962678@N06/sets/72157623028499803">
  <a href="https://www.flickr.com/photos/45962678@N06/4251573366/in/album-72157623028499803"><img src="https://farm5.staticflickr.com/4066/4251573366_3f0b16255e_q.jpg" alt="Julia & Judith" data-src-large="https://farm5.staticflickr.com/4066/4251573366_3f0b16255e_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4250800921/in/album-72157623028499803"><img src="https://farm3.staticflickr.com/2669/4250800921_71c284ff7a_q.jpg" alt="Kicker" data-src-large="https://farm3.staticflickr.com/2669/4250800921_71c284ff7a_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4250801061/in/album-72157623028499803"><img src="https://farm3.staticflickr.com/2692/4250801061_0397a6074c_q.jpg" alt="Isabelle" data-src-large="https://farm3.staticflickr.com/2692/4250801061_0397a6074c_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4250801241/in/album-72157623028499803"><img src="https://farm3.staticflickr.com/2706/4250801241_3880c689d0_q.jpg" alt="Ludwig & Lorena am Kicker" data-src-large="https://farm3.staticflickr.com/2706/4250801241_3880c689d0_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4250801387/in/album-72157623028499803"><img src="https://farm5.staticflickr.com/4036/4250801387_9e98664b8e_q.jpg" alt="Anne & Lorena" data-src-large="https://farm5.staticflickr.com/4036/4250801387_9e98664b8e_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4250801585/in/album-72157623028499803"><img src="https://farm5.staticflickr.com/4024/4250801585_fb9f77bcc3_q.jpg" alt="Leona, Johanna & Milena" data-src-large="https://farm5.staticflickr.com/4024/4250801585_fb9f77bcc3_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4251574268/in/album-72157623028499803"><img src="https://farm3.staticflickr.com/2787/4251574268_ac2bcea730_q.jpg" alt="Christin & Julia" data-src-large="https://farm3.staticflickr.com/2787/4251574268_ac2bcea730_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4250801865/in/album-72157623028499803"><img src="https://farm3.staticflickr.com/2680/4250801865_82b8e27d0b_q.jpg" alt="Ellen & Isabelle" data-src-large="https://farm3.staticflickr.com/2680/4250801865_82b8e27d0b_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4250802129/in/album-72157623028499803"><img src="https://farm5.staticflickr.com/4070/4250802129_5eedfb8911_q.jpg" alt="Die Wächtersbacher" data-src-large="https://farm5.staticflickr.com/4070/4250802129_5eedfb8911_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4251574786/in/album-72157623028499803"><img src="https://farm5.staticflickr.com/4024/4251574786_049777f3ec_q.jpg" alt="Thomas & Steffi" data-src-large="https://farm5.staticflickr.com/4024/4251574786_049777f3ec_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4250802345/in/album-72157623028499803"><img src="https://farm3.staticflickr.com/2700/4250802345_5062d369d3_q.jpg" alt="Dennis aka Rainer" data-src-large="https://farm3.staticflickr.com/2700/4250802345_5062d369d3_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4251575010/in/album-72157623028499803"><img src="https://farm3.staticflickr.com/2728/4251575010_c579e7a1a4_q.jpg" alt="Wegweiser" data-src-large="https://farm3.staticflickr.com/2728/4251575010_c579e7a1a4_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4250802581/in/album-72157623028499803"><img src="https://farm5.staticflickr.com/4068/4250802581_6ed4ae8b37_q.jpg" alt="Wegweiser" data-src-large="https://farm5.staticflickr.com/4068/4250802581_6ed4ae8b37_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4250802711/in/album-72157623028499803"><img src="https://farm5.staticflickr.com/4059/4250802711_30dbe7e7f8_q.jpg" alt="Johannes & Kilian" data-src-large="https://farm5.staticflickr.com/4059/4250802711_30dbe7e7f8_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/sets/72157623028499803" class="flickr-link" title="Bildergalerie Silvester im Schatten des Heiligtums auf Flickr">Bildergalerie</a>
</figure>
<div class="section"><h3>Countdown</h3>
<p>Vor dem Kapellchen versammelten sich alle und dann kam der Count Down.</p>
<p>10, 9, 8, 7, 6, 5, 4, 3, 2, 1&hellip;</p>
<p><strong>Frohes neues Jahr 2010.</strong></p>
<p>Doch vor dem Feuerwerk und den vielen Umarmungen stand das Bonifatiuslied mit Zicke-Zacke. Danach wurde kräftig mit Sekt angestoßen und die Raketen gen Himmel geschossen. Eine halbe Stunde dauerte es bis der letzte Kracher gezündet war und die letzten Neujahrswünsche ausgesprochen wurden.</p>
<p>Der Kälte und dem Glatteis entfliehend zog man sich in das Jugendheim zurück. Dort ging die große Party weiter. Es wurde allerhand Schnupp verteilt (der NICHT aus dem Schnuppschrank unter der Spühle der Mädchenjugend war!) und aus den Froggerschen Lautsprechern lief gute Musik.</p>
</div>
<h2>&bdquo;Und ein neuer Morgen/Jahr&ldquo;</h2>
<div class="section"><p>Einzelheiten des Abends sollen an dieser Stelle nicht bekannt werden nur so viel:</p>
<ol> <li>Nicht alle hatten den Mut, in Dietershausen zu übernachten</li> <li>Nicht alle mussten übernachten &ndash; einige Tapfere haben durchgemacht</li> <li>Die letzten Lieder, die ich mitbekommen habe waren &bdquo;Only you&ldquo; und &bdquo;Love me tender&ldquo; und liefen gegen 6.30 Uhr vom Band. Sehr zur Freude einiger Mädchenjugendmitglieder und Wegweiser auf dem Sofa.</li> <li>Für einige sah es so aus, als ob über Nacht die Heinzelmännchen da waren</li> </ol></div>
<p>Nach kurzem bis gar keinem Schlaf hieß es dann Frühstücken (Das Frühstück war wirklich weltklasse, allein der Berg Hackfleisch öffnete das Herz und versprach ein gutes Jahr 2010) und ab gings nach Hause. Die Heimfahrt sollte für einige recht lange dauern, da neben Mannesjugend, Mädchenjugend, KJF, Maltesern, Freunden und Freundinnen auch eine große Gruppe der Wegweiser da waren.</p>
<div class="section"><p>Ich möchte an dieser Stelle noch einmal im Namen aller Partygäste Johannes und Kilian für die tolle Organisation danken, sowie den Schwestern für die Location, dem Cateringservice (all denen, die Essen und Trinken vorbereitet haben), den Bands, unserem Herzblatttraumpaar, allen Anwesenden &ndash; es hat richtig Spaß gemacht mit euch in das Jahr zu starten.</p>
<p>Auf ein gutes Jahr 2010 freut sich</p>
<p>euer Bonkos</p>
</div>
<div class="video-content"><h3>Slideshow</h3>
<object id="vp12OP3w" width="648" height="360" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"><param name="movie" value="http://static.animoto.com/swf/w.swf?w=swf/vp1&e=1282554599&f=2OP3w06Alf0V4coszC9XEA&d=237&m=a&r=w+s&i=m&ct=&cu=&options=start_hq"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed id="vp12OP3w" src="http://static.animoto.com/swf/w.swf?w=swf/vp1&e=1282554599&f=2OP3w06Alf0V4coszC9XEA&d=237&m=a&r=w+s&i=m&ct=&cu=&options=start_hq" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="648" height="360"></embed></object></div>
