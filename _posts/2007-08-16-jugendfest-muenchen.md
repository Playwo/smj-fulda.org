---
categories:
- Jugendfest
- München
- '2007'
migrated:
  node_id: 686
  migrated_at: 2017-02-20 22:25
  alias: artikel/686-jugendfest-münchen
  user_id: 1
tagline: Zukunft - beleben!
title: Jugendfest München
created_at: 2007-08-16 16:16
excerpt: "<em>Zukunft - beleben!</em> hieß es für fünf SMJler am 07.06.07, als man sich mittags mit dem Auto und ab Gemünden mit dem Zug gen Süden aufmachte. Mit im Gepäck waren neben guter Laune und Proviant auch zahlreiche Erwartungen, denn die Erinnerungen an Schönstatt 2005 wurden wieder wach. Abends kamen wir ziemlich müde und abgeschlagen am Münchner Schönstattkapellchen an. Nachdem die Unterkunft bezogen war, erkundeten wir erstmal - wie sich das gehört - das Münchner Nachtleben."
---
<div class="section"><p><em>Zukunft - beleben!</em> hieß es für fünf SMJler am 07.06.07, als man sich mittags mit dem Auto und ab Gemünden mit dem Zug gen Süden aufmachte. Mit im Gepäck waren neben guter Laune und Proviant auch zahlreiche Erwartungen, denn die Erinnerungen an Schönstatt 2005 wurden wieder wach.</p>
<p>Abends kamen wir ziemlich müde und abgeschlagen am Münchner Schönstattkapellchen an. Nachdem die Unterkunft bezogen war, erkundeten wir erstmal - wie sich das gehört - das Münchner Nachtleben.</p>
</div>
<div class="section">
<h3>Freitag</h3>
<p>Im Gegensatz zu den vielen Helfern durften wir "ausschlafen" und entschlossen uns München bei Tag anzuschauen, bevor der Festbetrieb losging. gegen Nachmittag kamen dann auch die restlichen Gäste auf das Festgelände und mit einer Intro-Veranstaltung begann das große Jugendfest. Die Band stellte das selbstgeschriebene Juigendfestlied "Zukunft beleben" vor, von dem wir unbedingt eine CD wollten... . Am Abend sorgte die SMJ-Fulda für die Sicherheit der Gäste und natürlich hatten wir jede Menge Spaß mit den Funkgeräten, mit denen man noch besser bescheid wusste, als sowieso schon.</p>
</div>
<div class="section">
<h3>Samstag</h3>
<p>Eine große Talkrunde stimmte auf das Thema Zukunft ein. Danach gab es jede Menge Workshops. Wir entschieden uns für Volleyball...und mussten feststellen, dass es sich mit der Faust mind. genausogut spielt wie mit beiden Händen. Das Volleyballspiel bei strahlendem Sonnenschein war auch dringend nötig, um seine Klamotten in der Sonne trocknen zu lassen. Was will man machen, wenn die Münchner ein lebensgrußes Logo ihres Festes mit Wasser aufbauen... . Der feierliche Gottesdienstmit Kardinal Wetter war zweifelsfrei der TOP ACT! Bei den Schlussliedern hatte man eher das Gefühl auf einem Konzert der Red hot ChiliPeppers zu sein. Am Abend machten wir dann die Runde durch Frankenzelt, Bayernzelt, Schwabenzelt und Partybus. Ach ja.... nicht zu vergessen, es gab da noch jemanden unter uns, dem dieser Abend sicherlich noch länger in Erinnerung bleiben wird - gell Tobi?! Unser Glückspilz Tobias Büdel gewann den Rundflug über München bei der Verlosung. Als dann die Lichter am Festplatz ausgingen und die SMJ-Fulda ihr Bonifatiuslied gesungen hatte, hieß es "Schlafplatz" aufsuchen. "Schlafplatz" ist dabei ein dehnbarer Begriff.</p>
</div>
<div class="section">
<h3>Sonntag</h3>
<p>Bitte wer sollte das alles essen? Die Münchner Schönstattfamilien haben sich mit Kaiserschmarn, Waffeln, Eiern,.... ordentlich ins Zeug geschmissen und einen gigantischen Brunch aufgebaut. Leider wurde der letzte Tag von einem traurigen Ereignis überschattet. Da eine Mädchenleiche in der Nähe des Festgeländes gefunden wurde, musste die Polizei die Veranstaltung unterbrechen und nach Zeugen suchen. Wie sich später herausstellte hatte der Fall aber nichts mit dem Fest zu tun. Mit einem etwas bitteren Beigeschmack und der traurigen Seite der Zukunft vor Augen, endete das Fest in einem Abschlussakt mit Musik und einem Einzelsegen vor dem Kapellchen. Die Heimfahrt gestaltete sich auf Grund von Bauarbeiten an Gleisen und vollen Zugen etwas schwieriger als gedacht.</p>
</div>
<div class="section">
<p>Müde und dennoch überwältigt von dem absolut genialen und gelungenen Wochenende (an dieser Stelle Lob an die Münchner Schönstattjugend, das war der Hammer!!!) fielen wir in unsere Betten...Fuuulda hatte uns wieder!</p>
<p>Was bleibt sind tolle und nachdenkliche Erinnerungen.</p>
<p>Lasst uns die Zukunft im Land des heiligen Bonifatius beleben!</p>
<p>Bilder gibt es unter: <a href="http://www.jugendfest-muenchen.de">www.jugendfest-muenchen.de </a></p>
</div>
