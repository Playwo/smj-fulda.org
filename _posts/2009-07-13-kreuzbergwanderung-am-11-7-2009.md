---
categories:
- SMJ-Fulda
- Kreuzbergwanderung
- Kreuzberg
- Gersfeld
- dietershausen
- Bonifatiuslied
migrated:
  node_id: 188
  migrated_at: 2017-02-20 22:25
  alias: artikel/188-kreuzbergwanderung-am-11.7.2009
  user_id: 1
title: Kreuzbergwanderung am 11.7.2009
created_at: 2009-07-13 01:39
excerpt: Samstagmorgen 9:00 in Dietershausen, einige wagemutige SMJler und deren Freunde finden sich zum Aussendungsgebet im Kapellchen ein, um dann die Expedition auf den Kreuzberg zu starten. Die Wanderung beginnt gegen 9:15. Unser erstes größeres Etappenziel ist Gersfeld. Die ganze Strecke ist etwa 30 Kilometer lang. Viele werden sich natürlich fragen, warum wir uns jedes Jahr diese Tortur antun. Die Antwort auf die Frage ist ganz einfach. Das Gemeinschaftsgefühl wird gestärkt und wir alle kommen an unsere körperlichen Grenzen.
---
<p>Samstagmorgen 9:00 in Dietershausen, einige wagemutige SMJler und deren Freunde finden sich zum Aussendungsgebet im Kapellchen ein, um dann die Expedition auf den Kreuzberg zu starten. Die Wanderung beginnt gegen 9:15. Unser erstes größeres Etappenziel ist Gersfeld. Die ganze Strecke ist etwa 30 Kilometer lang. Viele werden sich natürlich fragen, warum wir uns jedes Jahr diese Tortur antun. Die Antwort auf die Frage ist ganz einfach. Das Gemeinschaftsgefühl wird gestärkt und wir alle kommen an unsere körperlichen Grenzen. Diese Erfahrung ist einfach genial und jeder sollte sie einmal gemacht haben.</p>
<p>In Gersfeld ist mehr als die Hälfte des Weges absolviert und wir legen erst mal eine längere Pause ein. Danach geht es über Sparbrod und den Arnsberg den Kreuzberg hinauf. Der schwerste Teil der Strecke wartet nun noch auf uns, aber auch diesen meistern wir. Am Arnsberg trennt sich dann die Spreu vom Weizen, da nicht alle die gleiche Geschwindigkeit laufen können. Wir treffen uns jedoch alle oben am Gipfel an der Kreuzgruppe. Die Letzten treffen etwa eine Stunde später als die Ersten ein, aber das ist nur nebensächlich. Die Hauptsache ist, dass wir es alle geschafft haben. Der Eine mit mehr körperlicher Anstrengung als der Andere.</p>
<p>Nach dem traditionellen Singen des Bonifatiusliedes geht es die Stufen wieder runter. Dann suchen wir einen Platz und genehmigen uns das wohlverdiente Bier. So verbringen wir noch einige Stunden und lassen uns am Abend abholen. Die Anstrengung hat sich wieder geholt. Auf ein Neues im nächsten Jahr!!!</p>
