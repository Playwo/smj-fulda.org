---
title: In 8 Tagen um die Welt
tags: [zeltlager]
image:
  cover: https://farm4.staticflickr.com/3857/14872166676_6a4287f1cd_o_d.jpg
author: Christian Schopp
excerpt: >
  Nach dem Lager ist vor dem Lager. 2019 findet unser Zeltlager vom 30. Juni bis 7. Juli statt.
---

Konfuzius sagt: *Nach dem Lager ist vor dem Lager.*

Zwar ist das Zeltlager 2018 schon zu Ende, dafür dürfen wir aber auf das nächste Jahr blicken. Dieses Mal gibt es eine verkürzte Version unserer beliebten Ferienfreizeit.

Traditionell zelten wir am Anfang der hessischen Sommerferien mit Jungs zwischen 9 und 13 Jahren. Los geht es am 30. Juni 2019. Bis zum 7. Juli verbringen wir acht einzigartige Tage auf dem Zeltplatz.
Hoffentlich auch mit so gutem Wetter, wie dieses Jahr. Wir freuen uns auf alle Fälle auf das Highlight der SMJ Fulda im Jahr 2019.

Auch das Thema steht schon fest: Unter dem Motto *In 8 Tagen um die Welt* warten Geländespiel, Lagerfeuer, Nachtwanderung und eine Zeit voller unvergesslicher Momente auf uns. Das Zeltlager 2019 werden wir wieder auf einem Zeltplatz in der Rhön verbringen.

