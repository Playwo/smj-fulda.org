---
categories:
- Gemeinschaftswochenenden
- Thomas Renze
- Saujagd
- gemeinschaftswochenende
- Casino
- Asterix
- '2011'
migrated:
  node_id: 858
  migrated_at: 2017-02-20 22:26
  alias: artikel/858-gemeinschaftswochenende-i
  user_id: 132
tagline: Asterix erobert Rom! Wo führt uns der Weg hin?
image:
  cover: https://farm6.staticflickr.com/5294/5462898108_5fd4cd6a3e_b.jpg
author: Steffen Büdel
title: Gemeinschaftswochenende I
created_at: 2011-02-20 15:18
excerpt: Das erste Gemeinschaftswochenende der SMJ Fulda 2011 begann am Freitag den 18. Februar mit dem Abendessen. Nach anschließenden Kennenlernspielen wurde noch bis in den späten Abend gespielt. Die Spiele, die den meisten gefielen, waren Schrubber-Hockey und Arsch-Hirn-Krabben. Danach ging es zum Abendgebet und anschließend ins Bett. Nachdem die Gruppenführer am Samstagmorgen die Kinder aus den Betten geholt hatten, wurde zunächst Frühsport betrieben und dann ging es zum Frühstück. Davor hatten wir im Heiligtum noch unser Morgengebet.
---
<p>Das erste Gemeinschaftswochenende der SMJ&nbsp;Fulda 2011 begann am Freitag den 18. Februar mit dem Abendessen. Nach anschließenden Kennenlernspielen wurde noch bis in den späten Abend gespielt. Die Spiele, die den meisten gefielen, waren Schrubber-Hockey und Arsch-Hirn-Krabben. Danach ging es zum Abendgebet und anschließend ins Bett.</p>
<p>Nachdem die Gruppenführer am Samstagmorgen die Kinder aus den Betten geholt hatten, wurde zunächst Frühsport betrieben und dann ging es zum Frühstück. Davor hatten wir im Heiligtum noch unser Morgengebet.</p>
<p>Eine kleine Pause später begannen wir mit unserem Samstagsprogramm. Zuerst erzählten die Gruppenführer den Kinder was zum Thema Apostolat und wir führten danach eine Gruppenstunde durch, bei denen die Kinder ein kleines Theaterstück vortragen sollten.</p>
<figure data-href="https://www.flickr.com/photos/45962678@N06/5462847816">
  <img src="https://farm6.staticflickr.com/5299/5462847816_762cd0b023_c.jpg" class="flickr-img flickr-img--medium" alt="" />
  <figcaption> (<time datetime="2011-02-18 22:28:49">2011</time>)</figcaption>
</figure>
<p>Nach dem Mittagessen spielten wir Saujagd: bei dem Würfel-Geländespiel war die Aufgabe&nbsp;&nbsp; Kärtchen zu suchen und pro gewürfelten Feld allgemeine Fragen zu beantworten oder kleine Aufgaben zu erledigen. Während des Spiels gab es natürlich auch eine Kaffepause mit Kakao und Kuchen. Nach dieser Stärkung ging es mit der sehr beliebten Saujagd weiter. Das Ergebnis war enorm knapp. Im Grunde hatte jede Gruppe gewonnen, da es nur ein bis zwei Felder Unterschied waren.</p>
<figure data-href="https://www.flickr.com/photos/45962678@N06/5462427523">
  <img src="https://farm6.staticflickr.com/5058/5462427523_82f2bfb2ae_c.jpg" class="flickr-img flickr-img--medium" alt="" />
  <figcaption> (<time datetime="2011-02-19 23:26:35">2011</time>)</figcaption>
</figure>
<p>Nach einer Pause feierten wir dann die Heilige Messe, die von von Kaplan Thomas Renze gehalten wurde. Dieser Gottesdienst war interessanter als normalerweise, da sich während der Predigt eine Art Dialog zwischen Kindern und Kaplan Renze ereignete. Nach diesem Gottesdienst gab es das sehr ersehnte Abendessen mit leckerer Pizza. Nach dem Abendessen hieß es für die Gruppenführer, sich für die nachfolgende Aktion vorzubereiten. Geplant war ein Casino-Abend mit den Spielen Blackjack, Poker und Roulette (natürlich mit Spielgeld). Die Kinder erziehlten ca. 40.000 &euro; Gewinn und hatten dabei jede Menge Spaß. Danach fand das Abendgebet statt und die Jungs gingen ins Bett.</p>
<p>Am nächsten Morgen hörten wir nach dem Frühstück etwas über Kampfgeist und Mut.Durch einige Zitate und kleine Filme wurde uns das Thema von den Gruppenleitern näher gebracht.</p>
<p>Am späteren Vormittag räumten wir alle unsere Zimmer auf und machten noch eine Abschlussrunde. Nach dem schmackhaftem Mittagessen wurden die Jungs von ihren Eltern abgeholt und sind nach Hause gefahren.</p>
<p>Wieder einmal ging ein sehr schönes und spaßiges Wochenende zu Ende!</p>
<figure class="flickr-photoset full-width" data-href="https://www.flickr.com/photos/45962678@N06/sets/72157625971326971">
  <a href="https://www.flickr.com/photos/45962678@N06/5462801320/in/album-72157625971326971"><img src="https://farm6.staticflickr.com/5060/5462801320_5e8c3903ed_q.jpg" alt="" data-src-large="https://farm6.staticflickr.com/5060/5462801320_5e8c3903ed_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5462204885/in/album-72157625971326971"><img src="https://farm6.staticflickr.com/5255/5462204885_9b4dfbd0e5_q.jpg" alt="" data-src-large="https://farm6.staticflickr.com/5255/5462204885_9b4dfbd0e5_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5462810114/in/album-72157625971326971"><img src="https://farm6.staticflickr.com/5175/5462810114_8970c316e0_q.jpg" alt="" data-src-large="https://farm6.staticflickr.com/5175/5462810114_8970c316e0_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5462814270/in/album-72157625971326971"><img src="https://farm6.staticflickr.com/5092/5462814270_1224df2a2a_q.jpg" alt="" data-src-large="https://farm6.staticflickr.com/5092/5462814270_1224df2a2a_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5462819576/in/album-72157625971326971"><img src="https://farm6.staticflickr.com/5174/5462819576_3af83bf558_q.jpg" alt="" data-src-large="https://farm6.staticflickr.com/5174/5462819576_3af83bf558_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5462823650/in/album-72157625971326971"><img src="https://farm6.staticflickr.com/5099/5462823650_a20872ded4_q.jpg" alt="" data-src-large="https://farm6.staticflickr.com/5099/5462823650_a20872ded4_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5462227963/in/album-72157625971326971"><img src="https://farm6.staticflickr.com/5259/5462227963_ecd1007da7_q.jpg" alt="" data-src-large="https://farm6.staticflickr.com/5259/5462227963_ecd1007da7_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5462833818/in/album-72157625971326971"><img src="https://farm6.staticflickr.com/5051/5462833818_673327b102_q.jpg" alt="" data-src-large="https://farm6.staticflickr.com/5051/5462833818_673327b102_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5462237043/in/album-72157625971326971"><img src="https://farm6.staticflickr.com/5217/5462237043_85a3ec670f_q.jpg" alt="" data-src-large="https://farm6.staticflickr.com/5217/5462237043_85a3ec670f_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5462842714/in/album-72157625971326971"><img src="https://farm6.staticflickr.com/5051/5462842714_0f6146bca3_q.jpg" alt="" data-src-large="https://farm6.staticflickr.com/5051/5462842714_0f6146bca3_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5462847816/in/album-72157625971326971"><img src="https://farm6.staticflickr.com/5299/5462847816_762cd0b023_q.jpg" alt="" data-src-large="https://farm6.staticflickr.com/5299/5462847816_762cd0b023_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5462251123/in/album-72157625971326971"><img src="https://farm6.staticflickr.com/5099/5462251123_587a83f003_q.jpg" alt="" data-src-large="https://farm6.staticflickr.com/5099/5462251123_587a83f003_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5462856806/in/album-72157625971326971"><img src="https://farm6.staticflickr.com/5220/5462856806_cefc1812ea_q.jpg" alt="" data-src-large="https://farm6.staticflickr.com/5220/5462856806_cefc1812ea_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5462861332/in/album-72157625971326971"><img src="https://farm6.staticflickr.com/5215/5462861332_d01d979c16_q.jpg" alt="" data-src-large="https://farm6.staticflickr.com/5215/5462861332_d01d979c16_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/sets/72157625971326971" class="flickr-link" title="Bildergalerie Gemeinschaftswochende Februar 2011 auf Flickr">Bildergalerie</a>
</figure>
