---
categories:
- Sonstiges
- smj
- jako
- '2007'
migrated:
  node_id: 46
  migrated_at: 2017-02-20 22:25
  alias: artikel/46-wahlparty-jako-auftakt
  user_id: 1
title: Wahlparty Jako-Auftakt
created_at: 2008-02-24 13:21
excerpt: |-
  Zeit: 13.30 Uhr
  Ort: Eichenzell (Bei Dückers vor der Tür)
  Personen: Alexander Dücker , Sebastian Hartmann Kalos beladen und auf geht’s. Die Reise am zweiten Tag des Fests der Liebe führt zunächst nach Lütter. Stefan wartet schon gespannt mit 3 Fässern feinsten Klosterbiers auf uns. Nachdem das Auto von Alex tiefergelegt wurde, wir uns mit Anzügen hineingezwängt hatten, konnte es endlich losgehen. Halt! Da fehlt noch wer! Genau! Johannes Müller aus dem Königreich Flieden. 4. Mann im Auto und somit wirklich das Ultimo an Zuladung. In Flieden trafen wir dann auch auf Tobi, Klaus und Ulli, die im zweiten Auto fuhren. Lautstark und unter dem Genuss von isotonischen Erfrischungsgetränken begannen wir die große Reise.
  Ziel: Schönstatt (Jako)
---
<h3 class="subtitle">2. Weihnachtsfeiertag 2007</h3>
<p class="pre">Zeit: 13.30 Uhr
Ort: Eichenzell (Bei Dückers vor der Tür)
Personen: Alexander Dücker , Sebastian Hartmann</p>
<p>
Kalos beladen und auf geht’s. Die Reise am zweiten Tag des Fests der Liebe führt zunächst nach Lütter. Stefan wartet schon gespannt mit 3 Fässern feinsten Klosterbiers auf uns. Nachdem das Auto von Alex tiefergelegt wurde, wir uns mit Anzügen hineingezwängt hatten, konnte es endlich losgehen. Halt! Da fehlt noch wer! Genau! Johannes Müller aus dem Königreich Flieden. 4. Mann im Auto und somit wirklich das Ultimo an Zuladung. In Flieden trafen wir dann auch auf Tobi, Klaus und Ulli, die im zweiten Auto fuhren. Lautstark und unter dem Genuss von isotonischen Erfrischungsgetränken begannen wir die große Reise.
Ziel: Schönstatt (Jako)</p>
<!--break-->
<p>Auf der Autobahn war niemand vor der SMJ Fulda sicher. Wir waren die Kings of the road. Naja,…also zumind. an den LKWs sind wir vorbeigedüst. Nicht nur unter Zeitdruck standen die Teilnehmer dabei. So wurde nur eine einzige Zwangspause gemacht. Erleichtert ging es weiter. Im Jugendzentrum angekommen, wurden zunächst die Zimmer bezogen und alle anderen herzlich begrüßt. Die Regio Mitte war für den ersten Abend (Wahlparty) zuständig. In einem kurzen briefing wurden die letzten Absprachen getroffen und die Vertreter der drei großen Parteien bestimmt. Nebenan wurde die Zapfanlage aufgebaut und in der Küche waren erlesene Pizzabrötchenbäcker aus Fulda am Werk. Man erkennt sofort, dass die SMJ-Fulda in ihren Spezialbereichen (Essen + Trinken) tätig war.</p>
<p>Um 19 Uhr fiel der Startschuss. Frank Blumers – der Moderator des Wahlabends- begrüßte die Gäste (ca. 60 Mann aus Nah und Fern [nur nicht aus Speyer, denn die SMJ aus Speyer könnte wegen schlechter Witterung nicht kommen]). Zunächst stellten die Vertreter der Parteien ihre Programme vor. Die Bindestrich-Partei-Deutschlands erklärte lautstark: „Wir-brauchen-Bindestriche,-die-verbinden! Weg mit den durchgezogenen Linien auf deutschen Straßen – da gehören Striche hin!“. Johannes Müller von der Partei der GROßBUCHSTABEN erklärte: „WENN WIR NICHT GROß SCHREIBEN, KANN AUCH NICHTS GROßES WACHSEN!“. Und zuletzt die ®-Partei, welche sich besonders für die rechtliche Sicherung der SMJ® Slogans einsetzte. Die ersten Hochrechnungen wurden gespannt erwartet. Dabei unterhielt man sich und genoss das gute Bier vom Kreuzberg und die leckeren Pizzabrötchen. Dann endlich das Endergebnis stand fest: Die Bindestrich-Partei-Deustchlands konnte die meisten Stimmen auf sich vereinen. Gefeiert wurde dies mit Sekt. Außenreporter Tobias Büdel befragt noch ein paar Experten und schließlich fand eine LIVE!!!!!!!!-Schaltung nach Rom statt. So übermittelte Benedikt XVI seine besten Glückwünsche zu der neuen Jahresparole der SMJ-Deutschland. Nach diesen bewegenden und spannenden Ereignissen setzte man sich noch ein wenig zusammen und plauderte oder sang lautstark. Hierbei glänzte neben der SMJ-Fulda, welche natürlich ungeschlagen bleibt, auch die SMJ aus Bayern. Jedoch sind bei den Süddeutschen noch ein paar Übungsstunden fällig ;-) . Um 0 Uhr wurde die Fulda-Hymne (Bonifatiuslied) gesungen und der Schlachtruf ordnungsgemäß durchgeführt.</p>
<p>Am nächsten Tag hieß es nach dem Frühstück „Sachen-Packen!“. Nach dem Mittagessen erfolgte die Verabschiedung und lediglich Klaus und Tobi blieben dort, um die Stellung zu halten. Die Heimfahrt gestaltete sich etwas länger als erwartet und eine große Enttäuschung erlebten die fünf Heimreisenden. Ein geplanter Zwischenstopp bei den LUDOLFs entpuppte sich als Reinfall, da die Ludolfs tatsächlich im falschen Dernbach im Westerwald wohnen. Aber die Stimmung wurde mit einem Besuch bei Burgerking wieder gehoben. In Fulda freuten sich noch ein paar Kinder über die „drei-Burgerking-Kronen-Könige“ und wir verabschiedeten uns voneinander.</p>
<p>Das Ergebnis der Aktion war: 3 leere Fässer Klosterbier, volle Mägen durch Pizzabrötchen, keine Stimmen mehr, eine Burgerkingkrone für jeden, Lust auf mehr gemeinsame Aktionen und ein einmaliges Gemeinschaftsgefühl.</p>
<p>Sie SMJ-Fulda hofft, dass wir euch einen netten Wahlabend beschert haben und wünschen der gesamten SMJ in Deutschland ein gutes Jahr 2008! </p>
