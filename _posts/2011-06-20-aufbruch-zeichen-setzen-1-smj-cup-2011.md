---
tags: [smjcup, smjdeutschland]
migrated:
  node_id: 868
  migrated_at: 2017-02-20 22:26
  alias: ''
  user_id: 25
author: Thomas Limbach
title: Aufbruch-Zeichen setzen! 1. SMJ-Cup 2011
created_at: 2011-06-20 14:15
excerpt: 'Schönstatt - Am Wochenende vom 17. – 19. Juni 2011 waren 65 Teilnehmer des 1. Fußball-Cups der Schönstatt-Mannesjugend begeistert, dem runden Leder hinter her jagen zu können. Den Auftakt am Freitagabend machten die verschiedenen Teams mit Spielabsprachen, Trainingsläufen und –spielen, als auch letzten taktischen Absprachen. Aussagen von Bastian Schweinsteiger: „Ich glaube einfach, das Gott mit im Spiel meines Lebens ist. Karriere machen ist zwar schön, aber kein Ziel. Dein bestes Ziel sollte es sein, deine Fähigkeiten genau zu kennen, sie so einzusetzen, dass es stets der Mannschaft dient“ oder auch die von Lukas Podolski: „Gott glaubt stets an dich, also verliere du auch nie den Glauben an dich“, beendeten den Tag mit dem Abendsegen.<br>'
---
<p>Schönstatt - Am Wochenende vom 17. &ndash; 19. Juni 2011 waren 65 Teilnehmer des 1. Fußball-Cups der Schönstatt-Mannesjugend begeistert, dem runden Leder hinter her jagen zu können. Den Auftakt am Freitagabend machten die verschiedenen Teams mit Spielabsprachen, Trainingsläufen und &ndash;spielen, als auch letzten taktischen Absprachen. Aussagen von Bastian Schweinsteiger: &bdquo;Ich glaube einfach, das Gott mit im Spiel meines Lebens ist. Karriere machen ist zwar schön, aber kein Ziel. Dein bestes Ziel sollte es sein, deine Fähigkeiten genau zu kennen, sie so einzusetzen, dass es stets der Mannschaft dient&ldquo; oder auch die von Lukas Podolski: &bdquo;Gott glaubt stets an dich, also verliere du auch nie den Glauben an dich&ldquo;, beendeten den Tag mit dem Abendsegen.<br />
	<br />
	Einen wahren Marathon-Turniertag erlebten sowohl Spieler und Zuschauer auf dem Sportplatz des SV Niederwerth am Samstag. Gut gestärkt mit Gottes Segen und einem Sportlerfrühstück&nbsp; fanden von 9.45 bis 16.45 Uhr packende und spannende Mannschaftsspiele statt. Um 17.00 Uhr liefen das Schiedsrichtergespann Thomas Limbach, Milan Drehmann und Rainer M. Gotter mit den beiden Teams der SMJ Oberland und der SMJ Trier/Paderborn als Spielgemeinschaft zum Anpfiff um Platz 3 und 4 auf.&nbsp; Im Finale verwies das Team der SMJ Fulda die SMJ Köln mit einem klaren 3:0 in die Schranken. Die anschließende Siegesfeier mit Siegerehrung aller Spieler und Teams brachte das Jugendzentrum noch mal so richtig in Feierlaune und beendete den Turniertag mit viel Spaß, Freude und supertollen Fangesängen.<br />
	<br />
	Rektor Egon Zillekens, Diözesanpriester und fußballbegeisterter Fan von Schalke 04, gab der &bdquo;Frohen Botschaft&ldquo; am Sonntag im Gottesdienst ein ganz neues Gesicht. Dass Fußball mehr ist als ein Sport, nämlich eine Weltanschauung ließ alle Ohren spitzen und als sie gemeinsam feststellten, dass Pater Josef Kentenich, vom alten Haus aus, seinen Jungen beim Fußball spielen, direkt neben dem Ur-Heiligtum, zugeschaut hat.&nbsp; Da war das Blitzen in den vielen Augen so gut zu erkennen, dass es bestimmt Freude gemacht hätte, gleich mal den Ball hervor zu holen und los zu legen.&nbsp; <br />
	<br />
	Zu guter Letzt sagte ein Spieler: &bdquo;Es&nbsp; ist einfach etwas ganz Besonderes, zum ersten Mal dabei gewesen zu sein. Beim Cup 2012 sind wir auf jeden Fall wieder mit am Start.&ldquo;<br />
	<br />
	Wir greifen als Schiedsrichterteam gerne diese Aussage auf und laden zum 2. Fußball-Cup der Schönstatt-Mannesjugend Deutschland vom 22.-24. Juni 2012 ins Jugendzentrum Marienberg ein.<br />
	<br />
	SMJ&hellip; immer am Ball!<br />
	Thomas Limbach &amp; Milan Drehmann &amp; Rainer M. Gotter</p>
