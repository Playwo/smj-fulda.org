---
tags: [smjfulda, kreise]
migrated:
  node_id: 978
  migrated_at: 2017-02-20 22:28
  alias: artikel/978-kreistagung-schönstatt
  user_id: 0
tagline: Vom 21. bis 23. März hatten sich die Kreise zur Tagung in Schönstatt getroffen.
title: Kreistagung in Schönstatt
created_at: 2014-03-28 18:57
excerpt: Am vergangenen Wochenende fand das Treffen der Kreise der SMJ-Fulda nicht wie üblich im Schönstattzentrum in Dietershausen, sondern an unserem Gründungsort in Schönstatt statt. Beginn war am Freitagabend mit dem Abendessen im Jugendzentrum auf dem Marienberg. Gemeinsam schauten wir uns anschließend den Film „Der Club der toten Dichter“ an. Dieser machte uns deutlich, wie wichtig es ist, seine Freiheiten wahrzunehmen und zu nutzen, aber auch welche Gefahren sich dahinter verbergen können. Am Samstagvormittag besuchten wir den Schönstattpater Wolfgang Götz, der ein ganz besonderes Leben führt.
---
<div align="JUSTIFY" style="margin-bottom: 0.35cm"><font size="3">Am vergangenen Wochenende fand das Treffen der Kreise der SMJ-Fulda nicht wie üblich im Schönstattzentrum in Dietershausen, sondern an unserem Gründungsort in Schönstatt statt. Beginn war am Freitagabend mit dem Abendessen im Jugendzentrum auf dem Marienberg. Gemeinsam schauten wir uns anschließend den Film &bdquo;Der Club der toten Dichter&ldquo; an. Dieser machte uns deutlich, wie wichtig es ist, seine Freiheiten wahrzunehmen und zu nutzen, aber auch welche Gefahren sich dahinter verbergen können.</font></div>
<div align="JUSTIFY" style="margin-bottom: 0.35cm"><font size="3">Am Samstagvormittag besuchten wir den Schönstattpater Wolfgang Götz, der ein ganz besonderes Leben führt. Er ist Pater, aber er lebt nicht mit seiner Gemeinschaft in einem der vielen Häuser Schönstatts, sondern im Wald in einer Hütte als Einsiedler. Pater Wolfgang berichtete uns, wie er seinen Alltag gestaltet. Darüber hinaus war es natürlich sehr interessant zu erfahren, was ihn dazu bewogen hat, ein Einsiedlerleben zu führen. An seine kleine Hütte schließt sich eine Kapelle an. Am Nachmittag ging es in den einzelnen Kreisen weiter. Die Jungs von Matthias und Ullrich fuhren nach Ehrenbreitstein, um eine Burg zu besichtigen. Kreis Tobias besuchte das Pater Josef Kentenich Haus und Kreis Alexander besuchte das Haus auf Berg Schönstatt, in dem Pater Kentenich drei Jahre seines Lebens verbracht hat. Eine über 90 Jahre alte Schwester erzählte viel Interessantes über den Gründer Schönstatts und sein Leben. Dieser Besuch war eines der Highlights des Wochenendes, gerade im Hinblick auf das 100-jährige Jubiläum der Schönstattbewegung in diesem Jahr.</font></div>
<div align="JUSTIFY" style="margin-bottom: 0.35cm"><font size="3">Den Abend verbrachten alle Kreise gemeinsam mit Gesellschaftsspielen. Für Spaß und Spannung war also gesorgt. Der Ein oder Andere geriet dabei ganz schön in Rage. Am Sonntagmorgen feierten alle zusammen den Gottesdienst mit Pater Stefan Strecker bereits um 7:30 im Hausheiligtum des Jugendzentrums. Danach hatten die Kreise noch etwas Zeit für sich. Am frühen Nachmittag ging es dann wieder nach Hause. Fazit: Es war ein rundum gelungenes Wochenende und in diesem Jahr sicherlich nicht der letzte Besuch in Schönstatt!</font></div>
<div style="margin-bottom: 0.35cm"><br />
	&nbsp;</div>
