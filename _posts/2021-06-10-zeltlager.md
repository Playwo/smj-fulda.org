---
title: "Zeltlager"
tagline: Ferienprogramm der SMJ-Fulda 2021
author: Christian Schopp
tags: [zeltlager]
image:
  cover: https://live.staticflickr.com/65535/48759165132_c2598c8653_k_d.jpg
  align: top
---

*Es ist dunkle Nacht als Percy über das weiche Moos läuft. Mit jedem Schritt hört er einen dumpfen Schlag wie eine Trommel. Oder kommen die Trommelschläge doch von vorne? Was wenn er sein Ziel gleich aus dem Wald auftaucht?*

*Fast schon kann Percy spüren, wie sich der Wald vor ihm lichtet. Ein paar Strahlen dringen durch das dichte Geäst. Auch der Boden verändert sich. Percy kann jetzt kleine Äste und Steinchen spüren, die sich in seine blanke Fußsohle zu bohren versuchen.*

*Alles verändert sich hier. Auch der feuchte, fast schon modrige Geruch ist jetzt weniger stechend. Eine leichte Brise weht über Percys Gesicht und kitzelt ihm unvermittelt in der Nase. In der Ferne sind Stimmen zu hören. Männer, die etwas Unverständliches schreien. Doch wo ist Percy hier nur gelandet…?*

Wenn du wissen willst, wie es weitergeht, dann gibt es nur eine Möglichkeit.

Dieses Jahr kannst du beim Zeltlager der SMJ allerhand dazulernen. Die griechische Mythologie mit all ihren Wundergeschichten, Göttern und Helden wartet darauf, von dir entdeckt zu werden. Also warte nicht länger: Melde dich an!

Auch dieses Jahr wird es coronabedingt ein abgespecktes Programm für unser Zeltlager geben. Soweit es uns möglich ist, wollen wir aber nicht auf den Spaß und die wertvollen Erlebnisse verzichten, die wir in so vielen Jahren schon erleben durften.

Vom 11. bis zum 13. August werden wir den Anfang für unsere diesjährige Freizeit in Dietershausen setzen. Danach gibt es nochmal vom 16. bis zum 20. August fünf Tage voller Aktion, Spiel und Spannung. Als Schönstätter ist es uns wichtig, christliche Werte zu vermitteln und die Entwicklung junger Menschen zu starken Charakteren zu fördern. Wir sind eine katholische Jugendbewegung, freuen uns aber auch über Teilnehmer aus anderen Konfessionen.

Eingeladen sind alle Jungs zwischen 9 und 14 Jahren. Hier nochmal die wichtigsten Daten:

<ul>
  <li><strong>Was?</strong> Zeltlager der Schönstatt Mannesjugend Fulda</li>
  <li><strong>Wann?</strong> 11.&ndash;13. (1. Woche) und 16.&ndash;20. August (2. Woche). Beide Wochen können unabhängig voneinander besucht werden.</li>
  <li><strong>Wo?</strong> Schönstattzentrum in 36093 Dietershausen</li>
  <li><strong>Wer?</strong> Jungs zwischen 9 und 14 Jahren</li>
  <li><strong>Kosten?</strong> 1. Woche 50€ (für Geschwister jeweils 40€) 2. Woche 90€ (für Geschwister jeweils 75€)</li>
  <li><strong>Kontakt?</strong> Lagerleiter Christian Schopp (<a href="mailto:christian.schopp@smj-fulda.org">christian.schopp@smj-fulda.org</a>)</li>
</ul>

Nach aktuellen Coronaregeln können wir beide Wochen als feste Blöcke inklusive Übernachtung anbieten.

Bitte beachten Sie unser <a href="https://files.smj-fulda.org/zeltlager/hygienekonzept-SMJ-Fulda-2021.pdf">Hygienekonzept</a>.

Für Fragen steht Lagerleiter Christian Schopp ([christian.schopp@smj-fulda.org](mailto:christian.schopp@smj-fulda.org)) gerne zur Verfügung.

<a href="/zeltlager" class="btn">Mehr zum Zeltlager</a>

<figure class="full-width">
    <a href="https://files.smj-fulda.org/zeltlager/zeltlager-flyer-2021.pdf" title="Zeltlagerflyer 2021 (PDF)">
        <img src="{{ site.baseurl }}/aktionen/zeltlager/2021/thumb.jpg" />
    </a>
    <figcaption>
    <a href="https://files.smj-fulda.org/zeltlager/zeltlager-flyer-2021.pdf" title="Zeltlagerflyer 2021" class="btn">Zeltlager-Einladung 2021 (PDF)</a>
    </figcaption>
</figure>
