---
categories:
- Zeltlager
migrated:
  node_id: 832
  migrated_at: 2017-02-21 14:40
  alias: artikel/832-starker-wind-–-wir-setzen-segel
  user_id: 1
tagline: Seefahrer-Zeltlager 2010 in Hausen
image:
  cover: https://farm5.staticflickr.com/4139/4901144764_8f4dab2d5f_b.jpg
title: Starker Wind – Wir setzen Segel!
created_at: 2010-08-15 16:30
excerpt: "<strong>In den Sommerferien 2010 hieß es vom 3. bis 14. August: <em>„Schiff ahoi!“</em> Auf dem Zeltplatz <em>Am Reith</em> in Hausen/Rhön waren knapp 30 Mann im Zeltlager der Schönstatt-Mannesjugend (SMJ) im Bistum Fulda unter dem Lagermotto <em>Starker Wind – Wir setzen Segel!</em> in See gestochen.</strong> In den zwölf Tagen auf See gab es neben vielen Spielen und spannenden Abenteuern auch inhaltliche Programmpunkte: In Referaten und Gruppenstunden wurden den Matrosen christliche Glaubensinhalte auf Basis der Schönstatt-Pädagogik vermittelt."
---
<p><strong>In den Sommerferien 2010 hieß es vom 3. bis 14. August: <em>&bdquo;Schiff ahoi!&ldquo;</em> Auf dem Zeltplatz <em>Am Reith</em> in Hausen/Rhön waren knapp 30 Mann im Zeltlager der Schönstatt-Mannesjugend (SMJ) im Bistum Fulda unter dem Lagermotto <em>Starker Wind &ndash; Wir setzen Segel!</em> in See gestochen.</strong></p>
<div class="section">
	<figure data-href="https://www.flickr.com/photos/45962678@N06/4901144764">
  <img src="https://farm5.staticflickr.com/4139/4901144764_8f4dab2d5f_c.jpg" class="flickr-img flickr-img--medium" alt="Gruppenbild am Oberdeck" />
  <figcaption>Gruppenbild am Oberdeck (<time datetime="2010-08-13 12:46:09">2010</time>)</figcaption>
</figure>
	<p>In den zwölf Tagen auf See gab es neben vielen Spielen und spannenden Abenteuern auch inhaltliche Programmpunkte: In Referaten und Gruppenstunden wurden den Matrosen christliche Glaubensinhalte auf Basis der Schönstatt-Pädagogik vermittelt.</p>
	<p>Geleitet wurde das Lager von Kapitän Tobias Büdel mit seinem Ersten Offizier Klaus Schmitt und dem Schiffspriester Pfarrer Ullrich Schäfer, sowie zehn weiteren Offizieren und Kombüsenpersonal. Die 9- bis 13-Jährigen Jungen waren in vier Kajüten unter den Kommandos von Thomas Limbach, Philipp Müller und Christian und Christoph Schopp eingeteilt.</p>
</div>
<div class="section"><figure data-href="https://www.flickr.com/photos/45962678@N06/4894439916">
  <img src="https://farm5.staticflickr.com/4077/4894439916_2e40c7fa0c_c.jpg" class="flickr-img flickr-img--medium" alt="Referat Niklas" />
  <figcaption>Referat Niklas (<time datetime="2010-08-05 10:24:17">2010</time>)</figcaption>
</figure>
	<p>Ein Highlight war sicher die Zwei-Tagestour, bei der die vier Gruppen einzeln zur Erkundung von jeweils einer von vier gesichteten Inseln ausgesandt wurden und einen ganzen Tagesmarsch bis dorthin benötigten. Dort angekommen hieß es, sich eine Unterkunft für die Nacht zu organisieren, bevor es am nächsten Tag wieder zurück an Bord ging.</p>
	<p>Auch das Tagesspiel fand viel Begeisterung, wo die Jungs vom Kapitän in einzelnen Gruppen losgeschickt wurden, um möglichst viele Abzeichen zum Beweis ihrer Seetauglichkeit zu sammeln. Dabei konnten bei den als verschiedene Seeleute verkleideten Betreuern Gegenstände gehandelt und Aufgaben gelöst werden. Auch zwei Piraten trieben ihr Unheil und so konnte ein gerade erfolgreich ergattertes Abzeichen abhandenkommen oder teuer verdientes Gold ging beim Glückspiel verloren.</p>
	<p>Daneben standen auch Stationslauf, Nachtwanderung, Schiffsolympiade, Schwimmbad und natürlich die abendliche Lagerfeuerrunde auf dem Programm. Am Sonntag hatte die gesamte Mannschaft Landgang und fuhr mit einer historischen Dampfeisenbahn nach Ostheim zum Minigolf-Platz.</p>
</div>
<div class="section">
	<p>Gleich in der zweiten Nacht traf die Mannschaft auf ein Piratenschiff unter dem Kommando von Kapitän Lumumba und musste durch mehrere kleinere Aufgaben die Freundin unseres Kapitäns aus der Gefangenschaft der Piraten befreien. Im weiteren Verlauf der Nacht wurde unser Schiff von weiteren Piraten aus Lumumbas Bande überfallen und erlitt durch den Verlust zweier Fahnen und der Marienikone eine heftige Niederlage.</p>
	<p>Durch den aufmerksamen Einsatz der Nachtwache konnten die folgenden sechs Piraten-Überfälle im Lauf unserer Reise erfolgreich abgewehrt werden und andere Piratenbanden konnte nichts Nennenswertes mehr erbeuten.</p>
</div>
<figure class="flickr-photoset full-width" data-href="https://www.flickr.com/photos/45962678@N06/sets/72157624605320731">
  <a href="https://www.flickr.com/photos/45962678@N06/4893763407/in/album-72157624605320731"><img src="https://farm5.staticflickr.com/4078/4893763407_06a57df633_q.jpg" alt="Zeltaufbau" data-src-large="https://farm5.staticflickr.com/4078/4893763407_06a57df633_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4894359008/in/album-72157624605320731"><img src="https://farm5.staticflickr.com/4141/4894359008_70afeb76ff_q.jpg" alt="Vortrupp" data-src-large="https://farm5.staticflickr.com/4141/4894359008_70afeb76ff_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4893766573/in/album-72157624605320731"><img src="https://farm5.staticflickr.com/4136/4893766573_2ef291bb4b_q.jpg" alt="Dennis übt Gitarre" data-src-large="https://farm5.staticflickr.com/4136/4893766573_2ef291bb4b_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4894361940/in/album-72157624605320731"><img src="https://farm5.staticflickr.com/4121/4894361940_c30b6f65c9_q.jpg" alt="Präzision im Detail" data-src-large="https://farm5.staticflickr.com/4121/4894361940_c30b6f65c9_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4893770027/in/album-72157624605320731"><img src="https://farm5.staticflickr.com/4114/4893770027_be635866a5_q.jpg" alt="Fußball" data-src-large="https://farm5.staticflickr.com/4114/4893770027_be635866a5_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4894365436/in/album-72157624605320731"><img src="https://farm5.staticflickr.com/4079/4894365436_41f1cfb988_q.jpg" alt="Fußball" data-src-large="https://farm5.staticflickr.com/4079/4894365436_41f1cfb988_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4894366858/in/album-72157624605320731"><img src="https://farm5.staticflickr.com/4074/4894366858_b2fc6b8006_q.jpg" alt="Unterdeck" data-src-large="https://farm5.staticflickr.com/4074/4894366858_b2fc6b8006_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4894368378/in/album-72157624605320731"><img src="https://farm5.staticflickr.com/4118/4894368378_9948e96608_q.jpg" alt="Glockenständer" data-src-large="https://farm5.staticflickr.com/4118/4894368378_9948e96608_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4893777853/in/album-72157624605320731"><img src="https://farm5.staticflickr.com/4142/4893777853_7e27b9d1e0_q.jpg" alt="Im Wald" data-src-large="https://farm5.staticflickr.com/4142/4893777853_7e27b9d1e0_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4894373742/in/album-72157624605320731"><img src="https://farm5.staticflickr.com/4078/4894373742_aef7436da8_q.jpg" alt="Felix sägt" data-src-large="https://farm5.staticflickr.com/4078/4894373742_aef7436da8_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4894377976/in/album-72157624605320731"><img src="https://farm5.staticflickr.com/4117/4894377976_78b54d271e_q.jpg" alt="Glockenständer" data-src-large="https://farm5.staticflickr.com/4117/4894377976_78b54d271e_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4893785447/in/album-72157624605320731"><img src="https://farm5.staticflickr.com/4078/4893785447_3b6065e899_q.jpg" alt="Smutje Ulli" data-src-large="https://farm5.staticflickr.com/4078/4893785447_3b6065e899_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4893787891/in/album-72157624605320731"><img src="https://farm5.staticflickr.com/4075/4893787891_be33eaeaaf_q.jpg" alt="Loch für Schiffskreuz" data-src-large="https://farm5.staticflickr.com/4075/4893787891_be33eaeaaf_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4893790741/in/album-72157624605320731"><img src="https://farm5.staticflickr.com/4117/4893790741_9dac344730_q.jpg" alt="Loch für Fahnenmast" data-src-large="https://farm5.staticflickr.com/4117/4893790741_9dac344730_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/sets/72157624605320731" class="flickr-link" title="Bildergalerie Zeltlager 2010 auf Flickr">Bildergalerie</a>
</figure>
<div class="section">
	<figure data-href="https://www.flickr.com/photos/45962678@N06/4901083202">
  <img src="https://farm5.staticflickr.com/4078/4901083202_c011fbbba6_c.jpg" class="flickr-img flickr-img--medium" alt="Fahne wird eingeholt" />
  <figcaption>Fahne wird eingeholt (<time datetime="2010-08-14 10:18:51">2010</time>)</figcaption>
</figure>
	<p>Am Ende der Reise lief das Schiff wieder in den Heimathafen ein und die Matrosen wurden von ihren Eltern abgeholt. Zusammen mit Eltern und Offizieren feierte man am Samstag ein großes Abschiedsfest mit Heiliger Messe, Mittagessen und Schiffszirkus. Hier fand auch die Ehrung von guten Leistungen in zahlreichen Wettbewerben statt, wobei den Sieg des herausragenden Gemeinschaftswettbewerbs die Kajüte von Philipp Müller für sich beanspruchen konnte.</p>
	<p>Nach zwölf Tagen war die gesamte Besatzung sichtlich erschöpft aber begeistert von einer schönen Zeit voller neuer Erfahrungen auf der abenteuerlichen Reise.</p>
</div>
