---
tags: [smjfulda, adventskalender, f1irstlive.de, presseschau]
migrated:
  node_id: 956
  migrated_at: 2017-02-20 22:28
  alias: artikel/956-f1rstlife.de:-der-etwas-andere-adventskalender
  user_id: 1
tagline: Das Jugendmagazin f1irstlive.de berichtet über unseren SMS-Adventskalender
title: 'f1rstlife.de: Der etwas andere Adventskalender'
created_at: 2013-11-26 23:18
---
<p>Das Jugendmagazin <a href="http://f1irstlive.de">f1irstlive.de</a> berichtet unter dem Titel <em>Der etwas andere Adventskalender</em> über unseren SMS-Adventskalender.</p>
<p>Hier gehts zum Artikel:<br />
	<a href="http://www.f1rstlife.de/news/details/artikel/der_etwas_andere_adventskalender/">http://www.f1rstlife.de/news/details/artikel/der_etwas_andere_adventskalender/</a> </p>
