---
title: Kreisttreffen mit Marienwallfahrt
tags: [smj-fulda, kreise]
author: Johannes Müller
image:
  cover: "https://farm5.staticflickr.com/4634/24616203277_7f22a247f6_k_d.jpg"
---

Der Kreis Alexander/Benjamin hat Anfang des Jahres als Kreistreffen eine Wallfahrt zum Gnadenbild der Muttergottes in Rückers unternommen. Von starkem Wind und Nieselregen ließen sich die Ritter Mariens nicht abschrecken. Pfarrer Thomas Maleja erläuterte kurz die Geschichte des Wallfahrtsortes, der vom Spätmittelalter bis ins 19. Jahrhundert größte Wallfahrtsstätte im Hochstift Fulda war.
Anschließend an die fromme Übung ging es zum Pizzaessen und gemütlichen Beisammensein nach Flieden ins Hause Müller.
