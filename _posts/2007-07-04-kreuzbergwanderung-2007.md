---
categories:
- Kreuzbergwanderung
migrated:
  node_id: 685
  migrated_at: 2017-02-20 22:25
  alias: artikel/685-kreuzbergwanderung-2007
  user_id: 1
title: Kreuzbergwanderung 2007
created_at: 2007-07-04 15:56
excerpt: Wir schreiben den 1. Juli 2007. Es ist Sonntag 9 Uhr. Ganz Deutschland schläft. Ganz Deutschland? NEIN! Eine kleine Gruppe unerschütterlicher SMJler und MäJulerinnen macht sich auf eine gefährliche und abenteuerliche Reise...
---
<p>Wir schreiben den 1. Juli 2007. Es ist Sonntag 9 Uhr. Ganz Deutschland schläft. Ganz Deutschland? NEIN! Eine kleine Gruppe unerschütterlicher SMJler und MäJulerinnen macht sich auf eine gefährliche und abenteuerliche Reise...</p>
<p>Der Beginn war eine kleine Station im Liebesheiligtum. Von dort aus startete der lange Marsch durch die Rhön. Waren uns auch die Knie weich, Kreuzberg wir verzagten nicht gleich. Lediglich der Salto mortale blieb aus. Nach ca. 7 Stunden Marsch durch unwegsames Gelände (wir haben nämlich auch abgekürzt!) konnten wir endlich das Bonifatiuslied bei der Kreuzigungsgruppe singen. Das Kreuzbergbier und kleine Schmankerl versüßten uns den Abschluss des Tages. Bis nächstes Jahr, wenn es wieder heißt: "Komm mit mein Schatz..."</p>
<p>Dass nächstes Jahr noch mehr SMJler und SMäJulerinnen mitkommen hofft<br>euer Bonkos</p>
