---
title: "Abschied vom Rhönstübchen"
tagline:
author: Johannes Müller
tags: [smj-fulda]
image:
  cover: https://live.staticflickr.com/65535/51058597501_3a0b5c80a6_k_d.jpg
---

Das Rhönstübchen war über Jahrzehnte Stammlokal und geliebtes Kleinod der SMJ-Fulda. Unzählige gesellige Abendrunden nahmen hier ihren Anfang und fulminanten Abschluss.
Jetzt müssen wir uns leider verabschieden. Der alte Teil vom Josef-Engling-Haus wird in wenigen Wochen abgetragen.

Dieses Video gibt noch mal ein paar schöne Einblicke in unsere SMJ-Heimastube. Zur Erinnerung.

<figure class="full-width">
  <iframe width="658" height="370" src="https://www.youtube-nocookie.com/embed/E9T7S_81ozA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

Und nun heißt es: Nehmt Abschied, Brüder, schließt den Kreis. Leb wohl, kleines Madel, leb wohl!

Einen würdigen Ersatz soll es natürlich auch geben, aber bis zur Bezugsreife dauert es noch etwas.
Alle wichtige Austattung ist eingelagert und wird mit umziehen.

<figure class="full-width">
  <img src="https://live.staticflickr.com/65535/51058685252_4109015364_c_d.jpg">
  <figcaption>Der obligatorische Betriebsurlaub wird nun für immer ein Ende haben.</figcaption>
</figure>

<figure class="full-width">
  <img src="https://live.staticflickr.com/65535/51058685347_e854f6ccfa_c_d.jpg">
  <figcaption>Hoffentlich können wir für eine hübsche Gestaltung wieder auf die MJF zählen.</figcaption>
</figure>

<figure class="full-width">
  <img src="https://live.staticflickr.com/65535/51058684802_cfdf3c0765_c_d.jpg">
</figure>

<figure class="full-width">
  <img src="https://live.staticflickr.com/65535/51058597891_97d7f11db0_c_d.jpg">
</figure>

<figure class="full-width">
  <img src="https://live.staticflickr.com/65535/51057876463_202f6b51b0_c_d.jpg">
</figure>
