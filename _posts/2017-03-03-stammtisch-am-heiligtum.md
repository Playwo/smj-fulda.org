---
title: Schönstatt-Stammtisch am Heiligtum
tags: [smjfulda, mjffulda, stammtisch, spieleabend]
author: Johannes Müller
image:
  cover: https://farm1.staticflickr.com/648/33291780525_44e82452dc_k_d.jpg
  align: top
---
Wir haben uns am 3. März gemeinsam mit der SchönstattMJF zum Stammtisch am Heiligtum in Dietershausen getroffen. Auf dem Programm standen Kelche- und Schlüssel-Sammeln mit Niederlagen-Proklamation, Pasta-Kochen, Geheimagenten-Enttarnen und fachmännisches Tee-Anrühren.
