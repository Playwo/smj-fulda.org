---
title: Einladung zum Gemeinschaftswochenende
tagline: 22. bis 24. Oktober 2021 in Dietershausen
author: Markus Breitenbach
tags: [gewo]
image:
  cover: https://live.staticflickr.com/65535/51433130233_f53be9b4f5_k_d.jpg
---

Es ist das größte Sportereignis der antiken Welt. Aus allen Ecken Griechenlands strömen Athleten nach Olympia, um an diesen riesigen Spielen zu Ehren der Götter teilzunehmen. Und auch du bist zu diesem Sportfest eingeladen. Wirst du dich gegen die Ringer aus Sparta, die Läufer aus Athen oder die Diskuswerfer aus Kreta behaupten können? Es gibt wohl nur eine Möglichkeit das herauszufinden.

An diesem Gemeinschaftswochenende der SMJ Fulda kannst du viel über die olympischen Werte lernen, aber auch Spiel und Spaß werden nicht zu kurz kommen. Bist du bereit für ein Wochenende voller Spannung und Freude mit vielen Gleichaltrigen? Dann melde dich an!

Die Schönstatt-Mannesjugend ist eine katholische Jugendorganisation und Teil der internationalen Schönstattbewegung. Als solche ist es uns wichtig, christliche Werte zu vermitteln und die Entwicklung junger Menschen zu starken Charakteren zu fördern.

Unser Gemeinschaftswochenende wird vom **22. bis 24. Oktober 2021** im **Josef-Engling-Haus in Dietershausen** stattfinden. Aufgrund der aktuellen Lage gelten die Hygienevorschriften des Josef-Engling-Hauses und der SMJ-Fulda. Des Weiteren gilt die **3G -Regel**. Wir bitten also bei der Anreise um die Vorlage eines aktuellen negativen Schnelltestzertifikats, eines Genesenenzertifikats oder eines Impfpasses.

Eingeladen sind alle Jungs zwischen 9 und 14 Jahren. Hier nochmal die wichtigsten Daten:

* **Was?** Gemeinschaftswochenende der Schönstatt-Mannesjugend Fulda
* **Wann?** 22.Oktober 18:00 Uhr bis 24. Oktober 12:30 Uhr
* **Wo?** Schönstattzentrum in 36093 Dietershausen
* **Wer?** Jungs zwischen 9 und 14 Jahren
* **Kosten?** 40€
* **Kontakt?** Tagungsleiter Markus Breitenbach (<a href="mailto:markus.breitenbach@smj-fulda.org">markus.breitenbach@smj-fulda.org</a>) und Elias Wolf (<a href="mailto:elias.wolf@smj-fulda.org">elias.wolf@smj-fulda.org</a>)

Für Fragen stehen Markus Breitenbach oder Elias Wolf gerne zur Verfügung. Anmeldungen werden von Markus Breitenbach entgegengenommen.
