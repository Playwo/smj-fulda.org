---
categories:
- Jahresparole
migrated:
  node_id: 841
  migrated_at: 2017-02-20 22:26
  alias: artikel/841-aufbruch-zeichen-setzen
  user_id: 5
tagline: Jahresparole 2011 der SMJ-Deutschland
author: Tobias Buedel
title: Aufbruch - Zeichen setzen!
created_at: 2011-01-01 19:41
excerpt: Letztes Jahr hatten wir mit unseren Säulen 10-jähriges Jubiläum. Wir machten uns bewusst, dass die Säulen die Grundlage unserer Arbeit und unserer Wertevermittlung sind. Deshalb besannen wir uns darauf und wollten sie stark nach außen präsentieren. "Denke kühn - zeige Stärke" entstand daraus.
---
<p>Letztes Jahr hatten wir mit unseren Säulen 10-jähriges Jubiläum. Wir  machten uns bewusst, dass die Säulen die Grundlage unserer Arbeit und  unserer Wertevermittlung sind. Deshalb besannen wir uns darauf und  wollten sie stark nach außen präsentieren. &quot;Denke kühn - zeige Stärke&quot;  entstand daraus.<br />
Nun, da in vielen Regionen Teilnehmerzahlen  zurückgehen und parallel dazu die aktuelle Shell-Studie sagt, dass immer  mehr junge Menschen sozial engagiert sind, aber nicht in der SMJ,  wollen wir uns weiterhin auf unsere Stärken, die Säulen, besinnen. Wir  als SMJ machen sehr gute Arbeit, können sie aber nicht sehr gut nach  außen &quot;verkaufen&quot;. Also müssen wir am Marketing arbeiten, schauen wie  man dieses gute Produkt an den Mann bringen kann. Und wir wissen alle  aus Erfahrung, dass das Produkt SMJ ein sehr gutes Produkt ist. Die  Werte, die wir vermitteln, sind eine gute Grundlage für ein ordentlich  geführtes, engagiertes Leben! Deshalb wollen wir nun aufbrechen, nach  vorne stürmen und Zeichen setzen! Wir wollen zeigen was wir draufhaben,  Potential ausschöpfen und Gas geben! Wir als SMJ haben Ziele! WJT Madrid  2011, Errichten eines Säulenplatzes am Taborheiligtum bis Oktober 2012,  Jubiläum 2014 und vieles mehr!! Dafür heißt es Einsatz zeigen, Power,  Energie, Mut, grenzenlos denken, Grenzen überwinden!!!<br />
Also brechen wir auf und setzen Zeichen!!!!</p>
