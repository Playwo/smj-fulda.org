---
title: Der Berg ruft auch Dich
tagline: Kreuzbergwanderung vom 23.12.2018
author: Christian Schopp
tags: [smjfulda, kreuzbergwanderung]
image:
  cover: https://farm5.staticflickr.com/4876/46487655681_d0fcfac5c1_o_d.jpg
  align: bottom
---

30 Kilometer, 12 junge Menschen, 5 Stunden Weg, 1 Mission.

Dieses Jahr starteten wir unsere traditionelle Weihnachts-Kreuzbergwanderung schon etwas früher als sonst.

Um 08:30 Uhr, etwa eine halbe Stunde vor der Traditionsstartzeit versammelten wir uns mit 12 jungen Leuten im Kapellchen.
Dort feierte Jugendpfarrer Alexander Best mit uns einen Gottesdienst bevor wir den gut 30 km langen Weg von Dietershausen zum heiligen Berg der Franken auf uns nahmen.

Bei leicht diesigem Wetter kämpften wir gegen die Widrigkeiten der Natur an (u. A. Regen, Matsch und Unlust)
und bezwangen gemeinsam Ebersberg, Schwedenschanze, Arnsberg sowie den Kreuzberg selbst.
Der Weg verlangte einiges von uns ab. Gerade deshalb sind wir stolz darauf, dass es am Ende alle auf den Berg geschafft haben.
Ein großes Lob geht an dieser Stelle an die Vertreterinnen der MJF Fulda, die uns wieder einmal bewiesen haben, wie durchsetzungsstark man als Frau sein kann.

Seit einigen Jahren haben wir uns den 23. Dezember für die Wanderung ausgesucht. Offensichtlich hatten diese Idee aber auch schon andere. Bei unserer Ankunft gegen 15:30 Uhr war es so voll, dass wir Angst darum haben mussten, überhaupt einen Sitzplatz zu bekommen. Nach kurzer Suche siegte die Mitmenschlichkeit und wir konnten uns einen halben Tisch sichern.

Dank des großen Aufgebots an Polizeikräften blieben größere Pannen aus. Auch die aus Dubai angereisten Wanderer konnten ihren Weg erfolgreich beenden. Was bleibt uns also als Fazit?

Zur Umsatzoptimierung schlagen wir dem Team der Bäckerei Happ eine Verlängerung der Öffnungszeiten ihrer Gersfelder Filiale um 15 Minuten vor!

An dieser Stelle noch einmal ein großes Dankeschön an die weitgereisten Abholer aus fernen Königreichen, ostasiatischen Volksrepubliken, einem von unbeugsamen Spessartbewohnern bevölkerten Dorf und aus unserem Startpunkt Dietershausen. Ohne euch wären wir wohl nie wieder zurück gekommen…

<figure class="full-width">
  <img src="https://farm5.staticflickr.com/4863/46487607771_4e5019ce5c_z_d.jpg" />
</figure>
