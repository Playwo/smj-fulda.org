---
tags: [smjfulda, noahstafelrunde]
migrated:
  node_id: 904
  migrated_at: 2017-02-21 14:41
  alias: artikel/904-die-smj-plant-2012
  user_id: 1
tagline: Am Planungstreffen der SMJ-Fulda haben 24 Verantwortliche das Veranstaltungsprogramm für das kommende Jahr geplant.
image:
  cover: https://farm8.staticflickr.com/7172/6766089713_92008747c2_b.jpg
title: Die SMJ-Fulda startet in 2012
created_at: 2012-01-26 09:52
excerpt: Am vergangenen Wochenende trafen sich die Verantwortlichen der Schönstatt Mannesjugend im Bistum Fulda zu ihrem Planungstreffen „Noahs Tafelrunde” um das Jahresprogramm der SMJ-Fulda auf die Beine zu stellen. Insgesamt kamen 24 junge Männer im Schönstattzentrum Dietershausen zusammen. Durch den Nachwuchs von 10 neuen Gruppenleitern, die sich ehrenamtlich für die Jugendarbeit begeistern, kann ein breites Spektrum angeboten werden. Und mit rund rund 40 Veranstaltungen ist der Kalender 2012 gut gefüllt.
---
<p>Am vergangenen Wochenende trafen sich die Verantwortlichen der Schönstatt Mannesjugend im Bistum Fulda zu ihrem Planungstreffen &bdquo;Noahs Tafelrunde&rdquo; um das Jahresprogramm der SMJ-Fulda auf die Beine zu stellen. Insgesamt kamen 24 junge Männer im Schönstattzentrum Dietershausen zusammen.</p>
<p>Durch den Nachwuchs von 10 neuen Gruppenleitern, die sich ehrenamtlich für die Jugendarbeit begeistern, kann ein breites Spektrum angeboten werden. Und mit rund rund 40 Veranstaltungen ist der Kalender 2012 gut gefüllt.</p>
<p>Die im Dezember neu gewählte Diözesanführung bestehend aus Steffen Büdel und Christoph Schopp erwartet ein ereignisreiches Jahr und ist froh, mit einem engagierten Team arbeiten zu können: &bdquo;Ich freue mich auf das Jahr 2012 und unsere Aktionen, die nur möglich sind, weil wir als Gruppenleiter zusammen eine richtig gute Gemeinschaft bilden.&rdquo;, so der Diözesanführer Steffen Büdel.</p>
<figure data-href="https://www.flickr.com/photos/45962678@N06/6766089713">
  <img src="https://farm8.staticflickr.com/7172/6766089713_92008747c2_c.jpg" class="flickr-img flickr-img--medium" alt="Gruppenleiter im Gespräch" />
  <figcaption>Gruppenleiter im Gespräch (<time datetime="2012-01-21 11:43:08">2012</time>)</figcaption>
</figure>
<p>Das Highlight ist sicherlich das <strong><a href="/zeltlager">Zeltlager</a></strong> für 9-13-jährige Jungen, das dieses Jahr vom 3. bis 14. Juli in Schönstatt stattfinden wird. Für dieselbe Altersgruppe gibt es dieses Jahr auch wieder vier <strong>Gemeinschaftswochenenden</strong>, an denen neben Spiel und Spaß auch altersgerecht religiöse Themen behandelt werden. Alle Veranstaltungen stehen dabei unter dem Thema <em>Die Siedler</em>, am <a href="/gewo">ersten Wochenende</a> mit dem Motto <em>Die erste Insel </em>(17.-19. Februar) werden Mut und Entdeckungslust gefordert sein.</p>
<figure data-href="https://www.flickr.com/photos/45962678@N06/6766089265">
  <img src="https://farm8.staticflickr.com/7151/6766089265_a4ec4b0f5a_c.jpg" class="flickr-img flickr-img--medium" alt="Noahs Tafelrunde" />
  <figcaption>Noahs Tafelrunde (<time datetime="2012-01-21 11:42:39">2012</time>)</figcaption>
</figure>
<p>Für ältere Jugendliche werden die im vergangenen Jahr begonnenen <a href="/aktionen#glaubenskurs"><strong>Glaubenskurs</strong>-Wochenenden</a> fortgeführt, die gemeinsam mit der SchönstattMJF, der Katholischen Jugend im Bistum Fulda (KJF) und dem Jugendbüro der Hünfelder Oblaten organisiert werden. Ein weiteres Gemeinschaftsprojekt ist das <strong><a href="/aktionen#fest-des-glaubens">Fest des Glaubens</a></strong> am 15. September. Zu diesem großen Jugendfest werden im Schönstattzentrum Dietershausen wieder mehrere hundert Teilnehmer ab 14 Jahren erwartet.</p>
<figure data-href="https://www.flickr.com/photos/45962678@N06/6766088759">
  <img src="https://farm8.staticflickr.com/7006/6766088759_7fe4f715ee_c.jpg" class="flickr-img flickr-img--medium" alt="Die Diözesanführung - Christoph Schopp und Steffen Büdel" />
  <figcaption>Die Diözesanführung - Christoph Schopp und Steffen Büdel (<time datetime="2012-01-21 11:42:12">2012</time>)</figcaption>
</figure>
<p>Auch über bestehende Projekte wie die <strong><em>iPray</em></strong>-Lobpreistour oder neue Werbestrategien wurden sich in großer Runde Gedanken gemacht. Neben dem 45-jährigen Jubiläum der SMJ-Fulda wird dieses Jahr auch das 100-jährige Bestehen der Schönstatt-Mannesjugend gefeiert: Dazu wird es im Oktober am Gründungsort Schönstatt ein <strong>Säulenfest</strong> geben, zum dem SMJler aus ganz Deutschland zusammen kommen werden. Als Geburtstagsgeschenk will die SMJ-Deutschland in Schönstatt fünf Säulen errichten, die für die Grundpfeiler der Mannesjugend stehen: Gemeinschaft, Lebensschule, Liebesbündnis, Mannsein und Apostelsein.</p>
<div class="box">Sämtliche Veranstaltungen finden sich in unserer <strong><a href="/termine">Terminübersicht</a>.</strong></div>
