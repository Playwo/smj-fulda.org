---
tags: [smjfulda, gewo]
migrated:
  node_id: 923
  migrated_at: 2017-02-20 22:27
  alias: artikel/923-die-entscheidung-ist-gefallen,-cowboy
  user_id: 132
tagline: Das Thema zum Zeltlager 2013 ist Wilder Western. Also, satteln wir die Pferde und reiten los!
author: Steffen Büdel
title: Die Entscheidung ist gefallen, Cowboy!
created_at: 2012-11-10 11:03
excerpt: 'Das Thema der SMJ-Fulda für das kommende Jahr lautet: <strong>Wilder Westen</strong>! Das haben die Gruppenleiter unserer SMJ kürzlich beschlossen. Dieses Thema wird sich durch das ganze Jahr durchziehen, besonders die vier Gemeinschaftswochenenden und natürlich das Zeltlager in Hausen/Hillenberg vom 9. bis 20. Juli werden von diesem Thema geprägt sein. Dieses Jahr wird es noch ein weiteres Gemeinschaftswochenende für Jungs im Alter von 9 bis 13 Jahren geben. Das Wochenende findet vom 14. bis 16. Dezember im Schönstattzentrum Dietershausen statt.'
---
<p>Das Thema der SMJ-Fulda für das kommende Jahr lautet: <strong>Wilder Westen</strong>! Das haben die Gruppenleiter unserer SMJ kürzlich beschlossen. Dieses Thema wird sich durch das ganze Jahr durchziehen, besonders die vier Gemeinschaftswochenenden und natürlich das Zeltlager in Hausen/Hillenberg vom 9. bis 20. Juli werden von diesem Thema geprägt sein.</p>
<p>Dieses Jahr wird es noch ein weiteres Gemeinschaftswochenende für Jungs im Alter von 9 bis 13 Jahren geben. Das Wochenende findet vom 14. bis 16. Dezember im Schönstattzentrum Dietershausen statt.</p>
<div class="box">Anmeldung an:<br />
	Steffen Büdel<br />
	Königsbergstraße 38<br />
	63637 Jossgrund<br />
	Tel.: 0151/28045824<br />
	e-mail: steffen.buedel@smj-fulda.org</div>
<p>Auch für sonstige Fragen rund um die SMJ Fulda stehe ich gerne zur Verfügung.</p>
