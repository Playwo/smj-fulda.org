---
tags: [smjdeutschland, jako]
migrated:
  node_id: 898
  migrated_at: 2017-02-20 22:27
  alias: artikel/898-die-weichen-sind-gestellt
  user_id: 1
tagline: 'Auf der Jahreskonferenz 2011 der Schönstatt-Mannesjugend wurden Zeichen gesetzt und die neue Jahresparole festgelegt: <em>Tiefe® leben – richtungsweisend</em>'
title: Die Weichen sind gestellt
created_at: 2012-01-10 14:05
excerpt: Vom 26. bis 31. Dezember 2011 fand die Jahreskonferenz der Schönstatt-Mannesjugend im Jugendzentrum Marienberg in Schönstatt-Vallendar statt. 80 Teilnehmer aus dem ganzen Bundesgebiet setzen richtungsweisende Zeichen, beschlossen das Jahresmotto ihrer Gemeinschaft für das Jahr 2012 und fokussierten sich auf die Feier von 100 Jahren Schönstatt-Mannesjugend (SMJ) am 27. Oktober 2012. <a href="http://cmsms.schoenstatt.de/de/news/1381/112/Die-Weichen-sind-gestellt.htm" rel="nofollow"><br> <strong>Die Weichen sind gestellt</strong> auf schoenstatt.de lesen »</a>
---
<p>Vom 26. bis 31. Dezember 2011 fand die Jahreskonferenz der Schönstatt-Mannesjugend im Jugendzentrum Marienberg in Schönstatt-Vallendar statt. 80 Teilnehmer aus dem ganzen Bundesgebiet setzen richtungsweisende Zeichen, beschlossen das Jahresmotto ihrer Gemeinschaft für das Jahr 2012 und fokussierten sich auf die Feier von 100 Jahren Schönstatt-Mannesjugend (SMJ) am 27. Oktober 2012.</p>
<div class="box" style="text-align: center;"><a href="http://cmsms.schoenstatt.de/de/news/1381/112/Die-Weichen-sind-gestellt.htm"><img src="http://cmsms.schoenstatt.de/images/news/2012/01/20120186Jako_SMJ_ALIM2513.jpg" width="620" /><br />
	<strong>Die Weichen sind gestellt</strong> auf schoenstatt.de lesen &raquo;</a></div>
