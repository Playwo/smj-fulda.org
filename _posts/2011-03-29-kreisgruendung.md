---
categories:
- Kreisarbeit
migrated:
  node_id: 865
  migrated_at: 2017-02-20 22:26
  alias: artikel/865-kreisgründung
  user_id: 5
author: Tobias Buedel
title: Kreisgründung
created_at: 2011-03-29 14:55
excerpt: Ganz im Zeichen der diesjährigen Jahresparole „Aufbruch – Zeichen setzen” haben wir vergangenes Wochenende einen neuen Kreis gründen dürfen. Die Jugendarbeit der vergangenen Jahre trägt also weiter ihre Früchte. Zu den bereits bestehenden Kreisen dazu gründeten nun die Jungen der Geburtsmonate Juli 1996 bis Dezember 1997 mit mir, ihrem Kreisleiter Tobias Büdel, einen neuen Kreis der SMJ Fulda. Zu diesem ersten Kreistreffen haben sich sieben Jungen zusammengefunden. Ich rechne aber mit circa 10 Jungen, die regelmäßig die Kreistreffen besuchen werden.
---
<p>Ganz im Zeichen der diesjährigen Jahresparole &bdquo;Aufbruch &ndash; Zeichen setzen&rdquo; haben wir vergangenes Wochenende einen neuen Kreis gründen dürfen.</p>
<p>Die Jugendarbeit der vergangenen Jahre trägt also weiter ihre Früchte. Zu den bereits bestehenden Kreisen dazu gründeten nun die Jungen der Geburtsmonate Juli 1996 bis Dezember 1997 mit mir, ihrem Kreisleiter Tobias Büdel, einen neuen Kreis der SMJ Fulda.</p>
<p>Zu diesem ersten Kreistreffen haben sich sieben Jungen zusammengefunden. Ich rechne aber mit circa 10 Jungen, die regelmäßig die Kreistreffen besuchen werden.</p>
<p>Um die Kreisgründung herum haben wir uns mit dem Thema Gemeinschaft beschäftigt. In einer gut funktionierenden Gemeinschaft sollte man sich sehr gut kennen. Dies zu lernen haben wir am Freitag Abend sehr gut gemeistert. Anhand eines selbst erstellten Profilbildes war es den Jungen ein Leichtes, über sich einiges zu erzählen und über den anderen viel bekanntes und neues kennenzulernen.</p>
<p>Die Fragestellung nach Eigenschaften einer Gemeinschaft beschäftigte uns sehr. Wir haben viele Punkte notiert auf denen wir an weiteren Treffen sehr gut aufbauen können. Als bildliche Darstelung einer Gemeinschaft, in der alle mit ihren Stärken und Schwächen akzeptiert werden, diente uns der bereits mehrfach ausgezeichnete Kurzfilm &bdquo;The Butterfly Circus&rdquo; (Der Schmetterlingszirkus).</p>
<p>Neben unseren thematischen Einheiten stand die Erholung, das Spiel und der Spaß ebenfalls sehr hoch im Kurs. Im Ganzen war es ein super Wochenende und ich als Kreisleiter freue mich schon sehr auf die kommenden Treffen mit den aufgeweckten Jungen!</p>
