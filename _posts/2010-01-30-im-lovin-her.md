---
created: 2010-01-30
title: I'm loving her
author: Johannes Müller
---
Die Mutter Gottes im Stil einer McDonald’s-Werbung, klasse gemacht. Statt *I’m lovin it* heißt es *I’m lovin her*. Statt Fast-food gibt es Fast-Gnadenstrom. Vielleicht sollte man an jedem Kapellchen ein großes *M* aufstellen?

<iframe width="560" height="315" src="https://www.youtube.com/embed/0PWv0PgzLoE" frameborder="0" allowfullscreen></iframe>

Interessant dazu ist auch der Artikel [Das McDonald’s-Prinzip]({{ site.baseurl }}/artikel/2010-01-30-das-mcdonalds-prinzip/) von Sebastian Hartmann.
