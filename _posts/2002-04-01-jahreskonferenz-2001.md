---
categories:
- jako
- '2002'
- '2001'
migrated:
  node_id: 687
  migrated_at: 2017-02-20 22:25
  alias: artikel/687-jahreskonferenz-2001
  user_id: 0
title: Jahreskonferenz 2001
created_at: 2002-04-01 13:19
excerpt: Zu einer Jahreskonferenz der SMJ Deutschland treffen sich alle Chefs (Diözesanführer,...) aller Diözesen und regionalen Unterabteilungen im Jugendzentrum, dem Zentrum der Mannesjugend, am Marienberg in Schönstatt. Die Mannesjugend Deutschland ist zusätzlich noch in Regionen eingeteilt, welche mehrere Diözesen zusammenschließt. Es gibt die Regio Wegweiser (Ostdeutschland), Erz-Regio Mitte, Leuchtturm (Norden,klar), Südwest, und die Regio Bayern. Ein herzliches Servus auch an die Vertreter der österreichischen Mannesjugend, die dieses Jahr auch teilnahmen.
---
<p>Zu einer Jahreskonferenz der SMJ Deutschland treffen sich alle Chefs (Diözesanführer,...) aller Diözesen und regionalen Unterabteilungen im Jugendzentrum, dem Zentrum der Mannesjugend, am Marienberg in Schönstatt. Die Mannesjugend Deutschland ist zusätzlich noch in Regionen eingeteilt, welche mehrere Diözesen zusammenschließt. Es gibt die Regio Wegweiser (Ostdeutschland), Erz-Regio Mitte, Leuchtturm (Norden,klar), Südwest, und die Regio Bayern.</p>
<p>Ein herzliches Servus auch an die Vertreter der österreichischen Mannesjugend, die dieses Jahr auch teilnahmen.</p>
<p>In einem fünftägigen Programm konferierten die Teilnehmer über bewegende Aktionen des letzten Jahres genauso wie über Ausblicke und Perspektiven unserer Arbeit im nächsten Jahr. Verschiedenste Referenten berichteten über ihre Art Schönstatt im Alltag umzusetzen, Schwierigkeiten beim Apostelsein, Networking und Marketing, etc.</p>
<p>In einer anstrengenden Endphase wurde fünf Stunden über das neue Jahresmotto gebrütet, welches unsere Arbeit 2002 prägen und antreiben, sowie andere, Außenstehende, interessieren soll. In Anlehnung an das diesjährige JaKo Motto, lautet unsere Parole für 2002:</p>
<p>Neben viel gemütlichen Zusammensein (Weihnachtsmarkt und Christkind im JZM, Spieleabend, Kicker und Ratskeller, Bayernhymne jeden Abend um 0.00 Uhr (wunderbar patriotisch), mehrgängiges Galadinner mit edlen Tropfen, ...), konnte auch diesmal wieder richtige tiefe Spiritualität in Gottesdiensten, Anbetung, regl. Gebet, usw. erlebt werden.</p>
<p>Auf geht’s SMJ 2002</p>
