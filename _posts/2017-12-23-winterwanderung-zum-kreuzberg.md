---
title: "Winterwanderung zum Kreuzberg"
excerpt: >
  Die Gipfelstürmer der SMJ-Fulda haben trotz widriger Witterung die traditionelle Kreuzbergwanderung am Tag vor Heiligabend gemeistert.
author: Philipp Müller
tags: [smjfulda, kreuzbergwanderung]
image:
    cover: "https://farm5.static.flickr.com/4685/25380030778_1f3e5141f6_o_d.jpg"
    align: top
---

Die alljährliche Kreuzbergwanderung der SMJ-Fulda am 23. Dezember wurde erfolgreich beendet. Die schwierigen Wetterverhältnisse haben die Gipfelstürmer Simon, Maxi und Philipp nicht abhalten können.

Schwer machte den Weg Schnee in den Farben weiß, gelb, braun und schwarz, sowie Eis, Matsch (nicht von Gulaschklößchen), abfließendes Schmelzwasser, Nebel mit ca. 15 Meter Sichtweite und Wind, der die Teilnehmer fast von der Straße fegte. Letzteres konnte glücklicherweise dank guter Vorarbeit am heimischen Essenstisch verhindert werden.

Leider musste an der *Hähnchen Paula* in Sparbrod der Abbruch der Wanderung zweier Weggefährten hingenommen werden. Die beiden haben jedoch immerhin bis dorthin durchgehalten und sich nicht schon vor dem Start disqualifiziert. Weiterhin sei ihnen ihr Abholdienst gedankt. 

Zur Wanderung im Jahr 2018 möchten wir bereits jetzt einladen. Möglicherweise wird dabei eine Seltenheit in der Tradition der Wanderung mitzuerleben sein, wenn sie bereits auf den 22. Dezember verlegt wird, um nicht mit dem Sonntag zu kollidieren.

<figure class="flickr-photoset full-width" data-href="https://www.flickr.com/photos/45962678@N06/sets/72157667712349059">
  <a href="https://www.flickr.com/photos/45962678@N06/39218377802/in/album-72157667712349059"><img src="https://farm5.staticflickr.com/4734/39218377802_5dfa4ecc11_q.jpg" alt="20171223_145248" data-src-large="https://farm5.staticflickr.com/4734/39218377802_5dfa4ecc11_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/25380031208/in/album-72157667712349059"><img src="https://farm5.staticflickr.com/4734/25380031208_d58095236e_q.jpg" alt="" data-src-large="https://farm5.staticflickr.com/4734/25380031208_d58095236e_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/24384294627/in/album-72157667712349059"><img src="https://farm5.staticflickr.com/4686/24384294627_17c53ee163_q.jpg" alt="" data-src-large="https://farm5.staticflickr.com/4686/24384294627_17c53ee163_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/25380030778/in/album-72157667712349059"><img src="https://farm5.staticflickr.com/4685/25380030778_12e53d03ec_q.jpg" alt="20171223_145315" data-src-large="https://farm5.staticflickr.com/4685/25380030778_12e53d03ec_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/25380029728/in/album-72157667712349059"><img src="https://farm5.staticflickr.com/4730/25380029728_ecd904e609_q.jpg" alt="20171223_122455" data-src-large="https://farm5.staticflickr.com/4730/25380029728_ecd904e609_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/sets/72157667712349059" class="flickr-link" title="Bildergalerie Winterwanderung zum Kreuzberg auf Flickr">Bildergalerie</a>
</figure>
