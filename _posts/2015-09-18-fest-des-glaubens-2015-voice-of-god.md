---
tags: [smjfulda, festdesglaubens, mjffulda, kjf, omijugend]
migrated:
  node_id: 1008
  migrated_at: 2017-02-20 22:28
  alias: artikel/1008-fest-des-glaubens-2015:-voice-god
  user_id: 1
tagline: Katholisches Jugendfest im Schönstattzentrum als Gemeinschaftsprojekt mit SchönstattMJF, Katholischer Jugend im Bistum Fulda und OMI-Jugend.
image:
  cover: https://farm6.staticflickr.com/5620/21323665210_90f7f516df_b.jpg
title: 'Fest des Glaubens 2015: Voice of God'
created_at: 2015-09-18 15:29
excerpt: Am vergangenen Wochenende war wieder einmal das Fest des Glaubens in Dietershausen. Rund 300 Jugendliche aus dem ganzen Bistum waren gekommen um Weihbischof Marian Eleganti und der Jesus-Online-Band zuzuhören, Workshops zu machen, Messe zu feiern und einfach Gemeinschaft im Glauben zu erleben. Das nächste Fest des Glaubens ist am 10. September 2016, leider nicht in Dietershausen sondern im Bonifatiuskloster Hünfeld. <a href="http://festdesglaubens.de/2015/the-voice-of-god-fdg-2015" rel="nofollow">Bericht vom Fest</a> <a href="https://www.flickr.com/photos/straight-shoota/albums/72157658746430381" rel="nofollow">Bildergalerie</a>
---
<p>Am vergangenen Wochenende war wieder einmal das Fest des Glaubens in Dietershausen. Rund 300 Jugendliche aus dem ganzen Bistum waren gekommen um Weihbischof Marian Eleganti und der Jesus-Online-Band zuzuhören, Workshops zu machen, Messe zu feiern und einfach Gemeinschaft im Glauben zu erleben.</p>
<p>Das nächste Fest des Glaubens ist am 10. September 2016, leider nicht in Dietershausen sondern im Bonifatiuskloster Hünfeld.</p>
<p><a href="http://festdesglaubens.de/2015/the-voice-of-god-fdg-2015">Bericht vom Fest</a></p>
<p><a href="https://www.flickr.com/photos/straight-shoota/albums/72157658746430381">Bildergalerie</a></p>
