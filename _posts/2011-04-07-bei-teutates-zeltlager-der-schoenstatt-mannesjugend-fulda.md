---
categories:
- Zeltlager
migrated:
  node_id: 866
  migrated_at: 2017-02-20 22:26
  alias: artikel/866-bei-teutates-zeltlager-der-schönstatt-mannesjugend-fulda
  user_id: 9
tagline: Presseartikel vom 06.04.2011
author: Sebastian Hartmann
title: Bei Teutates! Zeltlager der Schönstatt Mannesjugend Fulda
created_at: 2011-04-07 00:42
excerpt: "  Die ganzen Ferienangebote im Bistum Fulda sind besetzt – im ganzen Bistum? Nein! Eine Gruppe aufständischer Kinder mit ihren Betreuern der Schönstatt Mannesjugend Fulda leistet der Langeweile in den Sommerferien erbitterten Widerstand. Vom 28. Juno bis in die Iden des 9. Julius anno 2011 wird in Oberweißenbrunn am Fuße des heiligen Berges der Franken das gallische Zeltdorf für junge Männer im Alter von 9-13 Jahren die Gelegenheit geben, sich den Gefahren der Wildnis zu stellen. Wildschweinjagd (Geländespiel), Nachtwache, Lagerfeuer, Dorffeste, Olympiaden, Lederballjagd (Fußball), … und viele andere spannende Aktionen dürfen dabei natürlich nicht fehlen."
---
<div>&nbsp;</div>
<div>Die ganzen Ferienangebote im Bistum Fulda sind besetzt &ndash; im ganzen Bistum? Nein! Eine Gruppe aufständischer Kinder mit ihren Betreuern der Schönstatt Mannesjugend Fulda leistet der Langeweile in den Sommerferien erbitterten Widerstand.</div>
<div>Vom 28. Juno bis in die Iden des 9. Julius anno 2011 wird in Oberweißenbrunn am Fuße des heiligen Berges der Franken das gallische Zeltdorf für junge Männer im Alter von 9-13 Jahren die Gelegenheit geben, sich den Gefahren der Wildnis zu stellen. Wildschweinjagd (Geländespiel), Nachtwache, Lagerfeuer, Dorffeste, Olympiaden, Lederballjagd (Fußball), &hellip; und viele andere spannende Aktionen dürfen dabei natürlich nicht fehlen. Sei dabei wenn die Operation Hinkelstein die jungen Dorfbewohner ruft, Lagerbauten wie Fahnenmast, Lagerkreuz oder Befestigungen zu errichten. Für die tägliche Stärkung sorgt Schmecktnixgibtsnix. Werkzeuge zum Errichten von Lagerfeuern oder selbstgebauten Schuhunterständen gibt es im Werkzeugzelt bei Verleihnix und Automatix. Abends lädt Troubadix zum Mitsingen rund ums Lagerfeuer ein. Idefixe (Hunde), Handys, MP3 Player und andere technischen Geräte dürfen leider nicht an dem Abenteuer teilnehmen.</div>
<div>&nbsp;</div>
<div>Neben Spiel und Spaß steht bei uns als katholische Bewegung aber auch die Vermittlung von christlichen Werten auf dem Tagesplan. Begleitet wird unser Zeltlager von einem Druiden (Priester) und jungen ehrenamtlichen Dorfältesten (Jugendleiter), die eine Gruppenleiterschulung absolviert haben.</div>
<div>&nbsp;</div>
<div>Eingeladen sind alle jungen Männer im Alter von 9-13 Jahren, die sich auf ein Abenteuer einlassen und für 12 Tage echte Gallier sein wollen.</div>
<div>Anmeldung und weitere Informationen finden Sie unter:</div>
<div>&nbsp;</div>
<div><a href="http://www.smj-fulda.org/node/856">www.smj-fulda.org</a></div>
<div>&nbsp;</div>
<div>oder direkt bei unserem Majestatix</div>
<div>Tobias Büdel</div>
<div>Königsbergstraße 38</div>
<div>63637 Jossgrund</div>
<div>0160 90288138</div>
<div>&nbsp;</div>
<div>Also worauf wartet Ihr? Die Gallier sind los!</div>
