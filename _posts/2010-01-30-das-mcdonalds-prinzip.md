---
categories:
- Sonstiges
- Prinzip
- McDonald’s
- Mc Donald's
- kapellchen
- Heimat
migrated:
  node_id: 819
  migrated_at: 2017-02-20 22:25
  alias: artikel/819-das-mc-donalds-prinzip
  user_id: 9
tagline: Gedanken über Heimat und Sicherheit
author: Sebastian Hartmann
title: Das McDonald’s-Prinzip
created_at: 2010-01-30 00:15
excerpt: Warum ist die Fast-Food-Kette Mc Donald's so erfolgreich? Was steckt dahinter? Und was hat das alles mit der Schönstattbewegung zu tun?
---
<p>Warum ist die Fast-Food-Kette Mc Donald's so erfolgreich? Was steckt dahinter? Und was hat das alles mit der Schönstattbewegung zu tun?</p>
<p>Fragen über Fragen. Doch der Reihe nach. Jeder kennt sie und die meisten Leute speisen dort regelmäßig: Die bekannte Fast-Food-Kette &quot;Mc Donald's&quot;. Die Meinungen über dieses Unternehmen gehen weit auseinander. Von &quot;heiß geliebt&quot; bis hin zu &quot;bewusst gemieden&quot; ist wohl jede Haltung vertreten. Doch eine Sache muss man den Mc Donald's Restaurants doch eingestehen. Sie sind überall und jeder kennt sie. Das soll keinesfalls eine Werbung für diese Kette sein, jedoch wird das Phänomen an ihrem Beispiel am besten deutlich. Wer schonmal französische Küche ertragen musste, der weiß, was es bedeutet eine goldene Möwe am Himmel zu sehen. Ob Spar-Menü oder Big Mac, ob in Rom oder London, der Geschmack, die Ausstattung, einfach alles ist überall gleich. Man kennt sich sofort aus. Menschen neigen dazu Altbekanntes zu schätzen und fühlen sich wohl in einer vertrauten Umgebung. Genau das bietet eine Mc Donald's Filiale. Dahinter steckt eine grandiose Marketingstrategie.</p>

<p>Die Menschen lieben ein Umfeld, indem sie sich auskennen und wohlfühlen. Warum sollte das Mc Donald's Prinzip nicht auch in unserem Glaubensleben funktionieren? Was der Mehrung von Geld und leiblichem Wohl zugutekommt, das wird der Gnade und dem Geiste wohl kaum schaden. Die Schönstattkapellchen auf der ganzen Welt bieten dieses Gefühl der Heimat und Geborgenheit. Ist man in einer fremden Stadt oder einem fremden Land und betritt ein Schönstattheiligtum, so fühlt man sich sofort wohl. Man kennt alles. Man fühlt sich gut. Hier ist Gott und hier ist die Gottesmutter genauso wie überall auf der Welt. Das Gefühl zu beschreiben ist kaum möglich und der Vergleich mit Mc Donald's wird dem ganzen Ausmaß sicherlich nicht gerecht, aber wenn man in einer fremden Stadt (so z.B. in Köln am WJT 2005) in ein Schönstattheiligtum tritt, dann ist es so wie wenn man nach einer Woche Frankreich endlich in einen Big Mac beißen kann (Freunde der französischen Küche mögen mir verzeihen), nur noch viel besser.</p>
<p>Unser Glaube ist mit Sicherheit keine leichte Kost aber das Heimatgefühl und die Stärke, die wir im Heiligtum erfahren können/dürfen, ist unglaublich groß. Lasst uns gemeinsam andere auf den Geschmack des Glaubens bringen. Viele Menschen sehnen sich nach Sicherheit und Heimat. Laden wir sie ein die Gnade Gottes zu kosten.</p>
<div class="box extra"><h3>Bescheid gewusst</h3>
<p>Zur Zeit gibt es weltweit 195 Kapellchen, davon 56 in Deutschland, 31 im restlichen Europa und 74 in Südamerika.</p>
<p>Die Schönstatt-Website <a href="http://santuarios.schoenstatt.de/">santuarios.schoenstatt.de</a> bietet einen Überblick über alle Schönstattheiligtümer der Welt.</p>
<p>Zm Thema McDonald&rsquo;s gibt es auch einen tollen Werbespot-Spoof für die MTA:&nbsp;<a href="/artikel/2010-01-30-im-lovin-her/">Im Lovin&rsquo; her</a></p>
</div>
