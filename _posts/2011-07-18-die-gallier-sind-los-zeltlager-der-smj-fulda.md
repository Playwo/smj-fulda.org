---
tags: [smjfulda, zeltlager]
migrated:
  node_id: 869
  migrated_at: 2017-02-21 14:03
  alias: zeltlager/2011
  user_id: 1
tagline: Gallier-Zeltlager in Oberweißenbrunn
image:
  cover: https://farm7.staticflickr.com/6136/5926109618_a251bed8d2_b.jpg
title: Die Gallier sind los! Zeltlager der SMJ-Fulda
created_at: 2011-07-18 14:34
excerpt: "<strong>Wir befinden uns im Jahre MMXI n. Chr. Ganz Gallien ist von den Römern besetzt… Ganz Gallien? Ja! Bei Teutates, sogar das Dorf der unbeugsamen Gallier ist von den Römern eingenommen worden! Während Majestix mit seinen Kriegern und Dorfältesten die jungen Gallier in Dietershausen begrüßte, haben römische Truppen das verlassene Dorf besetzt. Doch die mutigen Gallier machten sich auf nach Oberweißenbrunn und nach einer kleinen Keilerei war das Dorf zurückerobert.</strong>"
---
<p><strong>Wir befinden uns im Jahre MMXI n. Chr. Ganz Gallien ist von den Römern besetzt&hellip; Ganz Gallien? Ja! Bei Teutates, sogar das Dorf der unbeugsamen Gallier ist von den Römern eingenommen worden! Während Majestix mit seinen Kriegern und Dorfältesten die jungen Gallier in Dietershausen begrüßte, haben römische Truppen das verlassene Dorf besetzt. Doch die mutigen Gallier machten sich auf nach Oberweißenbrunn und nach einer kleinen Keilerei war das Dorf zurückerobert.</strong></p>
<figure data-href="https://www.flickr.com/photos/45962678@N06/5925374751">
  <img src="https://farm7.staticflickr.com/6143/5925374751_0c179ef6fb_c.jpg" class="flickr-img flickr-img--medium" alt="Montag" />
  <figcaption>Montag (<time datetime="2011-07-04 09:28:22">2011</time>)</figcaption>
</figure>
<p>Noch am selben Tag begann der Wiederaufbau und der Zeltplatz am Himmeldunkberg wurde &nbsp;vom 28. Juni bis 9. Juli 2011 zur Heimat für 46 Jungen im Zeltlager der Schönstatt-Mannesjugend (SMJ) im Bistum Fulda. Während dieser zwölf Tage warteten viele Aufgaben auf die unbeugsamen Gallier: Kaum war das Dorf wieder aufgebaut, schickte Majestix seine jungen Krieger bereits wieder los, um das Land in der Umgebung zu erkunden. Während dieser 2-Tagestour waren die einzelnen Zeltgruppen zu unterschiedlichen Zielorten unterwegs und mussten dort eine Unterkunft suchen. Dabei konnte ein Palast gefunden werden, der dem alten Julius würdig wäre, oder es boten einfache Germanen ein Dach an, die den Widerstand gegen die Römer unterstützen. Am nächsten Tag kamen alle wieder wohlbehalten und mit guten Erinnerungen ins Dorf zurück.</p>
<p>Neben inhaltlichen Elementen wie Referaten zur Persönlichkeitsbildung und religiösen Themen, die für eine katholische Jugendfreizeit sehr wichtig sind, gab es auch einen Stationlauf, Nachtwanderung, ein Besuch um Haus der kleinen Wunder und sogar eine Olympiade in Gallien!</p>
<figure data-href="https://www.flickr.com/photos/45962678@N06/5926139794">
  <img src="https://farm7.staticflickr.com/6006/5926139794_c5dc65331a_c.jpg" class="flickr-img flickr-img--medium" alt="Dienstag" />
  <figcaption>Dienstag (<time datetime="2011-07-05 16:17:00">2011</time>)</figcaption>
</figure>
<p>Als Miraculix das Rezept &nbsp;für den Zaubertrank vergaß, wurde es einmal brenzlig für die Dorfbewohner. Doch gemeinsam schafften es die Gallier, das Rezept wiederzuerlangen und die notwendigen Zutaten zu besorgen. Rechtzeitig zum Angriff der römischen Legion hatte Miraculix den Zaubertrank fertig gebraut und nach einem Schluck schmackhaften Zaubertranks stand der für die Römer nicht ganz so lustigen Rauferei &nbsp;nichts mehr im Weg. Danach konnten alle wieder das gewohnte Dorfleben mit Lagerfeuer, Fußballspielen und einem Schwimmbadbesuch genießen.</p>
<p>Doch die Römer wollten nicht aufgeben und versuchten noch einmal, die Gallier zu unterwerfen. Zwar wurde der Häuptling Majestix von listigen Legionären entführt, doch dank ihrer vereinten Kampfesstärke bewiesen die Gallier endgültig ihren Triumph über Rom. Somit ging das Abenteuer glücklich zu Ende und wurde mit einem großen Festmahl gefeiert.</p>
<figure class="flickr-photoset full-width" data-href="https://www.flickr.com/photos/45962678@N06/sets/72157627105780668">
  <a href="https://www.flickr.com/photos/45962678@N06/5896681365/in/album-72157627105780668"><img src="https://farm6.staticflickr.com/5235/5896681365_b6f682c2b8_q.jpg" alt="0005-P1010025" data-src-large="https://farm6.staticflickr.com/5235/5896681365_b6f682c2b8_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5896682261/in/album-72157627105780668"><img src="https://farm6.staticflickr.com/5160/5896682261_c78e2d548f_q.jpg" alt="0004-P1010024" data-src-large="https://farm6.staticflickr.com/5160/5896682261_c78e2d548f_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5896683199/in/album-72157627105780668"><img src="https://farm6.staticflickr.com/5068/5896683199_9f628a6ea2_q.jpg" alt="0006-P1010026" data-src-large="https://farm6.staticflickr.com/5068/5896683199_9f628a6ea2_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5896684327/in/album-72157627105780668"><img src="https://farm7.staticflickr.com/6050/5896684327_e28cb463a6_q.jpg" alt="0007-P1010027" data-src-large="https://farm7.staticflickr.com/6050/5896684327_e28cb463a6_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5896685417/in/album-72157627105780668"><img src="https://farm6.staticflickr.com/5319/5896685417_7d921781bb_q.jpg" alt="0008-P1010028" data-src-large="https://farm6.staticflickr.com/5319/5896685417_7d921781bb_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5896686435/in/album-72157627105780668"><img src="https://farm6.staticflickr.com/5034/5896686435_fcd64849e0_q.jpg" alt="0009-P1010029" data-src-large="https://farm6.staticflickr.com/5034/5896686435_fcd64849e0_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5896687481/in/album-72157627105780668"><img src="https://farm7.staticflickr.com/6057/5896687481_ecd0f1158e_q.jpg" alt="0010-P1010030" data-src-large="https://farm7.staticflickr.com/6057/5896687481_ecd0f1158e_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5896688445/in/album-72157627105780668"><img src="https://farm6.staticflickr.com/5264/5896688445_470e88fabc_q.jpg" alt="0011-P1010031" data-src-large="https://farm6.staticflickr.com/5264/5896688445_470e88fabc_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5896689419/in/album-72157627105780668"><img src="https://farm6.staticflickr.com/5265/5896689419_a9b40c15c2_q.jpg" alt="0012-P1010032" data-src-large="https://farm6.staticflickr.com/5265/5896689419_a9b40c15c2_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5896690489/in/album-72157627105780668"><img src="https://farm6.staticflickr.com/5156/5896690489_0d56661eb3_q.jpg" alt="0013-P1010033" data-src-large="https://farm6.staticflickr.com/5156/5896690489_0d56661eb3_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5896691621/in/album-72157627105780668"><img src="https://farm7.staticflickr.com/6021/5896691621_de496e0949_q.jpg" alt="0014-P1010034" data-src-large="https://farm7.staticflickr.com/6021/5896691621_de496e0949_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5896692649/in/album-72157627105780668"><img src="https://farm7.staticflickr.com/6007/5896692649_520edc3ae5_q.jpg" alt="0015-P1010035" data-src-large="https://farm7.staticflickr.com/6007/5896692649_520edc3ae5_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5897260808/in/album-72157627105780668"><img src="https://farm7.staticflickr.com/6030/5897260808_e59fb827f4_q.jpg" alt="0019-P1010039" data-src-large="https://farm7.staticflickr.com/6030/5897260808_e59fb827f4_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5896694991/in/album-72157627105780668"><img src="https://farm6.staticflickr.com/5151/5896694991_4ebf31b5a5_q.jpg" alt="0018-P1010038" data-src-large="https://farm6.staticflickr.com/5151/5896694991_4ebf31b5a5_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/sets/72157627105780668" class="flickr-link" title="Bildergalerie Zeltlager 2011 auf Flickr">Bildergalerie</a>
</figure>
