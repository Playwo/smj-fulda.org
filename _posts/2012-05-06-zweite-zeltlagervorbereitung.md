---
tags: [smjfulda, zeltlager]
migrated:
  node_id: 912
  migrated_at: 2017-02-20 22:27
  alias: artikel/912-zweite-zeltlagervorbereitung
  user_id: 1
tagline: Verantwortliche der SMJ-Fulda planten das Zeltlager 2012
image:
  cover: https://farm8.staticflickr.com/7101/7003429812_9fa4be762f_b.jpg
title: Zweite Zeltlagervorbereitung
created_at: 2012-05-06 21:13
excerpt: 'Große Ereignisse werfen ihre Schatten voraus! Am 5. Mai 2012, knapp zwei Monate vor Beginn des <a href="/zeltlager">Zeltlagers im Junni 2012</a> traf sich die SMJ-Fulda zum zweiten Mal um das alljährliche Highlight der Jugendarbeit vorzubereiten. Am Morgen wurden die erledigten Aufgaben von den einzelnen Gruppenleitern vorgestellt: Die Ausarbeitung der Aktionen wie Tagespiel, Lagerolympiade und Stationslauf. Anschließend haben wir uns über die Inhalte der Referate und Workshops ausgetauscht, sowie die endgültigen Entscheidungen zu den Einzel- und Gruppensymbolen getroffen. Am Nachmittag informierte Kilian Machill zum Thema „Erste Hilfe“ in Zeltlagern. So sind wir nun auf alles gut vorbereitet.'
---
<p>Große Ereignisse werfen ihre Schatten voraus! Am 5. Mai 2012, knapp zwei Monate vor Beginn des <a href="/zeltlager">Zeltlagers im Junni 2012</a> traf sich die SMJ-Fulda zum zweiten Mal um das alljährliche Highlight der Jugendarbeit vorzubereiten.</p>
<p>Am Morgen wurden die erledigten Aufgaben von den einzelnen Gruppenleitern vorgestellt: Die Ausarbeitung der Aktionen wie Tagespiel, Lagerolympiade und Stationslauf. Anschließend haben wir uns über die Inhalte der Referate und Workshops ausgetauscht, sowie die endgültigen Entscheidungen zu den Einzel- und Gruppensymbolen getroffen.</p>
<p>Am Nachmittag informierte Kilian Machill zum Thema &bdquo;Erste Hilfe&ldquo; in Zeltlagern. So sind wir nun auf alles gut vorbereitet.</p>
<p>Mit vielen guten Ideen im Gepäck, freuen wir uns schon jetzt auf das diesjährige Zeltlager in Schönstatt vom 03. &ndash; 14. Juli 2012. Aktuell gibt es bereits über 40 Teilnehmer. Anmelden könnt ihr euch noch bis zum 15. Juni 2012 bei Steffen Büdel, Königsbergstraße 38 63637 Jossgrund 0151 28045824 oder steffen.buedel@smj-fulda.org</p>
