---
categories:
- Gemeinschaftswochenenden
migrated:
  node_id: 867
  migrated_at: 2017-02-20 22:26
  alias: artikel/867-asterix-und-obelix-–-operation-hinkelstein
  user_id: 8
tagline: Gemeinschaftswochenende der SMJ Fulda
image:
  cover: https://farm4.staticflickr.com/3012/5850121166_1bdd28b2af_b.jpg
author: kilian
title: Asterix und Obelix – Operation Hinkelstein
created_at: 2011-06-19 22:40
excerpt: Vom 27.5.-29.5. trafen sich im Schönstattzentrum Dietershausen 31 mutige Gallier. Aus allen Ecken des Landes waren sie gekommen, um ihr Können und ihre Fähigkeiten bei der Operation Hinkelstein unter Beweis zu stellen.
---
<p>Vom 27.5.-29.5. trafen sich im Schönstattzentrum Dietershausen 31 mutige Gallier. Aus allen Ecken des Landes waren sie gekommen, um ihr Können und ihre Fähigkeiten bei der Operation Hinkelstein unter Beweis zu stellen.</p>
<p>Nach dem Eintreffen der Abenteurer und nach einer Station am Heiligtum begann das erlebnisreiche Wochenende, wie es sich für echte Gallier gehörte, mit einem zünftigen Abendbrot. Im Anschluss war es Zeit für die erste große Runde&nbsp; um zu sehen wer überhaupt alles gekommen war. Eine bunte Mischung entpuppte sich aus den Anwesenden und schnell wurde klar, dass daraus eine starke Gemeinschaft werden würde. Als Älteste und Betreuer an diesem Wochenende stellten sich Steffen, Christoph, Marius, Felix, Tobias und Kilian vor. Sie waren diejenigen die die Operation Hinkelstein mitbegleiteten und&nbsp; Hilfestellung&nbsp; bei den bevorstehenden Aufgaben gaben. Der Abend klang mit verschiedenen Spielen und Angeboten aus und um 22.30 war Nachtruhe.</p>
<p>Als wir am Samstag um 7.30 geweckt wurden, kamen viele von uns nur schwer aus dem Bett, was bei den nächtlichen Festen der Gallier gut zu verstehen ist. Das ganze sah nach dem Frühsport aber schon etwas anders aus. Aufgewacht und aktiv ging es ins Kapellchen um den Segen für den Tag und für die vor uns liegenden Aufgaben zu erbitten. Danach war Frühstück. Als es anschließend im Roten Saal weiter ging, war jedem bewusst, dass die Operation Hinkelstein jetzt richtig starten würde!</p>
<p>Die Operation würde von jedem Gallier das äußerste fordern und nur in Gemeinschaft zu bewältigen sein. Es sollte von jedem Stärke, Geschicklichkeit, Mut, Ausdauer und Teamarbeit abverlangt werden. Am Vormittag stand eine Olympiade auf dem Plan bei der die Jungs in Altersgruppen aufgeteilt wurden. Neben Weitsprung und Sprinten standen aber auch Liegestütze und Hahnenkampf auf dem Programm. Nach 2 Stunden fairen Wettkampfs knurrten schon die ersten Mägen und das Mittagessen war angesagt.</p>
<p>Doch, dieses Mal sollte das Essen nicht wie gewohnt von den Marien - Schwestern zubereitet werden. Nein, wir mussten, wie die Gallier damals, selber uns das Mittagessen kochen. Da aber der nächste Wald zu weit weg war, und wir nicht genug Jagd Erfahrung hatten, entschlossen wir uns anstatt der Wildschweine, Eierkuchen zu machen. Getreu der Anleitung eines Gruppenleiters wurde die Eierkuchen-Masse in 6 Gruppen angerührt. Mit Milch, Eiern, Mehl panschten und manschten wir uns unser Essen zusammen. Aus jeder Gruppe wurde einer bestimmt, der die Ehre hatte die Gruppen-Eierkuchen-Masse zu braten. Nach anfänglichen Schwierigkeiten mit dem Ofen gelang uns schließlich das Meisterstück und wir konnten endlich essen.</p>
<p>Im Anschluss an das Essen war erst mal Pause und man konnte sich ausruhen. Es gab aber auch Angebote wie Fußball. Als nächstes wurde das handwerkliche Geschick getestet und gleichzeitig ein Andenken für jeden gebastelt. Für das kurz zuvor gemacht Gruppenbild bastelten wir einen Rahmen und konnten ihn nach Belieben verzieren und mit einem Brandmalkolben beschriften. Nach diesen drei großen Herausforderungen: Olympiade, Eierkuchen machen, Bilderrahmen basteln ging es ab zum Abendbrot und zum Abendprogramm.</p>
<p>Als Höhepunkt des Wochenendes und der Operation Hinkelstein waren die Stärken und Talente jedes einzelnen gefragt. Die Fragen: Was kannst DU besonders gut? Wo liegen DEINE Talente? beschäftigten uns&nbsp; den ganzen Abend. Aus diesem Anlass&nbsp; heraus startete am Abend eine Talentshow bei der jeder genau dies vor einer Jury zeigen konnte. Dabei hieß der Leitgedanke: Dabei sein ist alles!</p>
<p>Zwischen coolen und waghalsigen Waveboard-Einlagen und schmetternden Fußball-Lieder hatte es die Jury nicht leicht einen Gewinner zu küren. Das Publikum + Moderator Tobias staunte nicht schlecht als sogar (in einer Inszenierung) Barack Obama und Guido Westerwelle als Ehrengäste dieses Top-Event der SMJ Fulda besuchten! Doch am Ende stand der Gewinner fest. Nach einer meisterlichen Musikalischen Aufführung auf dem Piano konnte sich einer der Teilnehmer einen knappen Vorsprung den anderen gegenüber erhaschen und ein wirklich gelungener Abend neigte sich seinem Ende zu. Der Tag endete mit dem Abendgebet im Heiligtum und der anschließenden Nachtruhe.</p>
<p>Am Sonntag konnte alle etwas später aufstehen. Nach dem Morgengebet und Frühstück hörten wir noch ein Referat von Felix. Was ist Schönstatt überhaupt?, Wer war Pater Josef Kentenich? &ndash; diese Fragen wurden uns auf bespielhafte Weise erklärt und in einer netten Gesprächsrunde erläutert.</p>
<p>Danach ging es wie Sonntags üblich zum Gottesdienst. Nach der Messe hatten wir Zeit unsere Zimmer aufzuräumen und Sachen zu packen. Anschließend gab es Mitttagessen und ein sehr abenteuerliches Wochenende ging zu Ende.</p>
<p>Hoffentlich sehen&nbsp; wir uns im Zeltlager bald wieder. Ich freue mich schon!</p>
<p>Euer Kilian</p>
