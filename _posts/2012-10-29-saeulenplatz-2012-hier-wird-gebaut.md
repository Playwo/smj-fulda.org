---
tags: [smjdeutschland, jubiläum2014, kentenich]
migrated:
  node_id: 919
  migrated_at: 2017-02-20 22:27
  alias: artikel/919-säulenplatz-2012-–-hier-wird-gebaut
  user_id: 132
tagline: Zum 100. Jubiläum der Vorgründungsurkunde wurden die Säulen der SMJ neben dem Tabor-Heiligtum in Stein gemeißelt.
author: Steffen Büdel
title: Säulenplatz 2012 – Hier wird gebaut
created_at: 2012-10-29 16:46
excerpt: Es war einmal vor langer, langer Zeit (im Jahre 1912) ein junger Griechischlehrer, der neu in eine gespannte Klasse des Jungeninternats in Vallendar kam. Dass der Lehrer mehr als nur sein Fach unterrichten wollte, sollte sich schon sehr bald herausstellen, als der Lehrer gemeinsam mit der Klasse die alte Friedhofskapelle der Pallotiner, die inzwischen als Abstellraum genutzt wurde, entrümpelte und einen Platz der Persönlichkeitssuche, der Ruhe und des Gebets schuf, in dem die Klasse und der Lehrer sich am 18. Oktober 1914 schließlich zum allerersten Mal der Gottesmutter anvertrauten… Am vergangenen Wochenende war es so weit. Die SMJ feierte ihren 100. Geburtstag.
---
<p>Es war einmal vor langer, langer Zeit (im Jahre 1912) ein junger Griechischlehrer, der neu in eine gespannte Klasse des Jungeninternats in Vallendar kam. Dass der Lehrer mehr als nur sein Fach unterrichten wollte, sollte sich schon sehr bald herausstellen, als der Lehrer gemeinsam mit der Klasse die alte Friedhofskapelle der Pallotiner, die inzwischen als Abstellraum genutzt wurde, entrümpelte und einen Platz der Persönlichkeitssuche, der Ruhe und des Gebets schuf, in dem die Klasse und der Lehrer sich am 18. Oktober 1914 schließlich zum allerersten Mal der Gottesmutter anvertrauten&hellip;</p>
<p>Am vergangenen Wochenende war es so weit. Die SMJ feierte ihren 100. Geburtstag.</p>
<p>Das große Fest, zu dem die gesamte SMJ Deutschland geladen war, fand auf dem Marienberg in Schönstatt statt. Start war das Abendessen am Freitag. Bei Gesprächen und gemütlichem Beisammensein ließ man den Abend ausklingen. Der Samstagvormittag stand ganz im Zeichen der Diözesen. Jede Diözese konnte sich zusammensetzen und wichtige interne Themen besprechen. Am Samstagnachmittag begann das eigentliche Festprogramm mit einem Impuls in der Hauskapelle der Hochschule. Bei diesem Impuls wurde deutlich, wie wichtig die Säulen in der SMJ sind. Die 1999 verfassten 5 Grundsätze bilden ein Fundament für unsere Jugendarbeit. Anschließend feierte die SMJ mit den eingeladenen Gästen eine Heilige Messe. Der Hauptzelebrant Pater Lothar Penners gestaltete den Gottesdienst zusammen mit vielen weiteren Patres der Schönstattbewegung.</p>
<p>Nach der Messe gingen alle zum Urheiligtum und erneuerten dort ihr Liebesbündnis, wo vor 100 Jahren alles begann. Nun war die Zeit gekommen, den Säulenplatz am Taborheiligtum einzuweihen. Dort stehen 5 Steinsäulen, die die Grundlagen der SMJ verbildlichen. Weiße Tücher verhüllten allerdings noch ihr genaues Aussehen. Auf einer großen Leinwand wurde an die Gründungszeit erinnert. Zitate des Gründers Pater Josef Kentenich wurden eingeblendet und die Jahresparolen der letzten 12 Jahre gezeigt. Dann kam der große Moment&hellip;</p>
<p>Die Säulen wurden enthüllt! Ein großes Feuerwerk verdeutlichte, dass dies ein wichtiger Tag für die weltweite SMJ darstellte. Nach einem Sektempfang im Haus Tabor begann die Party im Jugendzentrum. Mit Musik, Essen und Jubiläumsbier wurde dieser Abend zu einem unvergesslichen Erlebnis. Der 100. Geburtstag wurde durch das Säulenfest würdig begangen.</p>
<p>Nach einer kurzen Nacht begann der Morgen mit einem großen Brunch, der keine Wünsche offen ließ. Zum Abschluss des Wochenendes trafen sich alle beim neuen Säulenplatz. Dieser Geburtstag ist nicht das Ende der SMJ, sondern vielmehr der Anfang der nächsten 100 Jahre!</p>
<p>Es sei wichtig, niemals unsere Grundsätze zu vergessen. Durch den Säulenplatz werden wir immer an die Säulen denken:</p>
<div class="box" style="font-weight: bold; text-align: center; font-size: 120%;padding: .8em;">Gemeinschaft &ndash; Lebensschule &ndash; Liebesbündnis &ndash; Apostelsein &ndash; Mannsein</div>
