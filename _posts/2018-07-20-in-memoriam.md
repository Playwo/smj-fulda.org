---
title: In Memoriam
tagline: Pfarrer Ulrich Schäfer
author: Johannes Müller
image:
  cover: https://farm5.staticflickr.com/4102/4894639995_72292c0890_o_d.jpg
  align: top
---

[Ulrich Schäfer](http://smj-fulda.org/artikel/2018-07-19-trauer-um-pfarrer-schaefer/) war wirklich SMJler durch und durch, 40 Jahre dabei. Den Großteil davon in leitender Funktion. Ein echter Bescheidwisser. Niemand hat die Geschichte der SMJ-Fulda und mehrere SMJler-Generationen so stark geprägt wie er.

Vieles bei uns trägt seine Handschrift, sein Vermächtnis – oder auch ein [Jesus-Shirt](https://shop.smj-fulda.org/), dass ja auf seinen Impuls zurückgeht. Es gibt so viele gute Erinnerungen an Dinge, die er jedem von uns mitgegeben hat. Sein plötzlicher Tod macht traurig, aber viel mehr noch können wir dankbar sein dafür, dass er bei uns war, und auf ein österliches Wiedersehen hoffen.

Nicht zu vergessen sind die vielen Anekdoten, mit denen er gewitzt seine Mitmenschen erheiterte oder zum Nachdenken anregte. So weiß beispielsweise jeder, der im Zeltlager 2010 in Hausen war, dass Kaiserin Sisi auf ihrer linken Schulter ein Ankertattoo hatte.
Jeder von uns hat Erinnerungen an besondere Momente mit dem Verstorbenen. Diese wollen wir als kleine Anekdoten sammeln und zu einer gemeinsamen Erinnerung in Form eines Videos zusammenstellen.

Dabei kann jeder mitmachen: Erzähle uns von deinen Erlebnissen mit Ulrich Schäfer. Eine Erinnerung an eine Begegnung, ein Gespräch, oder ein von ihm erzähltes Geschichtchen.

Falls du einen passenden Schnappschuss hast, freuen wir uns auch über interessante Originalaufnahmen von Ulrich (Bilder, Video, Audio).

Das ganze einfach per Handy oder Kamera aufnehmen und einsenden. Am Ende schneiden wir alles zusammen in ein Erinnerungsvideo.

Bitte auch weitersagen. Es dürfen nicht nur SMJ-ler mitmachen, sondern jeder der sich mit dem Verstorbenen verbunden fühlt und etwas beitragen möchte.

Praktische Hinweise:

* Aufnahme entweder als Video oder Sprachnotiz.
* Bitte nicht zu weit ausholen. Ein kurzes Statement mit max 2-3 Sätzen ist optimal.
* Dafür gerne mehrere einzelne Beiträge (geht auch in einer Aufnahme, mit 5 Sekunden Abstand).
* Eine ruhige Umgebung hilft, dass das Gesprochene gut verständlich ist.
* Handyvideos bitte im Querformat aufnehmen.
* Besser als Selfie-Videos ist, wenn jemand anderes filmt oder die Kamera fest steht.
* Die Dateien dann irgendwo hochladen, so dass sie zum Download zur Verfügung stehen (z.B. Dropbox, https://wetransfer.com/ oder in [diesen Google-Drive-Ordner](https://drive.google.com/drive/folders/1A87_eDRT36QLjG-dsJjuuQGKy-P9tuXo?usp=sharing))
* E-Mail mit Downloadlink und Namen der Beteiligten an [in-memoriam@smj-fulda.org](mailto:in-memoriam@smj-fulda.org) senden.

SERVUS MARIAE NUNQUAM PERIBIT
