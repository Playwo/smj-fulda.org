---
title: "72-Stunden-Aktion"
tagline: "Stockbrotbacken im Schönstattzentrum"
author: Elias Wolf
tags: [kreise]
image:
  cover: https://live.staticflickr.com/65535/48013700901_032a666b75_o_d.jpg 
  align: top
excerpt: >
  Während unserer letzten Kreistagung vom 24. bis 26. Mai hat gleichzeitig die 72-Stunden-Aktion des BDKJ stattgefunden.
  Das ist natürlich eine perfekte Gelegenheit für uns aktiv zu werden. Nach einigen Überlegungen stand fest, unser Projekt sollte das Zubereiten eines <em>XXXL Stockbrotes</em> sein.
---

<figure>
  <img src="https://live.staticflickr.com/65535/48013789902_c08ffa9685_z_d.jpg">
</figure>
<figure>
  <img src="https://live.staticflickr.com/65535/48013713758_00cbbba7b3_z_d.jpg">
</figure>
<figure>
  <img src="https://live.staticflickr.com/65535/48013713678_fff03a191b_z_d.jpg">
</figure>

Während unserer letzten Kreistagung vom 24. bis 26. Mai hat gleichzeitig die 72-Stunden-Aktion des BDKJ stattgefunden.
Das ist natürlich eine perfekte Gelegenheit für uns aktiv zu werden. Nach einigen Überlegungen stand fest, unser Projekt sollte das Zubereiten eines <em>XXXL Stockbrotes</em> sein.

Nach einem Überblick über die Vorbereitungen am Freitag konnte die Aktion am Samstag richtig durchstarten. 
Bis zum Start gab es jedoch noch einiges zu erledigen. Als Team ließen sich die Aufgaben wie Holz besorgen, Feuerstelle graben, Glutrinne bauen usw., ohne Probleme bis zum frühen Nachmittag erledigen.

Den Platz ordentlich ausgeschmückt, konnte es dann auch endlich losgehen. Der Stock wurde mit Teig umwickelt, die Glut in die Rinne geschaufelt, und das Backen hat begonnen. 
Die vielen Zweifel, die so mancher an der Konstruktion hatte, erwiesen sich glücklicherweise als falsch. Denn alles funktionierte nach Plan, und nach einiger Zeit war das Brot gut durch.

Gegen Abend fand dann noch ein Gottesdienst in der Pfarrkirche Dietershausen statt, den unser neuer Bischof Michael Gerber zelebrierte und der Band <em>Himmelwärts</em> begleitet wurde. 
Den Abend haben wir dann zusammen mit dem Bischof und einigen Gästen draußen am Lagerfeuer ausklingen lassen. 

Am Sonntag musste alles wieder aufgeräumt werden, doch der Höhepunkt war wie immer die Restevernichtung. Schließlich haben die „Küchenmänner“ den übrigen Teig im Ofen zubereitet.
Zumindest sieht jetzt alles wieder aus wie vorher. Naja, zumindest fast. 

<figure class="full-width">
  <img src="https://live.staticflickr.com/65535/48013701116_75a307cc36_z_d.jpg">
</figure>
