---
tags: [smjdeutschland, jako]
migrated:
  node_id: 927
  migrated_at: 2017-02-20 22:27
  alias: artikel/927-...dem-himmel-entgegen-–-mit-neuer-führung
  user_id: 8
tagline: Jahreskonferenz der SMJ-Deutschland beschließt neue Jahresparole. Thomas Limbach Regiosprecher der Regio Mitte.
author: kilian
title: "...dem Himmel entgegen – mit neuer Führung"
created_at: 2012-12-31 14:17
excerpt: 'Wie traditionell jedes Jahr nach Weihnachten trafen sich auch in diesem Jahr die Verantwortlichen der Diözesen zur Jahreskonferenz der SMJ-Deutschland. Unter dem Motto „Kundschafter“ tagte die Konferenz 5 Tage in Schönstatt. Während dieser Zeit wurde auch der neue Regiosprecher der Regio Mitte gewählt. Die Diözesen Speyer, Mainz/ Limburg und Fulda konnten sich auf einen Kanidaten einigen. Thomas Limbach löst ab sofort Matthias Schott als Regiosprecher ab und wurde gebührend ins Amt eingeführt. Gleichzeitig wurde auch die neue Jahresparole der SMJ-Deutschland für das Jahr 2013 ausgerufen:'
---
<p>Wie traditionell jedes Jahr nach Weihnachten trafen sich auch in diesem Jahr die Verantwortlichen der Diözesen zur Jahreskonferenz der SMJ-Deutschland. Unter dem Motto &bdquo;Kundschafter&ldquo; tagte die Konferenz 5 Tage in Schönstatt. Während dieser Zeit wurde auch der neue Regiosprecher der Regio Mitte gewählt. Die Diözesen Speyer, Mainz/ Limburg und Fulda konnten sich auf einen Kanidaten einigen. Thomas Limbach löst ab sofort Matthias Schott als Regiosprecher ab und wurde gebührend ins Amt eingeführt.</p>
<p>Gleichzeitig wurde auch die neue Jahresparole der SMJ-Deutschland für das Jahr 2013 ausgerufen:</p>
<h2>Festes Herz &ndash; himmelwärts</h2>
<p>Wir freuen uns als SMJ-Fulda auf das kommende Jahr mit vielen Aktionen, Wochenenden und dem Zeltlager. Danken möchten wir allen Unterstützern und Helfern unserer Jugendarbeit im Jahr 2012 und freuen uns mit Festem Herzen in die Zukunft zu gehen. Auf himmelwärts!</p>
