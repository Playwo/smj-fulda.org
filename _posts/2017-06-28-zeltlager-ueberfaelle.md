---
title: "Zeltlager-Überfälle"
author: Simon Wawra
tags: [zeltlager, smjfulda]
image:
    cover: "!baseurl!/images/2017/2017-06-28-zeltlager.jpg"
---
In 6 Tagen startet das Zeltlager! Mit einer riesigen Vorfreude fiebern wir diesem Highlight entgegen.

Für unsere Überfäller noch ein paar Infos:
Überfallnächte sind:

* Sa. 08.07.2017
* So. 09.07.2017
* Fr. 14.07.2017

Der [Überfallkodex](http://smj-fulda.org/artikel/2015-07-22-berfallcodex/) ist einzuhalten,
Fragen gerne an die Lagerleitung.