---
title: "Kreistagung: Bündniskultur der Schönstatt-Mannesjugend"
author: Rainer M. Gotter
tags: [kreise, smjfulda]
image:
    cover: "https://farm4.staticflickr.com/3789/33171992660_503d3bb662_o_d.jpg"
    align: top
---
Junge Männer lassen sich von der Jugendarbeit der Schönstatt-Mannesjugend Fulda ansprechen und setzen sich aktiv ein. Denn hier erleben sie den Glauben, die Gemeinschaft in der Kirche, lernen Verantwortung zu übernehmen und sich mit ihrer Kreativität einzusetzen. Normalerweise könnte man davon ausgehen, dass Jugendliche, die in der Mannesjugend aktiv sind, sich als Christ sehen. Das wäre logisch. Doch ist das Christ sein, in der Schule, im Studium oder am Arbeitsplatz gar nicht so einfach.

„Daher ist so ein Treffen mit Gleichaltrigen genau das Richtige“, so Samuel, der an diesen Wochenende auch mit dabei ist. „Vor allem ein Ort an dem auf uns eingegangen wird. Ein Ort, an dem wir selber Zeit für uns haben. Ein Ort, an dem wir uns ausprobieren können. Ein Ort, an dem Gespräche stattfinden, für die sich sonst nur schwer Gesprächspartner finden. Ein Ort, an dem Gedanken angeregt werden, welche sonst in einer dunklen Ecke verschwinden. Ein Ort, an dem Gefühle tatsächlich ausgesprochen und gezeigt werden. Ein Ort, an dem man Zuhause ist.“

Diese Tradition, sich in *Kreisen* von Gleichaltrigen zusammen zu finden, hat sich bis heute durchgesetzt. „Denn“, so stellen die Kreisleiter Steffen Büdel und Christian Schopp fest, „macht uns die Aufgabe selbst eine Menge Spaß, vor allem mit den jungen Menschen. Nicht zu vergessen, dass hinter allem, auch eine Glaubensgemeinschaft steht, die eine bessere Welt für möglich hält“. Diözesanführer Simon Wawra ergänzt, dass „das Ganze auch im 50. Jahr seit der Gründung der Schönstatt-Mannesjugend im Bistum Fulda, Kinder und Jugendliche gleichermaßen anspricht und sie sich die Ideale der Mannesjugend zu Eigen machen“.

Bescheid zu wissen im Glauben, seine Meinung zu sagen und sich nicht davon abhalten zu lassen. Eine Bündniskultur als Christ im Alltag zu leben, die nicht nur fromm und demütig daherkommt, sondern aktiv und echt: Das drückt das *Mitarbeiterbündnis* bei der Schönstatt-Mannesjugend aus. Sieben von diesen jungen Männern schlossen ihr Bündnis am 18. März 2017, in der Marienkapelle in Dietershausen. Das Mitarbeiterbündnis, wie die Sieben betonen, stützt sie in schweren Zeiten und macht das Miteinander-Freuen in guten Zeiten noch wertvoller. Daher war es selbstverständlich, dass alle Teilnehmer sich am Bündnisfeuer zusammenfanden, um ihre Gemeinschaft im gemeinsamen Treueschwur als Schönstatt-Mannesjugend Fulda zu bekräftigen. Das Wochenende vom 17.–19. März 2017 endete mal wieder viel zu schnell, aber mit gutgelaunten Gesichtern.

<figure class="flickr-photoset full-width" data-href="https://www.flickr.com/photos/45962678@N06/sets/72157678205934264">
  <a href="https://www.flickr.com/photos/45962678@N06/33514642736/in/album-72157678205934264"><img src="https://farm3.staticflickr.com/2841/33514642736_38f486beff_q.jpg" alt="2017-03
-17 Kreistagung-1" data-src-large="https://farm3.staticflickr.com/2841/33514642736_38f486beff_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/33555312945/in/album-72157678205934264"><img src="https://farm3.staticflickr.com/2840/33555312945_9c16ac25cf_q.jpg" alt="2017-03
-17 Kreistagung-2" data-src-large="https://farm3.staticflickr.com/2840/33555312945_9c16ac25cf_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/33171997520/in/album-72157678205934264"><img src="https://farm4.staticflickr.com/3856/33171997520_5280aa2ea8_q.jpg" alt="2017-03
-17 Kreistagung-3" data-src-large="https://farm4.staticflickr.com/3856/33171997520_5280aa2ea8_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/33514627636/in/album-72157678205934264"><img src="https://farm3.staticflickr.com/2945/33514627636_fd46f1dc48_q.jpg" alt="2017-03
-17 Kreistagung-4" data-src-large="https://farm3.staticflickr.com/2945/33514627636_fd46f1dc48_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/33171992660/in/album-72157678205934264"><img src="https://farm4.staticflickr.com/3789/33171992660_f0e4aee8de_q.jpg" alt="2017-03
-17 Kreistagung-5" data-src-large="https://farm4.staticflickr.com/3789/33171992660_f0e4aee8de_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/33514634586/in/album-72157678205934264"><img src="https://farm3.staticflickr.com/2943/33514634586_183e1ce1a5_q.jpg" alt="2017-03
-17 Kreistagung-6" data-src-large="https://farm3.staticflickr.com/2943/33514634586_183e1ce1a5_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/33514634056/in/album-72157678205934264"><img src="https://farm3.staticflickr.com/2878/33514634056_2e22a00d7b_q.jpg" alt="2017-03
-17 Kreistagung-7" data-src-large="https://farm3.staticflickr.com/2878/33514634056_2e22a00d7b_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/33171989250/in/album-72157678205934264"><img src="https://farm4.staticflickr.com/3837/33171989250_71287b5ba3_q.jpg" alt="2017-03
-17 Kreistagung-8" data-src-large="https://farm4.staticflickr.com/3837/33171989250_71287b5ba3_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/33514629076/in/album-72157678205934264"><img src="https://farm3.staticflickr.com/2889/33514629076_2043b6ce44_q.jpg" alt="2017-03
-17 Kreistagung-9" data-src-large="https://farm3.staticflickr.com/2889/33514629076_2043b6ce44_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/33171988180/in/album-72157678205934264"><img src="https://farm3.staticflickr.com/2831/33171988180_e5190586fe_q.jpg" alt="2017-03
-17 Kreistagung-10" data-src-large="https://farm3.staticflickr.com/2831/33171988180_e5190586fe_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/33555606055/in/album-72157678205934264"><img src="https://farm3.staticflickr.com/2833/33555606055_1b26a9f044_q.jpg" alt="1733343
6_969956659802000_8549732499813564416_n" data-src-large="https://farm3.staticflickr.com/2833/33555606055_1b26a9f044_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/sets/72157678205934264" class="flickr-link" title="Bildergalerie Kreistagung März 2017 auf Flickr">Bildergalerie</a>
</figure>
