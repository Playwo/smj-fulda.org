---
title: Das Leben ruft! Werkwoche in Óbudavár
tags: [smjfulda, kreise, werkwoche, regiomitte]
image: 
    cover: https://c1.staticflickr.com/5/4729/38555157974_d1f66182c4_k_d.jpg
    align: top
excerpt:
    In der Osterwoche fahren wir mit der Region Mitte zur Werkwoche nach Óbudavár, dem Schönstattzentrum in Ungarn. Dort stellen wir uns den Fragen des Lebens und genießen jenes in der Ferienregion am Plattensee.
---
Das Leben ruft! Welcher Leistungskurs passt zu mir? Der Schulabschluss ist geschafft, wie geht’s weiter? Freiwilligendienst oder durchstarten? Studium oder Ausbildung? Familie oder Karriere?

Immer wieder stellt sich die Frage, wohin mich das Leben ruft. Doch wer ruft da eigentlich? Wie kann ich die Rufe hören?

Zusammen wollen wir überlegen wie es ist, gerufen zu werden und wie ich herausfinde, welcher Weg zu mir passt.

## Werkwoche

Die Werkwoche ist eine Ferienfahrt für Jugendliche und junge Erwachsene, ganz ohne jüngere Gruppenkinder.
Dabei geht es darum, gemeinsam Spaß zu haben, die Gemeinschaft zu stärken, aber auch thematisch zu arbeiten.
Wir veranstalten diese Fahrt gemeinsam mit den anderen SMJ-Diözesen der Regio Mitte.

## Óbudavár in Ungarn

Trotz Thematik ist Werkwoche auch immer ein bisschen Urlaub. Dabei wollen wir die Idylle des Plattensees erkunden, uns mit der ungarischen SMJ treffen und austauschen.

## Infos

**Datum:** 1. bis 7. April 2018  
**Teilnehmer:** Jungen ab 14 bzw. Kreisalter  
**Ort:** Schönstattzentrum Óbudavár, Ungarn  
**Kosten:** 215€ (Brüder je 195€)  
**Frühbucherrabatt:** 195€ (Brüder je 175€) bei Anmeldung bis zum 15. Januar  
**Veranstalter:** Schönstatt-Mannesjugend der Diözesen Fulda, [Speyer](https://www.smj-speyer.de), [Trier](https://www.smj-trier.de), [Mainz und Limburg](https://www.smj-mainz-limburg.de)  
**Anmeldung:** Johannes Wende ([johannes.wende@smj-fulda.org](mailto:johannes.wende@smj-fulda.org))

Wir fahren gemeinsam in Bussen nach Ungarn, Abfahrt ist Ostersonntag am frühen Abend. Die genauen Abfahrtsorte und -zeiten werden noch bekanntgegeben.


