---
title: Basti goes to Hollywood!
tagline: Oder ein ganz normales Wochenende…
tags: [smjfulda, kreise]
author: Christian Schopp
image:
  cover: https://farm1.staticflickr.com/954/42176998031_a16d4adbe9_k_d.jpg
  position: bottom
excerpt: >
  Der Kreis Stefian machte eine Kreisfahrt nach München. Sie berichten von ihren Erlebnissen in der bayerischen Hauptstadt an einem normalen, aber doch außergewöhnlichen Wochenende.
---
Christi Himmelfahrt. Donnerstag. 07:30 Uhr. Viel zu früh. Das Wochenende beginnt.

Auf dem Weg in den stauärmsten Tag des Jahres beginnt unsere Reise in Rönshausen zur heiligen Messe. Der erste Eindruck ist positiv: Wir sind keine Firmlinge, wir kommen nicht von hier, wir sind Jugend, die Blicke ruhen auf uns. Auch der zweite Eindruck sagt uns: Wir polarisieren.

<figure>
    <img src="https://farm1.staticflickr.com/912/42130095652_554dfd17c5_z_d.jpg">
</figure>

Weiter geht’s, die zwei Piloten fliegen uns auf dem schnellsten Weg ins Münchener Schönstattzentrum, wo wir die nächsten Tage verbringen werden. Auf dem Weg treffen wir noch auf ein paar Marburger Cheerleader, die wir unbeeindruckt links liegen lassen können. In der bayerischen Landeshauptstadt angekommen, werden wir natürlich gleich vom Fieber gepackt und der Strudel zieht uns in den Tischtennis-Rundlauf hinein. Nach zwei oder drei Runden schaffen wir es dann doch noch, uns dem Sog zu entziehen und beschließen, dass wir uns hier von unserer besten Seite zeigen müssen. Gesagt, getan. Wir erobern den Hügel und können ihn auch solange behaupten bis uns Petrus von ganz oben zu verstehen gibt, dass uns kein irdisches Wesen mehr vom Hügel zu vertreiben wagt. Trotz wetterbedingter Unsicherheit beschließen wir, den Abend im Biergarten ausklingen zu lassen. Das gelingt uns auch sehr gut. Wir freuen uns auf die nächsten Tage.

<figure class="full-width">
    <img src="https://farm1.staticflickr.com/950/42130095642_069e89998f_z_d.jpg">
</figure>

Freitag in der Früh. Halb sechs. Wer hat das Licht an gemacht? Ach ne, das ist die Sonne.

Ohne Verzögerung starten wir in unseren zweiten Tag. Unsere erste Erkenntnis ist die wegweisende Entscheidung von gestern, gleich drei Gruppentickets für den Münchener Nahverkehr zu kaufen. Ohne diese Tickets hätte das Wochenende einen ganz anderen Verlauf genommen! Wir fahren also mit der S-Bahn zu Rasso. Ja genau. Das ist unser Stadtführer, der uns mit seiner haarigen Art einen sehr interessanten Einblick in die Kriminalgeschichte Münchens bietet. Schade, dass wir uns nach etwa 1,5 Stunden von Rasso verabschieden müssen. Zum Abschied zwirbelt er nochmal an seinem Bart und er verschwindet mit Christian im Office.

<figure>
    <a href="https://www.flickr.com/photos/45962678@N06/28304342238/in/album-72157696933950925"><img src="https://farm1.staticflickr.com/970/42130098292_c303abda49_z_d.jpg" /></a>
</figure>

Weiter geht’s zum englischen Garten. Wir können Steffen gerade so davon abhalten, zu den Surfern ins Wasser zu springen. Mit Bus und Bahn geht es nun Richtung Grünwald zu den Bavaria Filmstudios. Zum Glück haben wir unsere Bahntickets. Eine wilde Minenfahrt, 90 Minuten Führung und eine großartige Nachrichtenansage später stehen wir vor der nächsten Herausforderung. Yannick, Basti und Steffen beweisen ihr komödiantisches Geschick im Trailer zum neuen „(T)raumschiff Surprise“-Streifen zusammen mit Michael „Bully“ Herbig, Christian Tramitz und Rick Kavanian. Die Zuschauer jubeln und wir gönnen uns eine Pause im Augustinerkeller. Mit der Vogelwiese und der charmanten, original-bayerischen Massenabfertigung im Biergarten beenden wir auch diesen Tag. Das Wochenende ist schließlich noch lange nicht vorbei.

<figure class="full-width">
    <img src="https://farm1.staticflickr.com/946/28304343868_72b43f4e97_z_d.jpg">
</figure>

Samstagmorgen. Endlich Freizeit. Was? Wer hat da was von Freizeit gesagt?!

Nach dem Frühstück fahren wir zur KZ-Gedenkstätte nach Dachau. Schwester M. Elinor führt uns die nächsten Stunden auf den Spuren von Pater Kentenich durch das ehemalige Konzentrationslager. Nur durch eine viel zu kurze Mittagspause unterbrochen, schafft sie es, eine Vielzahl an Anekdoten aus den Geschehnissen, die dieses Gelände zu trauriger Berühmtheit geführt haben, zu erzählen. Es ist bemerkenswert, wie viele Einzelschicksale durch Pater Kentenich gestärkt wurden, wie vielen Menschen er zur Seite stand und bei wie vielen Begebenheiten er hätte umkommen können wäre da nicht der kleine Funke der Gottesmutter, der ihn durch seinen Lebensweg begleitet hat.

Auf dem Weg zurück zum Schönstattzentrum überlegen wir uns, ob die Stadt München Blitzer als bloße Dekorationselemente entdeckt hat oder ob es sich hierbei um eine Art Rallye handelt. Jedenfalls scheint es unmöglich, sämtliche Blitzer der Stadt an nur einem Tag zu erkunden. Für den Abend steht grillen auf dem Plan. Nach dem Genuss unseres Abendessens, stehen wir in einer sympathischen Münchener WG, die uns ihr Zusammenleben eindrucksvoll darzustellen vermag. Den Tagesabschluss verbringen wir zum Teil in der Stadt und zum Teil in der altbekannten Klause. Vielen Dank an dieser Stelle an die Diözese Eichstätt für die Entsendung ihres kompetenten Vertreters.

<figure>
    <img src="https://farm1.staticflickr.com/824/28304342238_4091454dc5_z_d.jpg">
</figure>

Sonntagmorgen. Wo bleibt eigentlich unser Frühstück?!

Wir genießen den Gottesdienst mit Pater Hans-Martin. Nach einem Gruppenfoto verabschieden wir uns mit unseren allseits bekannten und beliebten Engelschören vom Münchener Parkett. Es war echt schön hier! Einen Zwischenstopp machen wir noch bei Leo. Unglaublich, wie viele Pfannkuchen dieser Leo backen kann. Wir können unser Ziel, sämtliche Pfannkuchen im Restaurant zu essen, leider nicht erreichen und machen uns mit halbleeren Mägen auf den Heimweg. Wer sind eigentlich diese Verkehrsrowdys in der Münchener Innenstadt? Irgendwie scheinen die sich nicht zu mögen. Jedenfalls bricht der Kontakt untereinander nicht ab. Respekt und Anerkennung werden schier aus dem Auto heraus getragen.

Zum Glück finden wir heute den Stau, den wir am Donnerstag vergeblich gesucht haben. Ich könnte mir auch nichts Besseres ausdenken als einen Tag auf der Autobahn zu verbringen. Immerhin steht das Wetter ausnahmsweise auf unserer Seite. Es schenkt uns einen herrlichen Mix aus Sonne, Wolkenbruch, Gewitter, Starkregen und Aquaplaning. Doch allen Widrigkeiten und explodierenden Glasflaschen zum Trotz führt uns TomTom sicher nach Hause.

Es war wieder ein geniales Wochenende! Das sieht man schon alleine daran, dass der Innenraum meines Autos schlimmer aussieht als nach zwei Wochen Zeltlager.
