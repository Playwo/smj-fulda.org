---
tags: [smjfulda, gewo]
migrated:
  node_id: 907
  migrated_at: 2017-02-20 22:27
  alias: artikel/907-gemeinschaftswochenende-februar-2012
  user_id: 12
tagline: Gemeinschaftswochenende für 9-13-jährige Jungs am Fastnachtswochenende 2012
author: philipp
title: Gemeinschaftswochenende Februar 2012
created_at: 2012-02-22 20:44
excerpt: Samstagabend halb acht, das Stadion füllt sich und die Fans warten gespannt auf den Start des „Masken-Kicker-Turnieres“. Die Stimmung erreicht einen ersten Höhepunkt als der bekannte Sportmoderator Constantin Mühr das Stadion betritt und das erste Spiel ankündigt. Und schon in der Vorrunde kommt es zu spannenden Spielen und Überraschungen als mehrere Favoriten sich geschlagen geben müssen. Doch auch abseits des Kickertisches gibt es an diesem Abend viel zu erleben, es werden Snacks gereicht und einige Tanz-Begeisterte, nehmen die den Wettkampf um den verrücktesten Tanz auf.
---
<p>Samstagabend halb acht, das Stadion füllt sich und die Fans warten gespannt auf den Start des &bdquo;Masken-Kicker-Turnieres&ldquo;. Die Stimmung erreicht einen ersten Höhepunkt als der bekannte Sportmoderator Constantin Mühr das Stadion betritt und das erste Spiel ankündigt. Und schon in der Vorrunde kommt es zu spannenden Spielen und Überraschungen als mehrere Favoriten sich geschlagen geben müssen. Doch auch abseits des Kickertisches gibt es an diesem Abend viel zu erleben, es werden Snacks gereicht und einige Tanz-Begeisterte, nehmen die den Wettkampf um den verrücktesten Tanz auf. Doch auch am restlichen Abend kommt durch die Musik und klassisch gebrüllte Anfeuerungsrufe, richtige Stadionatmosphäre auf und so kann sich im Finale der besten drei das Team Antennen Boys knapp durchsetzten und sichert sich den Titel.</p>
<p>Ein traumhafter Abend und so fragt sich der ein oder andere, ob sie diesen Abend nur geträumt hätten. Doch nein, wenn man sich auf macht und den Abenteuern entgehen geht, geschehen solche grandiosen Dinge. Und so machten wir uns, als Gemeinschaft von 20 Jungs an diesem Wochenende nach Dietershausen auf, um uns den Herausforderungen, wie dem Frühsport, chinesisch Fußball, einer Nachtwanderung mit Gruselgeschichten aber auch geistige Herausforderungen, wie das Zuhören beim Referat und das Mitarbeiten in den Gruppenstunden. Doch die Mühen haben sich gelohnt und bescherten uns ein Wochenende voller Höhepunkte. So neben dem Kicker Turnier, auch die Messe mit Pfarrer Klobes aus Kalbach und das Basteln der Masken.</p>
