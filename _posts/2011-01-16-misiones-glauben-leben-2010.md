---
title:
categories:
  - video
author: Tobias Büdel
image:
  cover: https://farm5.staticflickr.com/4089/5011844104_da7b1c26fe_b_d.jpg
---
Impressionen der Glaubenswoche "misiones - Glauben leben" in der Pfarreingemeinschaft Christus, der Weinstock - Erlenbach am Main.

<iframe width="560" height="315" src="https://www.youtube.com/embed/XYr__SxsUJ8" frameborder="0" allowfullscreen></iframe>
