---
categories: [smjfulda, kreise]
migrated:
  node_id: 990
  migrated_at: 2017-02-20 22:28
  alias: artikel/990-kreise-treffen-sich-zum-nikolauswochenende
  user_id: 0
tagline: Vom 05. bis 07. Dezember trafen sich die Kreise der SMJ Fulda in Dietershausen. Zum Wochenende kamen die Kreise von Christian und Steffen, Kilian, Ullrich und auch der Kreis vom Alexander.
title: Kreise treffen sich zum Nikolauswochenende
created_at: 2014-12-10 22:19
excerpt: Vom 05. bis 07. Dezember trafen sich die Kreise der SMJ Fulda in Dietershausen. Zum Wochenende kamen die Kreise von Christian und Steffen, Kilian, Ullrich und auch der Kreis vom Alexander. Am Freitagabend begannen die Kreise dieses Mal gemeinsam im Heiligtum. Bei diesem Einstieg gab es einen Rückblick auf das Schönstatt-Jubiläum und was einem ein solches Ereignis persönlich bringen kann! Die vier Kreise befassten sich an dem Wochenende mit unterschiedlichen Themen. Der Kreis Alex blickt in die Zukunft und plante wie es mit dem Kreis weiter gehen kann. Kreis Kilian schaut darauf was einen Gruppenleiter ausmacht, der Kreis vom Ullrich arbeitete an seinem Projekt weiter. Sie haben ein Kreuz gebaut, welches für den Schönstattbildstock in Dietershausen geplant ist.
---
<p>Vom 05. bis 07. Dezember trafen sich die Kreise der SMJ Fulda in Dietershausen. Zum Wochenende kamen die Kreise von Christian und Steffen, Kilian, Ullrich und auch der Kreis vom Alexander. Am Freitagabend begannen die Kreise dieses Mal gemeinsam im Heiligtum. Bei diesem Einstieg gab es einen Rückblick auf das Schönstatt-Jubiläum und was einem ein solches Ereignis persönlich bringen kann!</p>
<p>Die vier Kreise befassten sich an dem Wochenende mit unterschiedlichen Themen. Der Kreis Alex blickt in die Zukunft und plante wie es mit dem Kreis weiter gehen kann. Kreis Kilian schaut darauf was einen Gruppenleiter ausmacht, der Kreis vom Ullrich arbeitete an seinem Projekt weiter. Sie haben ein Kreuz gebaut, welches für den Schönstattbildstock in Dietershausen geplant ist.</p>
<p>Der im September neugegründete Kreis von Christian und Steffen beschäftige sich am Wochenende mit dem Thema Freiheit! Am Freitagabend schauten sie auf ihre erste Kreistagung im September zurück. Dieses Mal waren 10 Teilnehmer des Kreises da. Nach einem kurzen Einstieg in das Thema Freiheit und einer Nachtwanderung war der Freitagabend auch schon wieder vorbei. Am Samstagvormittag ging es darum sich einmal Gedanken über verschiedene Herrschaftsformen wie Demokratie, Monarchie oder Anarchie zu machen. Was haben diese drei mit Freiheit zu tun? Was ist gerecht und wie geht es uns hier in unserem Land?</p>
<p>Am Nachmittag nach einer langen Mittagspause feierten alle Kreise gemeinsam mit Pfarrer Schäfer im Heiligtum die Heilige Messe. Am frühen Abend begann das Spiel &bdquo;Schlag die Kreisleiter&ldquo; nach Prinzip der Fernsehshow Schlag den Raab. Nach dem ersten Teil des Spiels und dem Abendessen ging der Kreis von Steffen und Christian nach Pilgerzell kegeln. Für zwei Stunden fielen die Kegel nur so und insgesamt war es so ein super Abend! Die anderen Kreise gingen zur selben Zeit nach Fulda bowlen.</p>
<p>Am Sonntagmorgen stand der zweite Teil von &bdquo;Schlag die Kreisleiter an&ldquo;. Nach 15 Spielen wie z. B. Tischtennis, Tauziehen, Kerzen anzünden, Quizze oder Tischkicker&hellip;</p>
<p>Am Ende gewannen die zwei Kreisleiter das Spiel knapp. Für alle gab es anschließend zur Feier frische Schokoküsse in verschiedenen Sorten. Nach dem Mittagessen war das Wochenende auch schon wieder dabei. Die nächste Kreistagung findet erst wieder im neuen Jahr statt. Darauf freuen sich schon alle und mit den Erfahrungen vom Wochenende geht es in die letzte Zeit des Advents!</p>
<p>[photoset id=72157647368904794]</p>
