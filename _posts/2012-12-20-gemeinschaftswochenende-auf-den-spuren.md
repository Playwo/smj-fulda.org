---
tags: [smjfulda, gewo]
migrated:
  node_id: 926
  migrated_at: 2017-02-20 22:27
  alias: artikel/926-gemeinschaftswochenende-„auf-den-spuren“
  user_id: 0
tagline: Gemeinschaftswochenende „Auf den Spuren“ am 13.-16. Dezember 2012
title: Gemeinschaftswochenende „Auf den Spuren“
created_at: 2012-12-20 10:48
excerpt: Zunächst wurden die Jungs auf die Zimmer verteilt, in denen sie sich noch schnell einrichten konnten bevor es zum Abendessen ging. Als Einleitung zum Gemeinschaftswochenende gingen wir danach ins Heiligtum. Am Abend wurden dann kleine Kennenlernspiele gespielt. Unter anderem „Karten im Kreis“ und „Arschhirnkrabben“. Um 21 Uhr begannen wir zusammen Brettspiele zu spielen, zum Beispiel Risiko, Monopoly und Werwolf. Zum Abschluss des ersten Tages trafen sich alle nochmal im Heiligtum, wo jeder seine Wünsche zum Gemeinschaftswochenende äußern durfte. Dann hieß es ab 23 Uhr Nachtruhe.
---
<p>Zunächst wurden die Jungs auf die Zimmer verteilt, in denen sie sich noch schnell einrichten konnten bevor es zum Abendessen ging. Als Einleitung zum Gemeinschaftswochenende gingen wir danach ins Heiligtum.</p>
<p>Am Abend wurden dann kleine Kennenlernspiele gespielt. Unter anderem &bdquo;Karten im Kreis&ldquo; und &bdquo;Arschhirnkrabben&ldquo;. Um 21 Uhr begannen wir zusammen Brettspiele zu spielen, zum Beispiel Risiko, Monopoly und Werwolf. Zum Abschluss des ersten Tages trafen sich alle nochmal im Heiligtum, wo jeder seine Wünsche zum Gemeinschaftswochenende äußern durfte. Dann hieß es ab 23 Uhr Nachtruhe.</p>
<p>Der nächste Tag begann für die Jungs um 7:40 Uhr mit Musik, einem freundlichen Guten Morgen der Gruppenleiter und Frühsport. Hellwach gingen wir zum Morgengebet ins Heiligtum und anschließend frühstückten wir gemeinsam.</p>
<p>Um 9:00 Uhr stellte David sein Referat über das Leben von Josef Engling, seinem persönlichen Ideal und dem Gnadenkapital vor. In der darauffolgenden Gruppenstunde vertieften die Jungs das Thema und stellten die Ergebnisse den anderen Gruppen vor. Anschließend gab es vor dem Mittagessen eine kurze Pause.</p>
<p>Nach der Mittagspause ging es um 14:30 Uhr mit dem Spiel &bdquo;Alle die&hellip;&ldquo; weiter. Später gab es Kaffee und Kuchen. Vor der heiligen Messe um 16:30 Uhr, spielten wir draußen mehrere Gruppenspiele, zum Beispiel Atomzertrümmerung, Kettenfangen und Gefängnisball. Zusammen aßen wir zu Abend. Es folgte der Höhepunkt des Tages, &bdquo;Schlagt die Gruppenleiter&ldquo;. In 15 spannenden Spielen gingen die Jungs als knapper Sieger hervor. Beide Seiten hatten dabei viel Spaß. Der Tag wurde mit dem Abendgebet im Heiligtum und kleinen Spielen beendet.</p>
<p>Der letzte Tag brach mit dem Morgengebet und anschließendem Frühstück an. Nach dem Frühstück wurden dann die Zimmer fertig für die Abreise gemacht. Mit einem abschließenden Referat von Lukas über das Liebesbündnis mit Maria und dem Spiel &bdquo;Reise nach Jerusalem&ldquo;, gingen wir zum Mittagessen. Um 13:00 Uhr wurden die Jungs dann abgeholt. Damit endete ein sehr spaßiges und interessantes Wochenende.</p>
