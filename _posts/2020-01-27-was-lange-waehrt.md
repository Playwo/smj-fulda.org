---
title: "Was lange währt ..."
tagline: Neue Ereignisse im Fall der vermissten Lagerglocke
author: Christian Schopp
tags: [smj-fulda]
image:
  cover: https://live.staticflickr.com/4094/4901087678_b4a39da189_k_d.jpg
  align: center
---

Es gibt eine Neuigkeit, die viele nicht mehr für möglich gehalten hätten! Vor drei Jahren verschwand unsere Zeltlagerglocke. Sie ist ein wichtiges Symbol und wird von uns Jahr für Jahr gegen befreundete Zeltlager-Überfäller verteidigt.

Unter ungeklärten Umständen verschwand die Glocke im Jahr 2017. Alles wurde abgesucht, niemand konnte sich erklären, wo die Glocke abgeblieben war. Und nun am [vergangenen Begegnungstag](/artikel/2020-01-25-begegenungstag) brachte uns der Heilige Geist in Form der langjährigen SMJ-Unterstützerin Dorothea B. wieder die originale Glocke zurück. Sie hatte sich eine Auszeit genommen und die letzten Jahre auf einem Dachboden versteckt. Mit lediglich einer leichten Unterkühlung musste sie nicht mal einen Tag in einer Klinik in Fulda behandelt werden. Sie wird uns in Zukunft wieder mit ihrer hellen Stimme die Tage versüßen und unser morgendliches Aufstehen erleichtern.

Peinlich berührt stellte Sohn Benjamin B. nur fest, dass er sich auch nicht erklären könne, wie die Glocke in seinen Dachboden gelangt sein könnte.

Die Ermittlungen zu den genauen Hintergründen sind noch im Gange und werden voraussichtlich im Rahmen unseres diesjährigen [Detektiv-Zeltlagers](/zeltlager) ihren Abschluss finden.

![](https://live.staticflickr.com/65535/49556471008_5758f44144_w_d.jpg)
