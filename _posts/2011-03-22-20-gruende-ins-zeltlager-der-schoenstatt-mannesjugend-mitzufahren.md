---
categories:
- Zeltlager
- Gründe
migrated:
  node_id: 864
  migrated_at: 2017-02-20 22:26
  alias: artikel/864-20-gründe,-ins-zeltlager-der-schönstatt-mannesjugend-mitzufahren
  user_id: 17
tagline: Wir erklären ganz einfach, was an unserem Zeltlager so toll ist!
image:
  cover: https://farm5.staticflickr.com/4114/4893770027_be635866a5_b.jpg
author: Christian Schopp
title: 20 Gründe, ins Zeltlager der Schönstatt-Mannesjugend mitzufahren
created_at: 2011-03-22 14:28
---
<ol>
	<li>Es macht Spaß</li>
	<li>Du lernst neue Freunde kennen</li>
	<li>Du bist von zuhause weg</li>
	<li>Du lernst fürs Leben</li>
	<li>Du hast eine Beschäftigung in den langen Sommerferien</li>
	<li>Du bist an der frischen Luft</li>
	<li>Jeden Abend Lagerfeuer</li>
	<li>Stockbrot und Nachtwache</li>
	<li>Viele Spiele</li>
	<li>Du hängst nicht nur vor der Glotze</li>
	<li>12 Tage ohne Erwachsene</li>
	<li>Neue Erfahrungen, wie 2-Tages-Tour und Lagerolympiade</li>
	<li>Mit vielen Leuten ins Schwimmbad gehen</li>
	<li>Fast zwei Wochen unter Jungs</li>
	<li>Lagergeschichte und Singen</li>
	<li>Es ist für jeden etwas dabei</li>
	<li>Das Lager ist nicht teurer, als die Zeit zuhause zu verbringen</li>
	<li>Täglich die Möglichkeit, Fußball zu spielen</li>
	<li>Neue Talente entdecken</li>
	<li>Im Zelt schlafen</li>
</ol>

Da man dem wohl nichts mehr erwiedern kann, bleibt nur eins: Komm mit in unser <strong><a href="/zeltlager" title="Zeltlager 2011">Zeltlager</a>!</strong> Vom 28. Juni bis 9. Juli 2011 fahren wir mit Jungs von 9 bis 14 Jahren nach Oberweißenbrunn und gestalten dort 12 Tage voller Spaß und Aktionen. Du kannst dabei sein! :)
