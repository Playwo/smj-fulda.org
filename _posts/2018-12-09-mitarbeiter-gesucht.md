---
title: Mitarbeiter gesucht
author: Rainer M. Gotter
tags: [smjfulda, kreise]
image:
  cover: https://farm8.staticflickr.com/7840/45969071224_d9ef034991_o_d.jpg
---

Für die Jugendarbeit werden immer ehrenamtliche Mitarbeiter gesucht, die helfen können die Arbeit auf mehrere Schultern zu verteilen. Dadurch wird jeder Einzelne zeitlich weniger belastet und es müssen nicht immer alle vollzählig anwesend sein.

Doch halt, das war doch gar nicht damit gemeint. Die „WOLF-Gang“, unser Jugendkreis, machte sich am 2. Adventswochenende auf die Suche nachdem „Mitarbeiter-Bündnis“. Ein Mitarbeiterbündnis werden sich jetzt viele Fragen, was soll das denn sein. Genau das wollten wir zusammen heraus bekommen. So steckten wir unsere Köpfe zusammen und schauten uns die [5 Säulen](/säulen) der Schönstatt-Mannesjugend an.

„Diese Säulen haben es in sich“, meinten nicht nur Dominik und Benedikt. „Hier steckt echt viel drin und wie man als Vorbild für andere da sein soll“, ergänzte Elias. „Und!“, sagte Thomas, „welche Seiten ich an mir, im Alltag trainieren kann, um als Mitarbeiter in der SMJ aktiv mit anzupacken“

Der Kinofilm „Das bescheuerte Herz“ stellte die vielen verschiedenen Aspekte, die wir miteinander diskutiert hatten, nochmals in einer anderen, aber wahren Geschichte dar. Im Ausblick auf unser Ziel das Mitarbeiterbündnis im März 2019 gemeinsam zu schließen, heißt es jetzt für jeden von uns, sich auf dieses Bündnis vorzubereiten. Also nichts wie ran, unser Trainingsprogramm hat begonnen!
