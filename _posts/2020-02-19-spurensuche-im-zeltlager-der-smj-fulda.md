---
title: "Die drei ???"
tagline: Auf Spurensuche im Zeltlager der SMJ-Fulda
author: Christian Schopp
tags: [zeltlager]
image:
  cover: /images/pages/zeltlager.jpg
  align: center
---

Wer sich schon immer gefragt hat, was drei junge Detektive in den Sommerferien so anstellen, der ist bei uns genau richtig. Zehn Tage Abenteuer pur. Langeweile ist hier fehl am Platz.

Mysteriöses geht da vor auf dem Zeltplatz in Fellen. Keiner weiß genau, was passiert ist. Oder haben die Anwohner doch etwas zu verbergen? Bisher macht es eher den Eindruck, als wollte niemand mit den drei Fragezeichen reden. Aber irgendetwas muss dahinter stecken. Von ganz alleine kann so etwas nicht passieren. Da muss jemand seine Finger im Spiel gehabt haben. Doch welcher Ganove wäre zu so etwas in der Lage? Finden wir es heraus!

Die drei Detektive Justus, Peter und Bob sind von ihrem Cousin darum gebeten worden, ein großes Rätsel zu lösen. Die ersten Ermittlungen auf dem Zeltplatz Klause nahe dem bayerischen Fellen verliefen jedoch nicht so, wie sie es geplant hatten. Scheinbar versucht jemand mit allen Mitteln, ihre Nachforschungen zu behindern. Aber warum sollte das jemand tun?

Vielleicht kannst du den dreien dabei helfen, die Erklärung für all die Vorkommnisse zu finden? Dann melde dich an für das Zeltlager der Schönstatt Mannesjugend Fulda.

Das Sommerzeltlager findet dieses Jahr vom 5. bis 15. Juli auf dem Zeltplatz *Klause* in 97778 Fellen statt. Eingeladen sind alle 9 bis 14-jährigen Jungs, die Interesse an zehn unvergesslichen Tagen mit viel Action, Lagerfeuer, Freundschaft und Herausforderung haben. Veranstaltet wird das Lager von der Schönstatt Mannesjugend (SMJ) im Bistum Fulda. Als Teil der internationalen Schönstattbewegung sind wir ein christlicher Jugendverband und bieten jedes Jahr Veranstaltungen für Kinder und Jugendliche an. Weitere Informationen gibt es beim Lagerleiter Christian Schopp ([christian.schopp@smj-fulda.org](mailto:christian.schopp@smj-fulda.org) oder im Internet unter [smj-fulda.org/zeltlager](https://smj-fulda.org/zeltlager).

<a href="https://files.smj-fulda.org/zeltlager/zeltlager-flyer-2020.pdf" title="Zeltlagerflyer 2020" class="btn">Zeltlager-Einladung 2020 (PDF)</a>

<a href="/zeltlager" class="btn">Mehr zum Zeltlager</a>

<figure class="full-width">
  <div style="background: #000315;text-align: center;font-family: Arial, Helvetica, sans-serif;color: #fff;padding: 3em 1em;letter-spacing: -.03em; margin-bottom: 1rem; font-weight: bold;">
    <div style="">
      <span style="font-size: 4rem; vertical-align: middle;">
        Die drei
      </span>
      <span style="font-size: 9rem; vertical-align: middle; font-stretch: ultra-condensed; letter-spacing: -0.02em; line-height: 1;">
        <span style="color: #007eff;">?</span>?<span style="color: #fd3434;">?</span>
      </span>
    </div>
    <div style="font-size: 1.5rem; margin: .5rem 0; color: #ddd;">Auf Spurensuche im Zeltlager der SMJ-Fulda</div>
    <div style="font-size: 2.5rem; margin: .5rem 0;">5. bis 15. Juli 2020</div>
  </div>
</figure>
