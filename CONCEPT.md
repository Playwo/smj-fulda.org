
# Startseite

* Bannerbild od. Kentenich-Spruch
* Jahresparole prominent
* Vier Kacheln
    - SMJ-Fulda -> SMJ-Logo
    - Zeltlager -> Zelt-Icon
    - Aktionen -> Flüstertüte
    - Shop -> JESUS
* Veranstaltungskalender -> Termine aus Google-Kalender
* Newsfeed
* Kontakt
    - Kontaktdaten Ansprechpartner
    - Facebook-Link
    - Mail
* MTA

# Hauptnav
* SMJ-Fulda
    - Mitarbeiter
    - Zentrum
    - Geschichte
    - Fünf Säulen
    - Schönstatt
* Aktionen
    - Zeltlager
* Berichte
* Termine

##
* Shop
* Kontakt


# Struktur

* Home
* Die SMJ-Fulda
* Aktionen/Projekte
    - "Kerngeschäft"
        + Zeltlager -> Zelt
        + Gewo ->
        + Kreise -> Kreis mit Kapellchen

    - ""
        + Stammtisch -> Bierglas
        + Red Hearts -> Red-Hearts-Logo
        + Noahs Tafelrunde -> Arche Noah oder Tafelrunde, Schwert
        + Kreuzbergwanderung -> Kreuzberg

    - ""
        + SMJ-Shop -> JESUS
        + SMS-Adventskalender -> Adventskalender
        + Glaubenskurs -> Glaub-Logo
        + Boni-Wallfahrt
        + Fest des Glaubens -> FdG-Logo

    - ""
        + NdH -> NdH-Logo
        + Nightfever -> NF-Logo
        + misiones -> misiones-Logo

* Shop
*

# Einzelseiten

* Mitarbeiter/Team
    - Aktuelle Bilder! Einzel- und Gruppenbilder


